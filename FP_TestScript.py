#!/usr/bin/env python
import datetime
import filecmp
import fileinput
import os
import sys
import time

def prRed(skk): print("\033[91m {}\033[00m" .format(skk))
def prGreen(skk): print("\033[92m {}\033[00m" .format(skk))
def prYellow(skk): print("\033[93m {}\033[00m" .format(skk))
def prLightPurple(skk): print("\033[94m {}\033[00m" .format(skk))
def prPurple(skk): print("\033[95m {}\033[00m" .format(skk))
def prCyan(skk): print("\033[96m {}\033[00m" .format(skk))
def prLightGray(skk): print("\033[97m {}\033[00m" .format(skk))
def prBlack(skk): print("\033[98m {}\033[00m" .format(skk))


def replace_words(base_text, device_values):
    for key, val in device_values.items():
        base_text = base_text.replace(key, val)
    return base_text


data_folder = "data"
simulation_style = "pipeline" # valid values are "pipeline_wrapper" and "pipeline"
operation_type = "regridding" # valid values are "gridding", "regridding", and "fft_ifft"

if operation_type == "gridding":
	num_input_points_sim = 460800
	grid_dim_sim         = 768
	num_weights          = 193
elif operation_type == "regridding":
	num_input_points_sim = 103680
	grid_dim_sim         = 512
	num_weights          = 193
elif operation_type == "fft_ifft":
	num_input_points_sim = 0
	grid_dim_sim         = 512
	num_weights          = 256

x_min = 0
x_max = 7
y_min = 0
y_max = 7

# Change the testbench in the Makefile
replace_set = {}
replace_set["			testbench/"] = "			testbench/tempbench_fp.sv #"
t = open('Makefile', 'r')
tempstr = t.read()
t.close()
output = replace_words(tempstr, replace_set)
fout = open('Makefile', 'w')
fout.write(output)
fout.close()

# Comment out any SAIF generation lines
replace_set = {}
replace_set["localparam NUM_INPUT_POINTS_SIM"] = "localparam NUM_INPUT_POINTS_SIM = %d; //" % (num_input_points_sim)
replace_set["localparam GRID_DIM_SIM"] = "localparam GRID_DIM_SIM = %d; //" % (grid_dim_sim)
replace_set["localparam NUM_WEIGHTS_SIM"] = "localparam NUM_WEIGHTS_SIM = %d; //" % (num_weights)
replace_set["nufft_op        <= `SD "] = "nufft_op        <= `SD %s; //" % (operation_type.upper())

t = open('testbench/testbench_%s_nufft_fp.sv' % (simulation_style), 'r')
tempstr = t.read()
t.close()
output = replace_words(tempstr, replace_set)
fout = open('testbench/tempbench_fp.sv', 'w')
fout.write(output)
fout.close()

# Remove old build files and reset terminal
os.system('make nuke')
os.system('reset')

# Peform the tests
prCyan("\rSimulating %s %s with %d Input Points" % (simulation_style, operation_type, num_input_points_sim))
start = time.time()
if simulation_style == "pipeline_wrapper":
	if operation_type == "gridding" or operation_type == "fft_ifft" or operation_type == "regridding":
		sys.stdout.write('\r' + str("Processing All Pipelines..."))
		sys.stdout.flush() # important..don't remember why

		# Run the simulation
		# os.system('make') # show make output
		os.system('make > /dev/null') # hide make output

		# Copy the output somewhere we can check it
		os.system('cat %s/tmp/%s_output_ALL.out | tr -d "[:blank:]" > %s/matlab/checker_files/test.out.check'  % (data_folder, operation_type, data_folder))

		if filecmp.cmp('%s/matlab/checker_files/test.out.check' % (data_folder), '%s/matlab/checker_files/%s/%s_final_output_ALL.out' % (data_folder, operation_type, operation_type)):
			prGreen("\rAll Pipelines Match!" + ' ' * 20)
			# prGreen("Pass :D")
		else:
			prRed("\rAll Pipelines Don't Match...Try Running Individual Pipeline Simulations." + ' ' * 20)
			# prRed("Fail :(")

		os.remove('%s/matlab/checker_files/test.out.check' % (data_folder))
	else:
		prRed("\rERROR: %s %s is not yet a supported simulation type!" % (simulation_style, operation_type) + ' ' * 20)
elif simulation_style == "pipeline":
	if operation_type == "gridding" or operation_type == "regridding":
		for pipeline_x_id in range(x_min, x_max+1):
			for pipeline_y_id in range(y_min, y_max+1):
				sys.stdout.write('\r' + str("Processing Pipeline (%d, %d)..." % (pipeline_x_id, pipeline_y_id)))
				sys.stdout.flush() # important..don't remember why

				# Set the current pipeline to simulate in the testbench
				replace_set = {}
				replace_set["localparam X_ACCEL_IDX_SIM"] = "localparam X_ACCEL_IDX_SIM        = %d; //" % pipeline_x_id
				replace_set["localparam Y_ACCEL_IDX_SIM"] = "localparam Y_ACCEL_IDX_SIM        = %d; //" % pipeline_y_id
				t = open('testbench/tempbench_fp.sv', 'r')
				tempstr = t.read()
				t.close()
				output = replace_words(tempstr, replace_set)
				# Write the updated file
				fout = open('testbench/tempbench_fp.sv', 'w')
				fout.write(output)
				fout.close()

				# Run the simulation
				# os.system('make') # show make output
				os.system('make > /dev/null') # hide make output

				# Copy the output somewhere we can check it
				os.system('cat %s/tmp/%s_output_x%d_y%d.out | tr -d "[:blank:]" > %s/matlab/checker_files/test.out.check' % (data_folder, operation_type, pipeline_x_id, pipeline_y_id, data_folder))

				if filecmp.cmp('%s/matlab/checker_files/test.out.check' % (data_folder), '%s/matlab/checker_files/%s/%s_final_output_x%d_y%d.out' % (data_folder, operation_type, operation_type, pipeline_x_id, pipeline_y_id)):
					prGreen("\rPipeline (%d, %d)" % (pipeline_x_id, pipeline_y_id) + ' ' * 20)
					# prGreen("Pass :D")
				else:
					prRed("\rPipeline (%d, %d)" % (pipeline_x_id, pipeline_y_id) + ' ' * 20)
					# prRed("Fail :(")

				# Reset the testbench file to its previous state
				replace_set = {}
				replace_set["localparam X_ACCEL_IDX_SIM        = %d; //" % pipeline_x_id] = "localparam X_ACCEL_IDX_SIM"
				replace_set["localparam Y_ACCEL_IDX_SIM        = %d; //" % pipeline_y_id] = "localparam Y_ACCEL_IDX_SIM"
				t = open('testbench/tempbench_fp.sv', 'r')
				tempstr = t.read()
				t.close()
				output = replace_words(tempstr, replace_set)
				# Write the updated file
				fout = open('testbench/tempbench_fp.sv', 'w')
				fout.write(output)
				fout.close()

		os.remove('%s/matlab/checker_files/test.out.check' % (data_folder))
	else:
		prRed("\rERROR: %s %s is not yet a supported simulation type!" % (simulation_style, operation_type) + ' ' * 20)
else:
	prRed("\rERROR: %s is not a known/supported simulation type!" % (simulation_style) + ' ' * 20)

print("Time elapsed: %s" % datetime.timedelta(seconds = (time.time() - start)))

# Reset the testbench in the Makefile
replace_set = {}
replace_set["			testbench/tempbench_fp.sv #"] = "			testbench/"
t = open('Makefile', 'r')
tempstr = t.read()
t.close()
output = replace_words(tempstr, replace_set)
fout = open('Makefile', 'w')
fout.write(output)
fout.close()
