# make          <- runs simv (after compiling simv if needed)
# make all      <- runs simv (after compiling simv if needed)
# make simv     <- compile simv if needed (but do not run)
# make syn      <- runs syn_simv (after synthesizing if needed then 
#                                 compiling synsimv if needed)
# make clean    <- remove files created during compilations (but not synthesis)
# make nuke     <- remove all files created during compilation and synthesis
#
# To compile additional files, add them to the TESTBENCH or SIMFILES as needed
# Every .vg file will need its own rule and one or more synthesis scripts
# The information contained here (in the rules for those vg files) will be 
# similar to the information in those scripts but that seems hard to avoid.
#
#

VCS = SW_VCS=2015.09 vcs -sverilog +vc -Mupdate -line -full64
LIB = /afs/umich.edu/class/eecs470/lib/verilog/lec25dscc25.v

# For visual debugger
VISFLAGS = -lncurses

all:    simv
	./simv | tee program.out

##### 
# Modify starting here
#####

TESTBENCH =	src/sys_defs.vh								\
			src/custom_types.sv							\
			testbench/testbench_pipeline_nufft_fp.sv

SIMFILES =	src/pipeline_wrapper_nufft_fp.sv			\
			src/pipeline_nufft_fp.sv					\
			src/select.sv								\
			src/fft2_controller.sv						\
			src/fixed_mem_mult_complex.sv				\
			src/fp_mem_accum.sv							\
			src/complex_fixed_to_float_0cycle.sv		\
			src/fp_mult_complex.sv						\
			src/fp_add_complex.sv						\
			src/fp_sub_complex.sv						\
			src/fadd_0cycle.sv							\
			src/fsub_0cycle.sv							\
			src/fmul_0cycle.sv							\
			src/fixed_to_float_0cycle.sv				\
			src/delay_queue.sv							\
			src/sample_delay_queue.sv					\
			src/mem_1r_1w.sv							\
			src/mem_2rw.sv								\
			sram/16nm/4096x64b_2p/sram_2p_uhde_64b.v	\
			sram/16nm/512x32b_dp/sram_dp_uhde_32b.v

SYNFILES =	synth/sample.vg

# For visual debugger
VISTESTBENCH = $(TESTBENCH:tempbench_pipeline.sv=visual_tempbench_pipeline.sv)	\
		testbench/visual_c_hooks.c

synth/sample.vg:	$(SIMFILES) synth/pipeline.tcl
	dc_shell-t -f ./synth/pipeline.tcl | tee pipeline.out 
	# cd synth && dc_shell-t -f ./pipeline.tcl | tee pipeline.out 

#####
# Should be no need to modify after here
#####
simv:	$(SIMFILES) $(TESTBENCH)
	$(VCS) $(TESTBENCH) $(SIMFILES) -o simv
	
dve:	$(SIMFILES) $(TESTBENCH)
	$(VCS) +memcbk $(TESTBENCH) $(SIMFILES) -o dve -R -gui
.PHONY:	dve

# For visual debugger
vis_simv:	$(SIMFILES) $(VISTESTBENCH)
	$(VCS) $(VISFLAGS) $(VISTESTBENCH) $(SIMFILES) -o vis_simv 
	./vis_simv

syn_simv:	$(SYNFILES) $(TESTBENCH)
	$(VCS) $(TESTBENCH) $(SYNFILES) $(LIB) -o syn_simv 

syn:	syn_simv
	./syn_simv | tee syn_program.out

clean:
	rm -rf simv simv.daidir csrc vcs.key program.out
	rm -rf vis_simv vis_simv.daidir
	rm -rf dve*
	rm -rf syn_simv syn_simv.daidir syn_program.out
	rm -rf synsimv synsimv.daidir csrc vcdplus.vpd vcs.key synprog.out pipeline.out writeback.out vc_hdrs.h
	rm -rf *.log ucli.key
	rm -rf crte_0000* Synopsys_stack_trace_*
	rm -f *.saif
	rm -f *.out output_data/*.out output_data_3d/*.out

nuke:	clean
	rm -f synth/*.vg synth/*.rep synth/*.ddc synth/*.chk synth/command.log
	rm -f synth/*.out command.log synth/*.db synth/*.svf
	rm -rf alib-52/
	rm -rf DVEfiles/
	rm -rf default.svf inter.vpd
	rm -rf synth/work
	rm -f data/tmp/*
