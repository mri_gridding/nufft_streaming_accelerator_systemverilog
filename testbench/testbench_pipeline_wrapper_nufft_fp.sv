/////////////////////////////////////////////////////////////////////////
//                                                                     //
//  Module name :  testbench.sv                                        //
//                                                                     //
//  Description :  Testbench for beamforming pipeline; this reads      //
//                 data files and distributes data to various modules. //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

`timescale 1ns/100ps
import custom_types::*;

module testbench;

    // Parameters
    parameter INPUT_BIT_WIDTH        = 32; // used as a #define for input files
    parameter ACCEL_DIM              = 8; // number of pipelines in the y dimension
    parameter MAX_GRID_DIM           = 1024; // max size of the grid in the y dimension
    parameter MEM_TILE_DIM           = (MAX_GRID_DIM/ACCEL_DIM);
    parameter MAX_INTERP_WINDOW_SIZE = ACCEL_DIM; // max size of the interpolation window in any single dimension
    parameter SAMPLE_BIT_WIDTH       = 32;
    parameter SAMPLE_COORD_BIT_WIDTH = 32;
    parameter WEIGHT_BIT_WIDTH       = 16;
    parameter WEIGHT_FRAC_WIDTH      = 14;
    parameter WEIGHT_OVERSAMP        = 32; // number of table samples between each whole coordinate
    parameter NUM_WEIGHTS            = 512; // (MAX_INTERP_WINDOW_SIZE*WEIGHT_OVERSAMP);
    parameter NUM_BANKS              = 4; // number of banks in each pipeline's data buffer
    parameter NUM_BANK_ENTRIES       = 4096; // number of entries in each data buffer bank
    parameter NUM_BUFFER_ENTRIES     = (NUM_BANKS*NUM_BANK_ENTRIES); // number of entries in each pipeline's data buffer
    parameter MAX_TRANSMIT_HOPS      = ACCEL_DIM;
    parameter N = 32;
    parameter E = 8;
    parameter S = 1;

    localparam INPUT_DATA_FOLDER     = "data";
    localparam OUTPUT_DATA_FOLDER    = "data";
    localparam NUM_INPUT_POINTS_SIM  = 103680; // GRIDDING = 460800; REGRIDDING = 103680; FFT_IFFT = N/A
    localparam GRID_DIM_SIM          = 512; // GRIDDING = 768; REGRIDDING = 512; FFT_IFFT = 512 OR 1024
    localparam TILE_DIM_SIM          = (GRID_DIM_SIM/ACCEL_DIM);
    localparam INTERP_WINDOW_DIM_SIM = 6;
    localparam NUM_WEIGHTS_SIM       = 256; // GRIDDING = 193; REGRIDDING = 193; FFT_IFFT = 256 OR 512 (GRID_DIM_SIM/2)
    localparam NUM_BUFFER_DATA       = (MAX_GRID_DIM*MAX_GRID_DIM)/(ACCEL_DIM*ACCEL_DIM);

    function logic [15:0] swap_endianness_16b (
        // Inputs
        input logic [15:0] input_val,

        // Outputs
        output logic [15:0] output_val
    );

        begin
            output_val[15:8] = input_val[7:0];
            output_val[7:0]  = input_val[15:8];

            return output_val;
        end
    endfunction

    function logic [31:0] swap_endianness_32b (
        // Inputs
        input logic [31:0] input_val,

        // Outputs
        output logic [31:0] output_val
    );

        begin
            output_val[31:24] = input_val[7:0];
            output_val[23:16] = input_val[15:8];
            output_val[15:8]  = input_val[23:16];
            output_val[7:0]   = input_val[31:24];

            return output_val;
        end
    endfunction

    function fixed_to_float (
        // Inputs
        input logic [16-1:0] fixed_in,

        // Outputs
        output logic [32-1:0] float_out
    );

        logic                   float_sign_next;
        logic [8-1:0]           float_exp_next;
        logic [16-2:0]          tmp_mant_next;
        logic [32-8-1-1:0]       float_mant_next;

        begin
            tmp_mant_next = fixed_in[16-2:0];

            float_sign_next = '0;
            float_exp_next = '0;
            float_mant_next = '0;

            // Cycle 0 - get absolute value of input
            if(fixed_in[16-1]) begin
                float_sign_next = 1;
                tmp_mant_next = (~fixed_in[16-2:0])+1;
            end

            if(fixed_in[16-2:0] != 0) begin
                for(int i = 0; i < 16-1; ++i) begin
                    if(tmp_mant_next[16-2-i]) begin
                        float_exp_next = 127 - i;
                        float_mant_next[32-8-1-1:9] = tmp_mant_next[16-3:0] << i;

                        break;
                    end
                end
            end

            float_out = {float_sign_next, float_exp_next, float_mant_next};

            return float_out;
        end

    endfunction

    // Testbench Signals
    logic        clock;
    logic        reset;
    logic [31:0] clock_count;

    // Pipeline Signals

    // Inputs
    operation_types                                                      nufft_op;
    logic                                                                init_mem;
    logic                                                                fft_start_en_in;
    logic                                                                ifft_en_in;
    logic [$clog2(MAX_GRID_DIM):0]                                       grid_dim_in;
    logic [$clog2($clog2(MAX_GRID_DIM)):0]                               fft_num_stages_in;
    logic [$clog2(MEM_TILE_DIM)-1:0]                                     tile_dim_in;
    logic [$clog2(MAX_INTERP_WINDOW_SIZE)-1:0]                           window_dim_in;
    logic                                                                weight_const_wr_en_in;
    logic [$clog2(NUM_WEIGHTS)-1:0]                                      weight_const_wr_addr_in;
    logic [(WEIGHT_BIT_WIDTH*2)-1:0]                                     weight_const_wr_data_in;
    logic                                                                rd_en_in; // output a value (for reading final values out)
    logic [$clog2(NUM_BUFFER_ENTRIES)-1:0]                               rd_addr_in; // read/write tile address (can be used for reading final values out)
    logic [ACCEL_DIM-1:0][ACCEL_DIM-1:0]                                 wr_en_in; // write a value (for loading uniform grid data)
    logic [ACCEL_DIM-1:0][ACCEL_DIM-1:0][$clog2(NUM_BUFFER_ENTRIES)-1:0] wr_addr_in;
    logic [ACCEL_DIM-1:0][ACCEL_DIM-1:0][(SAMPLE_BIT_WIDTH*2)-1:0]       wr_data_in;
    nonuniform_sample                                                    sample_in;

    // Outputs
    nonuniform_sample                                              sample_out;
    logic [ACCEL_DIM-1:0][ACCEL_DIM-1:0]                           rd_en_out;
    logic [ACCEL_DIM-1:0][ACCEL_DIM-1:0][(SAMPLE_BIT_WIDTH*2)-1:0] rd_data_out;
    logic [ACCEL_DIM-1:0][ACCEL_DIM-1:0]                           fft_done_out;


    // Instantiate the pipeline wrapper
    pipeline_wrapper_nufft_fp #(
        .ACCEL_DIM(ACCEL_DIM),
        .MAX_GRID_DIM(MAX_GRID_DIM),
        .MEM_TILE_DIM(MEM_TILE_DIM),
        .MAX_INTERP_WINDOW_SIZE(MAX_INTERP_WINDOW_SIZE),
        .SAMPLE_BIT_WIDTH(SAMPLE_BIT_WIDTH),
        .SAMPLE_COORD_BIT_WIDTH(SAMPLE_COORD_BIT_WIDTH),
        .WEIGHT_BIT_WIDTH(WEIGHT_BIT_WIDTH),
        .WEIGHT_FRAC_WIDTH(WEIGHT_FRAC_WIDTH),
        .WEIGHT_OVERSAMP(WEIGHT_OVERSAMP),
        .NUM_WEIGHTS(NUM_WEIGHTS),
        .NUM_BANKS(NUM_BANKS),
        .NUM_BANK_ENTRIES(NUM_BANK_ENTRIES),
        .NUM_BUFFER_ENTRIES(NUM_BUFFER_ENTRIES),
        .MAX_TRANSMIT_HOPS(MAX_TRANSMIT_HOPS),
        .N(N),
        .E(E),
        .S(S)
        ) pipeline_wrap_0 (

        // Inputs
        .clock(clock),
        .reset(reset),
        .nufft_op(nufft_op),
        .init_mem(init_mem),
        .fft_start_en_in(fft_start_en_in),
        .ifft_en_in(ifft_en_in),
        .grid_dim_in(grid_dim_in),
        .fft_num_stages_in(fft_num_stages_in),
        .tile_dim_in(tile_dim_in),
        .window_dim_in(window_dim_in),
        .weight_const_wr_en_in(weight_const_wr_en_in),
        .weight_const_wr_addr_in(weight_const_wr_addr_in),
        .weight_const_wr_data_in(weight_const_wr_data_in),
        .rd_en_in(rd_en_in),
        .rd_addr_in(rd_addr_in),
        .wr_en_in(wr_en_in),
        .wr_addr_in(wr_addr_in),
        .wr_data_in(wr_data_in),
        .sample_in(sample_in),

        // Outputs
        .sample_out(sample_out),
        .rd_en_out(rd_en_out),
        .rd_data_out(rd_data_out),
        .fft_done_out(fft_done_out)

    );

    integer ppfile; // output file descriptor
    integer testfile; // output file descriptor
    integer output_file; // output file descriptor
    integer debug_file; // output file descriptor
    integer fileID12; // output file descriptor
    integer file_weight_data; // input file descriptor
    integer file_uniform_data; // input file descriptor
    integer file_nonuniform_data; // input file descriptor
    integer file_nonuniform_coords; // input file descriptor
    integer code; // return code for fscanf
    logic [31:0] nonuniform_coord_x; // assumes the file always contains 32-bit integers
    logic [31:0] nonuniform_coord_y; // assumes the file always contains 32-bit integers
    logic [31:0] nonuniform_data_real; // assumes the file always contains 32-bit integers
    logic [31:0] nonuniform_data_imag; // assumes the file always contains 32-bit integers
    logic [31:0] uniform_data_real; // assumes the file always contains 32-bit integers
    logic [31:0] uniform_data_imag; // assumes the file always contains 32-bit integers
    logic [31:0] table_data; // assumes the file always contains 32-bit integers
    logic [63:0] file_tmp_64b; // used as a temp holder for fscanf returns
    logic [31:0] file_tmp_32b; // used as a temp holder for fscanf returns
    logic [15:0] file_tmp_16b; // used as a temp holder for fscanf returns
    integer iter_count; // used as an index/counter in the fscanf loops
    integer debug_count; // used as an index/counter in the fscanf loops

    // Generate System Clock
    always begin
        #(`VERILOG_CLOCK_PERIOD/2.0);
        clock = ~clock;
    end

    // Count the number of posedges and number of instructions completed
    // till simulation ends
    always @(posedge clock) begin
        if(reset) begin
            clock_count <= `SD 0;
        end else begin
            clock_count <= `SD (clock_count + 1);
        end
    end

    // Print Debug Output
    always @(posedge clock) begin
        // if(nufft_op == FFT_IFFT && pipeline_wrap_0.gen_pipelines_x[0].gen_pipelines_y[0].pipeline_0.fft_w_en) begin
        //     $fdisplay(fileID12, "w_addr: %0d", pipeline_wrap_0.gen_pipelines_x[0].gen_pipelines_y[0].pipeline_0.fft_w_addr);
        // end
        // if(nufft_op == FFT_IFFT && pipeline_wrap_0.gen_pipelines_x[0].gen_pipelines_y[0].pipeline_0.fft_w_en && pipeline_wrap_0.gen_pipelines_x[0].gen_pipelines_y[0].pipeline_0.fft_val_en) begin
        //     $fdisplay(fileID12, "tile_idx_1: %0d", pipeline_wrap_0.gen_pipelines_x[0].gen_pipelines_y[0].pipeline_0.fft_val_addr);
        // end
        // if(nufft_op == FFT_IFFT && ~pipeline_wrap_0.gen_pipelines_x[0].gen_pipelines_y[0].pipeline_0.fft_w_en && pipeline_wrap_0.gen_pipelines_x[0].gen_pipelines_y[0].pipeline_0.fft_val_en) begin
        //     $fdisplay(fileID12, "tile_idx_2: %0d", pipeline_wrap_0.gen_pipelines_x[0].gen_pipelines_y[0].pipeline_0.fft_val_addr);
        // end
        // if(nufft_op == FFT_IFFT && pipeline_wrap_0.gen_pipelines_x[0].gen_pipelines_y[0].pipeline_0.fp_rd_en_out) begin
        //     $fdisplay(fileID12, "%b", pipeline_wrap_0.gen_pipelines_x[0].gen_pipelines_y[0].pipeline_0.fp_rd_data_out);
        // end
        // if(nufft_op == FFT_IFFT && pipeline_wrap_0.gen_pipelines_x[0].gen_pipelines_y[0].pipeline_0.fp_wr_en) begin
        //     $fdisplay(fileID12, "%b", pipeline_wrap_0.gen_pipelines_x[0].gen_pipelines_y[0].pipeline_0.fp_wr_data);
        // end
        // if(nufft_op == FFT_IFFT && pipeline_wrap_0.gen_pipelines_x[0].gen_pipelines_y[0].pipeline_0.fp_mult_en) begin
        //     $fdisplay(fileID12, "%b\t%b", pipeline_wrap_0.gen_pipelines_x[0].gen_pipelines_y[0].pipeline_0.weight_data, pipeline_wrap_0.gen_pipelines_x[0].gen_pipelines_y[0].pipeline_0.sample_data);
        // end
        // if(nufft_op == FFT_IFFT && pipeline_wrap_0.gen_pipelines_x[0].gen_pipelines_y[0].pipeline_0.fp_add_en) begin
        //     $fdisplay(fileID12, "%b\t%b %0d %b", pipeline_wrap_0.gen_pipelines_x[0].gen_pipelines_y[0].pipeline_0.fp_add_val1, pipeline_wrap_0.gen_pipelines_x[0].gen_pipelines_y[0].pipeline_0.fp_add_val2, pipeline_wrap_0.gen_pipelines_x[0].gen_pipelines_y[0].pipeline_0.delayed_fft_transmit_dist, pipeline_wrap_0.gen_pipelines_x[0].gen_pipelines_y[0].pipeline_0.delayed_fft_second_dim_en);
        // end

        // if(nufft_op == FFT_IFFT && pipeline_wrap_0.gen_pipelines_x[7].gen_pipelines_y[7].pipeline_0.fft_w_en && pipeline_wrap_0.gen_pipelines_x[7].gen_pipelines_y[7].pipeline_0.fft_val_en) begin
        //     $fdisplay(fileID12, "tile_idx_1: %0d", pipeline_wrap_0.gen_pipelines_x[7].gen_pipelines_y[7].pipeline_0.fft_val_addr);
        // end
        // if(nufft_op == FFT_IFFT && ~pipeline_wrap_0.gen_pipelines_x[7].gen_pipelines_y[7].pipeline_0.fft_w_en && pipeline_wrap_0.gen_pipelines_x[7].gen_pipelines_y[7].pipeline_0.fft_val_en) begin
        //     $fdisplay(fileID12, "tile_idx_2: %0d", pipeline_wrap_0.gen_pipelines_x[7].gen_pipelines_y[7].pipeline_0.fft_val_addr);
        // end
        // if(nufft_op == FFT_IFFT && pipeline_wrap_0.gen_pipelines_x[7].gen_pipelines_y[7].pipeline_0.fp_rd_en_out) begin
        //     $fdisplay(fileID12, "%b", pipeline_wrap_0.gen_pipelines_x[7].gen_pipelines_y[7].pipeline_0.fp_rd_data_out);
        // end
        // if(nufft_op == FFT_IFFT && pipeline_wrap_0.gen_pipelines_x[7].gen_pipelines_y[7].pipeline_0.fp_wr_en) begin
        //     $fdisplay(fileID12, "%b", pipeline_wrap_0.gen_pipelines_x[7].gen_pipelines_y[7].pipeline_0.fp_wr_data);
        // end
        // if(nufft_op == FFT_IFFT && pipeline_wrap_0.gen_pipelines_x[7].gen_pipelines_y[7].pipeline_0.fp_mult_en) begin
        //     $fdisplay(fileID12, "%b\t%b", pipeline_wrap_0.gen_pipelines_x[7].gen_pipelines_y[7].pipeline_0.weight_data, pipeline_wrap_0.gen_pipelines_x[7].gen_pipelines_y[7].pipeline_0.sample_data);
        // end
        // if(nufft_op == FFT_IFFT && pipeline_wrap_0.gen_pipelines_x[7].gen_pipelines_y[7].pipeline_0.fp_sub_en) begin
        //     $fdisplay(fileID12, "%b\t%b %0d %b", pipeline_wrap_0.gen_pipelines_x[7].gen_pipelines_y[7].pipeline_0.fp_sub_val1, pipeline_wrap_0.gen_pipelines_x[7].gen_pipelines_y[7].pipeline_0.fp_sub_val2, pipeline_wrap_0.gen_pipelines_x[7].gen_pipelines_y[7].pipeline_0.delayed_fft_transmit_dist, pipeline_wrap_0.gen_pipelines_x[7].gen_pipelines_y[7].pipeline_0.delayed_fft_second_dim_en);
        // end
        if(nufft_op == FFT_IFFT && pipeline_wrap_0.gen_pipelines_x[7].gen_pipelines_y[7].pipeline_0.fp_sub_en) begin
            $fdisplay(fileID12, "%b\t%b %0d %b", pipeline_wrap_0.gen_pipelines_x[7].gen_pipelines_y[7].pipeline_0.fp_sub_val1, pipeline_wrap_0.gen_pipelines_x[7].gen_pipelines_y[7].pipeline_0.fp_sub_val2, pipeline_wrap_0.gen_pipelines_x[7].gen_pipelines_y[7].pipeline_0.delayed_fft_transmit_dist2, pipeline_wrap_0.gen_pipelines_x[7].gen_pipelines_y[7].pipeline_0.delayed_fft_second_dim_en2);
        end
    end // always @(posedge clock)

    // Print Pipeline Output
    always @(posedge clock) begin
        if(nufft_op == REGRIDDING) begin
            if(sample_out.en) begin
                $fdisplay(output_file, "%b", sample_out.data);
            end
        end
    end // always @(posedge clock)

    initial begin
        // $set_gate_level_monitoring("on");
        // $set_toggle_region(pipeline_wrap_0);
        // $toggle_start;

        // $monitor("Time:%4.0f reset:%b data_in:%d rd_data_out:%d", $time, reset, data_in, rd_data_out);
        ppfile   = $fopen("pipeline.out", "w");
        testfile = $fopen("test.out", "w");

        fileID12 = $fopen($psprintf("%s/tmp/wrap_random.out", OUTPUT_DATA_FOLDER), "w");

        // Initialize inputs
        clock <= `SD '0;
        reset <= `SD '0;

        nufft_op        <= `SD FFT_IFFT;
        init_mem        <= `SD '0;
        fft_start_en_in <= `SD '0;
        ifft_en_in      <= `SD '0;

        grid_dim_in       <= `SD GRID_DIM_SIM;
        fft_num_stages_in <= `SD $clog2(GRID_DIM_SIM);
        tile_dim_in       <= `SD TILE_DIM_SIM;
        window_dim_in     <= `SD INTERP_WINDOW_DIM_SIM;

        weight_const_wr_en_in   <= `SD '0;
        weight_const_wr_addr_in <= `SD '0;
        weight_const_wr_data_in <= `SD '0;

        rd_en_in   <= `SD '0;
        rd_addr_in <= `SD '0;

        wr_en_in   <= `SD '0;
        wr_addr_in <= `SD '0;
        wr_data_in <= `SD '0;

        sample_in.en      <= `SD '0;
        sample_in.x_coord <= `SD '0;
        sample_in.y_coord <= `SD '0;
        sample_in.data    <= `SD '0;

        // Pulse the mem init signal
        $display("@@\n@@\nInitializing Memory......\n@@\n@@\n");
        init_mem                    <= `SD 1'b1;
        @(posedge clock);
        init_mem                    <= `SD 1'b0;

        @(posedge clock);

        // Pulse the reset signal
        $display("@@\n@@\nAsserting System reset......\n@@\n@@\n");
        reset <= `SD 1'b1;

        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        
        $display("Reading Weight Constants...");
        $fdisplay(ppfile, "Reading Weight Constants...");
        file_weight_data = $fopen($psprintf("%s/matlab/input/%s/%s_weight_data_fixed.%0d.bin", INPUT_DATA_FOLDER, nufft_op.name().tolower(), nufft_op.name().tolower(), NUM_WEIGHTS_SIM), "rb"); // "rb" means reading binary
        iter_count       = 0;
        while(!$feof(file_weight_data)) begin
            // Get the real component
            code = $fscanf(file_weight_data,"%h", file_tmp_16b);
            swap_endianness_16b(file_tmp_16b, table_data[31:16]);

            // Get the imag component
            code = $fscanf(file_weight_data,"%h", file_tmp_16b);
            swap_endianness_16b(file_tmp_16b, table_data[15:0]);

            // $display("%h", table_data);
            weight_const_wr_data_in <= `SD table_data;
            weight_const_wr_addr_in <= `SD iter_count;
            weight_const_wr_en_in   <= `SD 1;

            @(posedge clock);
            iter_count++;
        end // while(!$feof(file_weight_data))
        weight_const_wr_en_in   <= `SD '0;
        weight_const_wr_addr_in <= `SD '0;
        weight_const_wr_data_in <= `SD '0;
        $fclose(file_weight_data); // once reading is finished, close the file
        $display("Done Reading Weight Constants.");
        $fdisplay(ppfile, "Done Reading Weight Constants.");
        
        @(posedge clock);
        
        $display("@@\n@@\nConstant Initialization Complete...Deasserting System Reset...... %d Cycles\n@@\n@@\n", clock_count);
        reset <= `SD 1'b0;

        @(posedge clock);

        if(nufft_op == GRIDDING) begin
            output_file = $fopen($psprintf("%s/tmp/%s_output_ALL.out", OUTPUT_DATA_FOLDER, nufft_op.name().tolower()), "w");

            $display("Reading input data...");
            $fdisplay(ppfile, "Reading input data...");
            file_nonuniform_coords = $fopen($psprintf("%s/matlab/input/%s/%s_nonuniform_coords.%0d.bin", INPUT_DATA_FOLDER, nufft_op.name().tolower(), nufft_op.name().tolower(), NUM_INPUT_POINTS_SIM), "rb"); // "rb" means reading binary
            file_nonuniform_data = $fopen($psprintf("%s/matlab/input/%s/%s_nonuniform_data.%0d.bin", INPUT_DATA_FOLDER, nufft_op.name().tolower(), nufft_op.name().tolower(), NUM_INPUT_POINTS_SIM), "rb"); // "rb" means reading binary
            while(!$feof(file_nonuniform_coords)) begin // read line by line until an "end of file" is reached
                // Get the x coordinate
                code = $fscanf(file_nonuniform_coords,"%h", file_tmp_32b[31:16]);
                code = $fscanf(file_nonuniform_coords,"%h", file_tmp_32b[15:0]);
                swap_endianness_32b(file_tmp_32b, nonuniform_coord_x);
                // $display("%h", nonuniform_coord_x);

                // Get the y coordinate
                code = $fscanf(file_nonuniform_coords,"%h", file_tmp_32b[31:16]);
                code = $fscanf(file_nonuniform_coords,"%h", file_tmp_32b[15:0]);
                swap_endianness_32b(file_tmp_32b, nonuniform_coord_y);
                // $display("%h", nonuniform_coord_y);

                // Get the real component
                code = $fscanf(file_nonuniform_data,"%h", file_tmp_32b[31:16]);
                code = $fscanf(file_nonuniform_data,"%h", file_tmp_32b[15:0]);
                swap_endianness_32b(file_tmp_32b, nonuniform_data_real);
                // $display("%h", nonuniform_data_real);

                // Get the real component
                code = $fscanf(file_nonuniform_data,"%h", file_tmp_32b[31:16]);
                code = $fscanf(file_nonuniform_data,"%h", file_tmp_32b[15:0]);
                swap_endianness_32b(file_tmp_32b, nonuniform_data_imag);
                // $display("%h", nonuniform_data_imag);

                // Assign the values to the signals
                sample_in.en      <= `SD 1;
                sample_in.x_coord <= `SD nonuniform_coord_x;
                sample_in.y_coord <= `SD nonuniform_coord_y;
                sample_in.data    <= `SD {nonuniform_data_real, nonuniform_data_imag};

                @(posedge clock);
            end // while(!$feof(file_nonuniform_coords))
            sample_in.en      <= `SD '0;
            sample_in.x_coord <= `SD '0;
            sample_in.y_coord <= `SD '0;
            sample_in.data    <= `SD '0;
            $fclose(file_nonuniform_data); // once reading is finished, close the file
            $fclose(file_nonuniform_coords); // once reading is finished, close the file
            $display("Done reading input data.\n");
            $fdisplay(ppfile, "Done reading input data.");

            for(int i = 0; i <= 500; i++) begin
                @(posedge clock); // extra cycles to account for module propagation delay
            end

            // Finished
            // $toggle_stop;
            // $toggle_report("PIPELINE_WRAPPER.saif", 1.0e-9, pipeline_wrap_0);

            $display("@@\n@@\nData Stream Complete...Reading Grid Data...... %d Cycles\n@@\n@@\n", clock_count);

            // // Read out the data column-by-column
            // nufft_op = READ_MEM;
            // rd_en_in <= `SD 1;
            // for(int i = 0; i < ACCEL_DIM; i++) begin
            //     for(int j = 0; j < ACCEL_DIM; j++) begin
            //         // debug_file = $fopen($psprintf("%s/tmp/gridding_output_x%0d_y%0d.out", OUTPUT_DATA_FOLDER, i, j), "w");
            //         for(int addr = 0; addr < NUM_BUFFER_DATA; addr++) begin
            //             rd_en_in   <= `SD 1;
            //             rd_addr_in <= `SD addr;
            //             @(posedge clock);
            //             rd_en_in <= `SD 0;

            //             while(!rd_en_out[i][j])
            //                 @(posedge clock);

            //             $fdisplay(output_file, "%b", rd_data_out[i][j]);
            //             // $fdisplay(debug_file, "%b", rd_data_out[i][j]);
            //         end
            //         // $fclose(debug_file);
            //     end
            // end
            // rd_en_in <= `SD 0;
            // @(posedge clock);
            // nufft_op = READ_MEM;

            // Read out the data tile-by-tile (this reduces read time; column-by-column is far slower)
            nufft_op = READ_MEM;
            rd_en_in <= `SD 1;
            for(int addr = 0; addr < NUM_BUFFER_DATA; addr++) begin
                rd_en_in   <= `SD 1;
                rd_addr_in <= `SD addr;
                @(posedge clock);
                rd_en_in <= `SD 0;

                // Loop until we see the data is ready (all bits will be true)
                while(!rd_en_out[0][0])
                    @(posedge clock);

                // Write all available points to the file (order is {tile1, tile2, ..., tileN})
                for(int i = 0; i < ACCEL_DIM; i++) begin
                    for(int j = 0; j < ACCEL_DIM; j++) begin
                        $fdisplay(output_file, "%b", rd_data_out[i][j]);
                    end
                end
            end
            rd_en_in <= `SD 0;
            @(posedge clock);
            nufft_op = READ_MEM;

            $fclose(output_file);
        end else if(nufft_op == REGRIDDING) begin
            output_file = $fopen($psprintf("%s/tmp/%s_output_ALL.out", OUTPUT_DATA_FOLDER, nufft_op.name().tolower()), "w");

            // nufft_op <= `SD READ_MEM;
            for(int x_accel_idx = 0; x_accel_idx < ACCEL_DIM; x_accel_idx++) begin
                for(int y_accel_idx = 0; y_accel_idx < ACCEL_DIM; y_accel_idx++) begin
                    $display("Reading Grid Data From %s", $psprintf("%s/matlab/input/%s/uniform_data/%s_uniform_data_col_x%0d_y%0d.%0d.bin", INPUT_DATA_FOLDER, nufft_op.name().tolower(), nufft_op.name().tolower(), x_accel_idx, y_accel_idx, NUM_BUFFER_DATA));
                    $fdisplay(ppfile, "Reading Grid Data From %s", $psprintf("%s/matlab/input/%s/uniform_data/%s_uniform_data_col_x%0d_y%0d.%0d.bin", INPUT_DATA_FOLDER, nufft_op.name().tolower(), nufft_op.name().tolower(), x_accel_idx, y_accel_idx, NUM_BUFFER_DATA));
                    file_uniform_data = $fopen($psprintf("%s/matlab/input/%s/uniform_data/%s_uniform_data_col_x%0d_y%0d.%0d.bin", INPUT_DATA_FOLDER, nufft_op.name().tolower(), nufft_op.name().tolower(), x_accel_idx, y_accel_idx, NUM_BUFFER_DATA), "rb"); // "rb" means reading binary
                    iter_count        = 0;
                    while(!$feof(file_uniform_data)) begin
                        // Get the real component
                        code = $fscanf(file_uniform_data,"%h", file_tmp_32b[31:16]);
                        code = $fscanf(file_uniform_data,"%h", file_tmp_32b[15:0]);
                        swap_endianness_32b(file_tmp_32b, uniform_data_real);
                        // $display("%h", nonuniform_data_real);

                        // Get the real component
                        code = $fscanf(file_uniform_data,"%h", file_tmp_32b[31:16]);
                        code = $fscanf(file_uniform_data,"%h", file_tmp_32b[15:0]);
                        swap_endianness_32b(file_tmp_32b, uniform_data_imag);
                        // $display("%h", nonuniform_data_imag);

                        // Assign the values to the signals
                        wr_en_in[x_accel_idx][y_accel_idx]   <= `SD 1;
                        wr_addr_in[x_accel_idx][y_accel_idx] <= `SD iter_count;
                        wr_data_in[x_accel_idx][y_accel_idx] <= `SD {uniform_data_real, uniform_data_imag};

                        @(posedge clock);
                        iter_count++;
                    end // while(!$feof(file_uniform_data))
                    wr_en_in   <= `SD '0;
                    wr_addr_in <= `SD '0;
                    wr_data_in <= `SD '0;
                    $fclose(file_uniform_data); // once reading is finished, close the file
                end
            end
            $display("Done Reading Grid Data.");
            $fdisplay(ppfile, "Done Reading Grid Data.");
            @(posedge clock);
            nufft_op <= `SD REGRIDDING;

            $display("Reading input data...");
            $fdisplay(ppfile, "Reading input data...");
            file_nonuniform_coords = $fopen($psprintf("%s/matlab/input/%s/%s_nonuniform_coords.%0d.bin", INPUT_DATA_FOLDER, nufft_op.name().tolower(), nufft_op.name().tolower(), NUM_INPUT_POINTS_SIM), "rb"); // "rb" means reading binary
            file_nonuniform_data   = $fopen($psprintf("%s/matlab/input/%s/nonuniform_data/%s_nonuniform_data_col_x0_y0.%0d.bin", INPUT_DATA_FOLDER, nufft_op.name().tolower(), nufft_op.name().tolower(), NUM_INPUT_POINTS_SIM), "rb"); // "rb" means reading binary
            while(!$feof(file_nonuniform_coords)) begin // read line by line until an "end of file" is reached
                // Get the x coordinate
                code = $fscanf(file_nonuniform_coords,"%h", file_tmp_32b[31:16]);
                code = $fscanf(file_nonuniform_coords,"%h", file_tmp_32b[15:0]);
                swap_endianness_32b(file_tmp_32b, nonuniform_coord_x);
                // $display("%h", nonuniform_coord_x);

                // Get the y coordinate
                code = $fscanf(file_nonuniform_coords,"%h", file_tmp_32b[31:16]);
                code = $fscanf(file_nonuniform_coords,"%h", file_tmp_32b[15:0]);
                swap_endianness_32b(file_tmp_32b, nonuniform_coord_y);
                // $display("%h", nonuniform_coord_y);

                // Get the real component
                code = $fscanf(file_nonuniform_data,"%h", file_tmp_32b[31:16]);
                code = $fscanf(file_nonuniform_data,"%h", file_tmp_32b[15:0]);
                swap_endianness_32b(file_tmp_32b, nonuniform_data_real);
                // $display("%h", nonuniform_data_real);

                // Get the real component
                code = $fscanf(file_nonuniform_data,"%h", file_tmp_32b[31:16]);
                code = $fscanf(file_nonuniform_data,"%h", file_tmp_32b[15:0]);
                swap_endianness_32b(file_tmp_32b, nonuniform_data_imag);
                // $display("%h", nonuniform_data_imag);

                // Assign the values to the signals
                sample_in.en      <= `SD 1;
                sample_in.x_coord <= `SD nonuniform_coord_x;
                sample_in.y_coord <= `SD nonuniform_coord_y;
                sample_in.data    <= `SD {nonuniform_data_real, nonuniform_data_imag};

                @(posedge clock);
            end // while(!$feof(file_nonuniform_coords))
            sample_in.en      <= `SD '0;
            sample_in.x_coord <= `SD '0;
            sample_in.y_coord <= `SD '0;
            sample_in.data    <= `SD '0;
            $fclose(file_nonuniform_data); // once reading is finished, close the file
            $fclose(file_nonuniform_coords); // once reading is finished, close the file
            $display("Done reading input data.\n");
            $fdisplay(ppfile, "Done reading input data.");

            for(int i = 0; i <= 1000; i++) begin
                @(posedge clock); // extra cycles to account for module propagation delay
            end
        end else if(nufft_op == FFT_IFFT) begin
            output_file = $fopen($psprintf("%s/tmp/%s_output_ALL.out", OUTPUT_DATA_FOLDER, nufft_op.name().tolower()), "w");

            for(int x_accel_idx = 0; x_accel_idx < ACCEL_DIM; x_accel_idx++) begin
                for(int y_accel_idx = 0; y_accel_idx < ACCEL_DIM; y_accel_idx++) begin
                    $display("Reading Grid Data From %s", $psprintf("%s/matlab/input/%s/uniform_data/%s_len%0d_uniform_data_col_x%0d_y%0d.%0d.bin", INPUT_DATA_FOLDER, nufft_op.name().tolower(), nufft_op.name().tolower(), GRID_DIM_SIM, x_accel_idx, y_accel_idx, NUM_BUFFER_DATA));
                    $fdisplay(ppfile, "Reading Grid Data From %s", $psprintf("%s/matlab/input/%s/uniform_data/%s_len%0d_uniform_data_col_x%0d_y%0d.%0d.bin", INPUT_DATA_FOLDER, nufft_op.name().tolower(), nufft_op.name().tolower(), GRID_DIM_SIM, x_accel_idx, y_accel_idx, NUM_BUFFER_DATA));
                    file_uniform_data = $fopen($psprintf("%s/matlab/input/%s/uniform_data/%s_len%0d_uniform_data_col_x%0d_y%0d.%0d.bin", INPUT_DATA_FOLDER, nufft_op.name().tolower(), nufft_op.name().tolower(), GRID_DIM_SIM, x_accel_idx, y_accel_idx, NUM_BUFFER_DATA), "rb"); // "rb" means reading binary
                    iter_count        = 0;
                    nufft_op          = READ_MEM;
                    while(!$feof(file_uniform_data)) begin
                        // Get the real component
                        code = $fscanf(file_uniform_data,"%h", file_tmp_32b[31:16]);
                        code = $fscanf(file_uniform_data,"%h", file_tmp_32b[15:0]);
                        swap_endianness_32b(file_tmp_32b, uniform_data_real);
                        // $display("%h", nonuniform_data_real);

                        // Get the real component
                        code = $fscanf(file_uniform_data,"%h", file_tmp_32b[31:16]);
                        code = $fscanf(file_uniform_data,"%h", file_tmp_32b[15:0]);
                        swap_endianness_32b(file_tmp_32b, uniform_data_imag);
                        // $display("%h", nonuniform_data_imag);

                        // Assign the values to the signals
                        wr_en_in[x_accel_idx][y_accel_idx]   <= `SD 1;
                        wr_addr_in[x_accel_idx][y_accel_idx] <= `SD iter_count;
                        wr_data_in[x_accel_idx][y_accel_idx] <= `SD {uniform_data_real, uniform_data_imag};

                        @(posedge clock);
                        iter_count++;
                    end // while(!$feof(file_uniform_data))
                    wr_en_in[x_accel_idx][y_accel_idx]   <= `SD '0;
                    wr_addr_in[x_accel_idx][y_accel_idx] <= `SD '0;
                    wr_data_in[x_accel_idx][y_accel_idx] <= `SD '0;
                    $fclose(file_uniform_data); // once reading is finished, close the file
                    @(posedge clock);
                    nufft_op = FFT_IFFT;
                end
            end
            $display("Done Reading Grid Data.");
            $fdisplay(ppfile, "Done Reading Grid Data.");
            nufft_op <= `SD FFT_IFFT;

            fft_start_en_in <= `SD 1'b1; // set the pulse
            ifft_en_in      <= `SD 1'b0; // 0 for FFT, 1 for IFFT

            @(posedge clock);
            fft_start_en_in <= `SD 1'b0; // clear the pulse
            
            // Loop until the FFT is complete
            for(int i = 0; i <= 1000000; i++) begin
                if(fft_done_out)
                    break;

                @(posedge clock);
            end

            for(int i = 0; i <= 20; i++) begin
                @(posedge clock); // extra cycles to account for module propagation delay
            end

            $display("@@\n@@\nFFT Complete...Reading Grid Data...... %d Cycles\n@@\n@@\n", clock_count);

            for(int i = 0; i <= 500; i++) begin
                @(posedge clock); // extra cycles to account for module propagation delay
            end
            // rd_en_in <= `SD 1;
            // nufft_op <= `SD READ_MEM;
            // for(int i = 0; i < NUM_BUFFER_DATA; i++) begin
            //     rd_addr_in <= `SD i;
            //     @(posedge clock);
            // end
            // rd_en_in <= `SD 0;

            // Read out the data column-by-column
            // nufft_op = READ_MEM;
            // rd_en_in <= `SD 1;
            // for(int i = 0; i < ACCEL_DIM; i++) begin
            //     for(int j = 0; j < ACCEL_DIM; j++) begin
            //         // debug_file = $fopen($psprintf("%s/tmp/fft_ifft_output_x%0d_y%0d.out", OUTPUT_DATA_FOLDER, i, j), "w");
            //         for(int addr = 0; addr < NUM_BUFFER_DATA; addr++) begin
            //             rd_en_in   <= `SD 1;
            //             rd_addr_in <= `SD addr;
            //             @(posedge clock);
            //             rd_en_in <= `SD 0;

            //             while(!rd_en_out[i][j])
            //                 @(posedge clock);

            //             $fdisplay(output_file, "%b", rd_data_out[i][j]);
            //             // $fdisplay(debug_file, "%b", rd_data_out[i][j]);
            //         end
            //         // $fclose(debug_file);
            //     end
            // end
            // rd_en_in <= `SD 0;
            // @(posedge clock);
            // nufft_op = READ_MEM;

            // Read out the data tile-by-tile (this reduces read time; column-by-column is far slower)
            nufft_op = READ_MEM;
            rd_en_in <= `SD 1;
            for(int addr = 0; addr < NUM_BUFFER_DATA; addr++) begin
                rd_en_in   <= `SD 1;
                rd_addr_in <= `SD addr;
                @(posedge clock);
                rd_en_in <= `SD 0;

                // Loop until we see the data is ready (all bits will be true)
                while(!rd_en_out[0][0])
                    @(posedge clock);

                // Write all available points to the file (order is {tile1, tile2, ..., tileN})
                for(int i = 0; i < ACCEL_DIM; i++) begin
                    for(int j = 0; j < ACCEL_DIM; j++) begin
                        $fdisplay(output_file, "%b", rd_data_out[i][j]);
                    end
                end
            end
            rd_en_in <= `SD 0;
            @(posedge clock);
            nufft_op = READ_MEM;

            $fclose(output_file);
        end

        for(int i = 0; i <= 500; i++) begin
            @(posedge clock); // extra cycles to account for module propagation delay
        end

        $display("\n@@@Output should be invalid from this point\n\n");

        for(int i = 0; i <= 20; i++) begin
            @(posedge clock); // extra cycles to account for module propagation delay
        end

        $fclose(fileID12);

        $fclose(ppfile);
        $fclose(testfile);
        $fclose(output_file);
        $display("@@@DONE\n");
        $finish;
    end

endmodule
