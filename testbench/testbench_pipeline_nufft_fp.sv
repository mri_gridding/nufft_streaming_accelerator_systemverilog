/////////////////////////////////////////////////////////////////////////
//                                                                     //
//  Module name :  testbench.sv                                        //
//                                                                     //
//  Description :  Testbench for beamforming pipeline; this reads      //
//                 data files and distributes data to various modules. //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

`timescale 1ns/100ps
import custom_types::*;

module testbench;

    // Parameters
    parameter INPUT_BIT_WIDTH        = 32; // used as a #define for input files
    parameter ACCEL_DIM              = 8; // number of pipelines in the x dimension
    parameter MAX_GRID_DIM           = 1024; // max size of the grid in the x dimension
    parameter MEM_TILE_DIM           = (MAX_GRID_DIM/ACCEL_DIM);
    parameter MAX_INTERP_WINDOW_SIZE = ACCEL_DIM; // max size of the interpolation window in any single dimension
    parameter SAMPLE_BIT_WIDTH       = 32;
    parameter SAMPLE_COORD_BIT_WIDTH = 32;
    parameter WEIGHT_BIT_WIDTH       = 16;
    parameter WEIGHT_FRAC_WIDTH      = 14;
    parameter WEIGHT_OVERSAMP        = 32; // number of table samples between each whole coordinate
    parameter NUM_WEIGHTS            = 512; // (MAX_INTERP_WINDOW_SIZE*WEIGHT_OVERSAMP);
    parameter NUM_BANKS              = 4; // number of banks in each pipeline's data buffer
    parameter NUM_BANK_ENTRIES       = 4096; // number of entries in each data buffer bank
    parameter NUM_BUFFER_ENTRIES     = (NUM_BANKS*NUM_BANK_ENTRIES); // number of entries in each pipeline's data buffer
    parameter MAX_TRANSMIT_HOPS      = ACCEL_DIM;

    localparam INPUT_DATA_FOLDER     = "data";
    localparam OUTPUT_DATA_FOLDER    = "data";
    localparam NUM_INPUT_POINTS_SIM  = 460800; // GRIDDING = 460800; REGRIDDING = 103680; FFT_IFFT = N/A
    localparam X_ACCEL_IDX_SIM       = 0;
    localparam Y_ACCEL_IDX_SIM       = 0;
    localparam GRID_DIM_SIM          = 768; // GRIDDING = 768; REGRIDDING = 512; FFT_IFFT = 512 OR 1024
    localparam TILE_DIM_SIM          = (GRID_DIM_SIM/ACCEL_DIM);
    localparam INTERP_WINDOW_DIM_SIM = 6; // GRIDDING = 6; REGRIDDING = 6; FFT_IFFT = N/A
    localparam NUM_WEIGHTS_SIM       = 193; // GRIDDING = 193; REGRIDDING = 193; FFT_IFFT = 256 OR 512 (GRID_DIM_SIM/2)
    localparam NUM_BUFFER_DATA       = (MAX_GRID_DIM*MAX_GRID_DIM)/(ACCEL_DIM*ACCEL_DIM);


    function logic [15:0] swap_endianness_16b (
        // Inputs
        input logic [15:0] input_val,

        // Outputs
        output logic [15:0] output_val
    );

        begin
            output_val[15:8] = input_val[7:0];
            output_val[7:0]  = input_val[15:8];

            return output_val;
        end
    endfunction

    function logic [31:0] swap_endianness_32b (
        // Inputs
        input logic [31:0] input_val,

        // Outputs
        output logic [31:0] output_val
    );

        begin
            output_val[31:24] = input_val[7:0];
            output_val[23:16] = input_val[15:8];
            output_val[15:8]  = input_val[23:16];
            output_val[7:0]   = input_val[31:24];

            return output_val;
        end
    endfunction

    function fixed_to_float (
        // Inputs
        input logic [16-1:0] fixed_in,

        // Outputs
        output logic [32-1:0] float_out
    );

        logic              float_sign_next;
        logic [8-1:0]      float_exp_next;
        logic [16-2:0]     tmp_mant_next;
        logic [32-8-1-1:0] float_mant_next;

        begin
            tmp_mant_next = fixed_in[16-2:0];

            float_sign_next = '0;
            float_exp_next  = '0;
            float_mant_next = '0;

            // Cycle 0 - get absolute value of input
            if(fixed_in[16-1]) begin
                float_sign_next = 1;
                tmp_mant_next = (~fixed_in[16-2:0])+1;
            end

            if(fixed_in[16-2:0] != 0) begin
                for(int i = 0; i < 16-1; ++i) begin
                    if(tmp_mant_next[16-2-i]) begin
                        float_exp_next = 127 - i;
                        float_mant_next[32-8-1-1:9] = tmp_mant_next[16-3:0] << i;

                        break;
                    end
                end
            end

            float_out = {float_sign_next, float_exp_next, float_mant_next};

            return float_out;
        end

    endfunction

    // Testbench Signals
    logic        clock;
    logic        reset;
    logic [31:0] clock_count;

    // Pipeline Signals

    // Inputs
    operation_types                               nufft_op;
    logic                                         init_mem;
    logic                                         fft_start_en_in;
    logic                                         ifft_en_in;
    logic [$clog2(ACCEL_DIM)-1:0]                 accel_x_idx_in;
    logic [$clog2(ACCEL_DIM)-1:0]                 accel_y_idx_in;
    logic [$clog2(MAX_GRID_DIM):0]                grid_dim_in;
    logic [$clog2($clog2(MAX_GRID_DIM)):0]        fft_num_stages_in;
    logic [$clog2(MEM_TILE_DIM)-1:0]              tile_dim_in;
    logic [$clog2(MAX_INTERP_WINDOW_SIZE)-1:0]    window_dim_in;
    logic                                         weight_const_wr_en_in;
    logic [$clog2(NUM_WEIGHTS)-1:0]               weight_const_wr_addr_in;
    logic [(WEIGHT_BIT_WIDTH*2)-1:0]              weight_const_wr_data_in;
    logic                                         rd_en_in; // output a value (for reading final values out)
    logic [$clog2(NUM_BUFFER_ENTRIES)-1:0]        rd_addr_in;
    logic                                         wr_en_in; // write a value (for loading uniform grid data)
    logic [$clog2(NUM_BUFFER_ENTRIES)-1:0]        wr_addr_in;
    logic [(SAMPLE_BIT_WIDTH*2)-1:0]              wr_data_in;
    nonuniform_sample                             sample_in;
    logic [ACCEL_DIM-1:0][(SAMPLE_BIT_WIDTH*2):0] x_extern_data_in;
    logic [ACCEL_DIM-1:0][(SAMPLE_BIT_WIDTH*2):0] y_extern_data_in;

    // Outputs
    nonuniform_sample                sample_out;
    logic                            rd_en_out;
    logic [(SAMPLE_BIT_WIDTH*2)-1:0] rd_data_out;
    logic [(SAMPLE_BIT_WIDTH*2):0]   extern_data_out;
    logic                            fft_done_out;


    // Instantiate the pipeline
    pipeline_nufft_fp #(
        .ACCEL_DIM(ACCEL_DIM),
        .MAX_GRID_DIM(MAX_GRID_DIM),
        .MEM_TILE_DIM(MEM_TILE_DIM),
        .MAX_INTERP_WINDOW_SIZE(MAX_INTERP_WINDOW_SIZE),
        .SAMPLE_BIT_WIDTH(SAMPLE_BIT_WIDTH),
        .SAMPLE_COORD_BIT_WIDTH(SAMPLE_COORD_BIT_WIDTH),
        .WEIGHT_BIT_WIDTH(WEIGHT_BIT_WIDTH),
        .WEIGHT_OVERSAMP(WEIGHT_OVERSAMP),
        .NUM_WEIGHTS(NUM_WEIGHTS),
        .NUM_BANKS(NUM_BANKS),
        .NUM_BANK_ENTRIES(NUM_BANK_ENTRIES),
        .NUM_BUFFER_ENTRIES(NUM_BUFFER_ENTRIES),
        .MAX_TRANSMIT_HOPS(MAX_TRANSMIT_HOPS)
        ) pipeline_0 (

        // Inputs
        .clock(clock),
        .reset(reset),
        .nufft_op(nufft_op),
        .init_mem(init_mem),
        .fft_start_en_in(fft_start_en_in),
        .ifft_en_in(ifft_en_in),
        .accel_x_idx_in(accel_x_idx_in),
        .accel_y_idx_in(accel_y_idx_in),
        .grid_dim_in(grid_dim_in),
        .fft_num_stages_in(fft_num_stages_in),
        .tile_dim_in(tile_dim_in),
        .window_dim_in(window_dim_in),
        .weight_const_wr_en_in(weight_const_wr_en_in),
        .weight_const_wr_addr_in(weight_const_wr_addr_in),
        .weight_const_wr_data_in(weight_const_wr_data_in),
        .rd_en_in(rd_en_in),
        .rd_addr_in(rd_addr_in),
        .wr_en_in(wr_en_in),
        .wr_addr_in(wr_addr_in),
        .wr_data_in(wr_data_in),
        .sample_in(sample_in),
        .x_extern_data_in(x_extern_data_in),
        .y_extern_data_in(y_extern_data_in),

        // Outputs
        .sample_out(sample_out),
        .rd_en_out(rd_en_out),
        .rd_data_out(rd_data_out),
        .extern_data_out(extern_data_out),
        .fft_done_out(fft_done_out)

    );

    integer ppfile; // output file descriptor
    integer testfile; // output file descriptor
    integer output_file; // output file descriptor
    integer fileID12; // output file descriptor
    integer file_weight_data; // input file descriptor
    integer file_uniform_data; // input file descriptor
    integer file_nonuniform_data; // input file descriptor
    integer file_nonuniform_coords; // input file descriptor
    integer code; // return code for fscanf
    logic [31:0] nonuniform_coord_x; // assumes the file always contains 32-bit integers
    logic [31:0] nonuniform_coord_y; // assumes the file always contains 32-bit integers
    logic [31:0] nonuniform_data_real; // assumes the file always contains 32-bit integers
    logic [31:0] nonuniform_data_imag; // assumes the file always contains 32-bit integers
    logic [31:0] uniform_data_real; // assumes the file always contains 32-bit integers
    logic [31:0] uniform_data_imag; // assumes the file always contains 32-bit integers
    logic [31:0] table_data; // assumes the file always contains 32-bit integers
    logic [63:0] file_tmp_64b; // used as a temp holder for fscanf returns
    logic [31:0] file_tmp_32b; // used as a temp holder for fscanf returns
    logic [15:0] file_tmp_16b; // used as a temp holder for fscanf returns
    integer iter_count; // used as an index/counter in the fscanf loops
    integer debug_count; // used as an index/counter in the fscanf loops
    shortreal tmpshortreal;

    // Generate System Clock
    always begin
        #(`VERILOG_CLOCK_PERIOD/2.0);
        clock = ~clock;
    end

    // Count the number of posedges and number of instructions completed
    // till simulation ends
    always @(posedge clock) begin
        if(reset) begin
            clock_count <= `SD 0;
            // instr_count <= `SD 0;
        end else begin
            clock_count <= `SD (clock_count + 1);
            // instr_count <= `SD (instr_count + pipeline_completed_insts);
        end
    end

    // Print Debug Output
    always @(posedge clock) begin
        // if(pipeline_0.fp_prod_en) begin
        //     $fdisplay(fileID12, "%b\t%b", pipeline_0.fp_prod, pipeline_0.delayed_sample_2.data);
        // end
        // if(pipeline_0.fp_sum_en) begin
        //     $fdisplay(fileID12, "%b", pipeline_0.fp_sum);
        // end
        // if(~pipeline_0.fp_sum_en) begin
        //     $fdisplay(fileID12, "%b", pipeline_0.delayed_sample_3.data);
        // end
        // if(pipeline_0.ccm.wr_en) begin
        //     $fdisplay(fileID12, "clock_count:%d\t%h\t%h", clock_count+1, pipeline_0.ccm.wr_addr, pipeline_0.ccm.wr_data);
        // end

        // if(pipeline_0.fp_mult_complex_unit.mult_en_in) begin
        //     // $fdisplay(fileID12, "%b\t%b\t%b", pipeline_0.sel_weight_addr1, pipeline_0.sel_weight_addr2, pipeline_0.sel_fp_addr);
        //     $fdisplay(fileID12, "k1_next:%b", pipeline_0.fp_mult_complex_unit.k1_next);
        //     $fdisplay(fileID12, "k2_next:%b", pipeline_0.fp_mult_complex_unit.k2_next);
        //     $fdisplay(fileID12, "k3_next:%b", pipeline_0.fp_mult_complex_unit.k3_next);
        // end
        // if(pipeline_0.slct.sample_en_saved_2_next) begin
        //     $fdisplay(fileID12, "%0d\t%0d\t%0d\t%0d\t%0d", pipeline_0.slct.x_tile_idx_next, pipeline_0.slct.y_tile_idx_next, pipeline_0.slct.x_base_tile_idx, pipeline_0.slct.y_base_tile_idx, pipeline_0.slct.tile_dim);
        // end
        // if(pipeline_0.sel_en) begin
        //     $fdisplay(fileID12, "%b\t%b\t%b", pipeline_0.sel_weight_addr1, pipeline_0.sel_weight_addr2, pipeline_0.sel_fp_addr);
        // end
        // if(pipeline_0.fp_prod_en) begin
        //     $fdisplay(fileID12, "%b\t%b", pipeline_0.fp_prod, pipeline_0.delayed_sample_2.data);
        // end
        // if(pipeline_0.fft2_col_ctrl.w_en_next && debug_count < 12288) begin
        // if(pipeline_0.fft2_ctrl.w_en_next && ~pipeline_0.fft2_ctrl.fft_second_dim_en) begin
        //     debug_count = debug_count + 1;
        //     // $fdisplay(fileID12, "stage: %0d\tw_addr: %0d", pipeline_0.fft2_col_ctrl.stage_idx, pipeline_0.fft2_col_ctrl.w_addr_next);
        //     // $fdisplay(fileID12, "stage: %0d\ttile_idx_2: %0d", pipeline_0.fft2_col_ctrl.stage_idx, pipeline_0.fft2_col_ctrl.tile_idx_next);
        //     $fdisplay(fileID12, "stage: %0d\tw_addr: %0d", pipeline_0.fft2_ctrl.stage_idx, pipeline_0.fft2_ctrl.w_addr_next);
        //     $fdisplay(fileID12, "stage: %0d\ttile_idx_1: %0d", pipeline_0.fft2_ctrl.stage_idx, pipeline_0.fft2_ctrl.tile_idx_next);
        // end
        // if(pipeline_0.fft2_ctrl.w_en_next && pipeline_0.fft2_ctrl.fft_second_dim_en) begin
        //     debug_count = debug_count + 1;
        //     // $fdisplay(fileID12, "stage: %0d\tw_addr: %0d", pipeline_0.fft2_col_ctrl.stage_idx, pipeline_0.fft2_col_ctrl.w_addr_next);
        //     // $fdisplay(fileID12, "stage: %0d\ttile_idx_2: %0d", pipeline_0.fft2_col_ctrl.stage_idx, pipeline_0.fft2_col_ctrl.tile_idx_next);
        //     $fdisplay(fileID12, "stage: %0d\tw_addr: %0d", pipeline_0.fft2_ctrl.stage_idx, pipeline_0.fft2_ctrl.w_addr_next);
        //     $fdisplay(fileID12, "stage: %0d\ttile_idx_1: %0d", pipeline_0.fft2_ctrl.stage_idx, pipeline_0.fft2_ctrl.tile_idx_next);
        // end
        if(pipeline_0.fft_w_en && pipeline_0.fft_val_en) begin
            $fdisplay(fileID12, "tile_idx_1: %0d", pipeline_0.fft_val_addr);
        end
        if(~pipeline_0.fft_w_en && pipeline_0.fft_val_en) begin
            $fdisplay(fileID12, "tile_idx_2: %0d", pipeline_0.fft_val_addr);
        end
        // if(pipeline_0.fft2_col_ctrl.tile_idx_en_next && pipeline_0.fft2_col_ctrl.state == 2 && ~pipeline_0.fft2_col_ctrl.tile_idx_2_en) begin
        //     $fdisplay(fileID12, "stage: %0d\ttile_idx_1: %0d", pipeline_0.fft2_col_ctrl.stage_idx, pipeline_0.fft2_col_ctrl.tile_idx_next);
        // end
        // if(pipeline_0.fft2_col_ctrl.tile_idx_en_next && pipeline_0.fft2_col_ctrl.state == 2 && pipeline_0.fft2_col_ctrl.tile_idx_2_en) begin
        //     $fdisplay(fileID12, "stage: %0d\ttile_idx_2: %0d", pipeline_0.fft2_col_ctrl.stage_idx, pipeline_0.fft2_col_ctrl.tile_idx_next);
        // end
        // if(pipeline_0.fft2_col_ctrl.w_en_next && pipeline_0.fft2_col_ctrl.state == 3 && pipeline_0.fft2_col_ctrl.tile_idx_2_en) begin
        //     // $fdisplay(fileID12, "stage: %0d\tw_addr: %0d", pipeline_0.fft2_col_ctrl.stage_idx, pipeline_0.fft2_col_ctrl.w_addr_next);
        //     // $fdisplay(fileID12, "stage: %0d\ttile_idx_2: %0d", pipeline_0.fft2_col_ctrl.stage_idx, pipeline_0.fft2_col_ctrl.tile_idx_next);
        //     $fdisplay(fileID12, "stage: %0d\tw_addr: %b", pipeline_0.fft2_col_ctrl.stage_idx, pipeline_0.fft2_col_ctrl.w_addr_next);
        //     $fdisplay(fileID12, "stage: %0d\ttile_idx_2: %b", pipeline_0.fft2_col_ctrl.stage_idx, pipeline_0.fft2_col_ctrl.tile_idx_next);
        //     // $fdisplay(fileID12, "stage: %0d\tcontroller_idx_checker: %b", pipeline_0.fft2_col_ctrl.stage_idx, pipeline_0.fft2_col_ctrl.controller_idx_checker);
        // end
        // if(~pipeline_0.fft_w_en && pipeline_0.fft_val_en) begin
        //     $fdisplay(fileID12, "%b\t%b", pipeline_0.weight_data, pipeline_0.sample_data);
        // end
        // if(pipeline_0.fp_rd_en_out && pipeline_0.fft2_col_ctrl.stage_idx < 2) begin
        //     $fdisplay(fileID12, "%b", pipeline_0.fp_rd_data_out);
        // end
        // if(pipeline_0.fp_mult_en && debug_count < 4096) begin // debug_count should be 1024 per stage checked
        // if(pipeline_0.fp_mult_en) begin // debug_count should be 1024 per stage checked
        //     $fdisplay(fileID12, "%b\t%b", pipeline_0.weight_data, pipeline_0.sample_data);
        //     debug_count = debug_count + 1;
        // end
        // if(pipeline_0.fp_mult_complex_unit.k6_val_en && debug_count < 10240) begin // debug_count should be 1024 per stage checked
        //     // $fdisplay(fileID12, "k4: %b\tk5: %b\tk6: %b", pipeline_0.fp_mult_complex_unit.k4, pipeline_0.fp_mult_complex_unit.k5, pipeline_0.fp_mult_complex_unit.k6);
        //     $fdisplay(fileID12, "r: %b\ti: %b", pipeline_0.fp_mult_complex_unit.r_next, pipeline_0.fp_mult_complex_unit.i_next);
        //     debug_count = debug_count + 1;
        // end
        // if(pipeline_0.fp_prod_en && debug_count < 10240) begin // debug_count should be 1024 per stage checked
        //     $fdisplay(fileID12, "%b\t%b", pipeline_0.delayed_fp_rd_data_out3, pipeline_0.fp_prod);
        //     debug_count = debug_count + 1;
        // end
        // if(pipeline_0.fp_prod_en) begin // debug_count should be 1024 per stage checked
        //     $fdisplay(fileID12, "%b", pipeline_0.fp_prod);
        //     debug_count = debug_count + 1;
        // end
        // if(pipeline_0.fp_diff_en) begin // debug_count should be 1024 per stage checked
        //     $fdisplay(fileID12, "%b", pipeline_0.fp_diff);
        // end
        // if(pipeline_0.fp_sum_en) begin // debug_count should be 1024 per stage checked
        //     $fdisplay(fileID12, "%b", pipeline_0.fp_sum);
        //     debug_count = debug_count + 1;
        // end
    end // always @(posedge clock)

    // Print Pipeline Output
    always @(posedge clock) begin
        if(nufft_op == REGRIDDING) begin
            if(sample_out.en) begin
                $fdisplay(output_file, "%b", sample_out.data);
            end
        end
    end // always @(posedge clock)

    initial begin

        // $set_gate_level_monitoring("on");
        // $set_toggle_region(pipeline_0);
        // $toggle_start;

        // $monitor("Time:%4.0f reset:%b data_in:%d rd_data_out:%d", $time, reset, data_in, rd_data_out);
        ppfile   = $fopen("pipeline.out", "w");
        testfile = $fopen("test.out", "w");

        fileID12 = $fopen($psprintf("%s/tmp/random.out", OUTPUT_DATA_FOLDER), "w");
        debug_count = 0;

        // Initialize inputs
        clock <= `SD '0;
        reset <= `SD '0;

        nufft_op        <= `SD GRIDDING;
        init_mem        <= `SD '0;
        fft_start_en_in <= `SD '0;
        ifft_en_in      <= `SD '0;

        accel_x_idx_in    <= `SD X_ACCEL_IDX_SIM;
        accel_y_idx_in    <= `SD Y_ACCEL_IDX_SIM;
        grid_dim_in       <= `SD GRID_DIM_SIM;
        fft_num_stages_in <= `SD $clog2(GRID_DIM_SIM);
        tile_dim_in       <= `SD TILE_DIM_SIM;
        window_dim_in     <= `SD INTERP_WINDOW_DIM_SIM;

        weight_const_wr_en_in   <= `SD '0;
        weight_const_wr_addr_in <= `SD '0;
        weight_const_wr_data_in <= `SD '0;

        rd_en_in   <= `SD '0;
        rd_addr_in <= `SD '0;

        wr_en_in   <= `SD '0;
        wr_addr_in <= `SD '0;
        wr_data_in <= `SD '0;

        sample_in.en      <= `SD '0;
        sample_in.x_coord <= `SD '0;
        sample_in.y_coord <= `SD '0;
        sample_in.data    <= `SD '0;

        x_extern_data_in <= `SD '0;
        y_extern_data_in <= `SD '0;

        // Pulse the mem init signal
        $display("\n@@\n@@\nInitializing Memory......\n@@\n@@\n");
        init_mem <= `SD 1'b1;
        @(posedge clock);
        init_mem <= `SD 1'b0;

        @(posedge clock);

        // Pulse the reset signal
        $display("@@\n@@\nAsserting System reset......\n@@\n@@\n");
        reset <= `SD 1'b1;

        @(posedge clock);
        @(posedge clock);
        @(posedge clock);
        
        $display("Reading Weight Constants...");
        $fdisplay(ppfile, "Reading Weight Constants...");
        file_weight_data = $fopen($psprintf("%s/matlab/input/%s/%s_weight_data_fixed.%0d.bin", INPUT_DATA_FOLDER, nufft_op.name().tolower(), nufft_op.name().tolower(), NUM_WEIGHTS_SIM), "rb"); // "rb" means reading binary
        iter_count       = 0;
        while(!$feof(file_weight_data)) begin
            // Get the real component
            code = $fscanf(file_weight_data,"%h", file_tmp_16b);
            swap_endianness_16b(file_tmp_16b, table_data[31:16]);

            // Get the imag component
            code = $fscanf(file_weight_data,"%h", file_tmp_16b);
            swap_endianness_16b(file_tmp_16b, table_data[15:0]);

            // $display("%h", table_data);
            weight_const_wr_data_in <= `SD table_data;
            weight_const_wr_addr_in <= `SD iter_count;
            weight_const_wr_en_in   <= `SD 1;

            @(posedge clock);
            iter_count++;
        end // while(!$feof(file_weight_data))
        weight_const_wr_en_in   <= `SD '0;
        weight_const_wr_addr_in <= `SD '0;
        weight_const_wr_data_in <= `SD '0;
        $fclose(file_weight_data); // once reading is finished, close the file
        $display("Done Reading Weight Constants.");
        $fdisplay(ppfile, "Done Reading Weight Constants.");
        
        @(posedge clock);
        
        $display("@@\n@@\nConstant Initialization Complete...Deasserting System Reset...... %d Cycles\n@@\n@@\n", clock_count);
        reset <= `SD 1'b0;

        @(posedge clock);

        if(nufft_op == GRIDDING) begin
            output_file = $fopen($psprintf("%s/tmp/%s_output_x%0d_y%0d.out", OUTPUT_DATA_FOLDER, nufft_op.name().tolower(), X_ACCEL_IDX_SIM, Y_ACCEL_IDX_SIM), "w");

            $display("Reading input data...");
            $fdisplay(ppfile, "Reading input data...");
            file_nonuniform_coords = $fopen($psprintf("%s/matlab/input/%s/%s_nonuniform_coords.%0d.bin", INPUT_DATA_FOLDER, nufft_op.name().tolower(), nufft_op.name().tolower(), NUM_INPUT_POINTS_SIM), "rb"); // "rb" means reading binary
            file_nonuniform_data = $fopen($psprintf("%s/matlab/input/%s/%s_nonuniform_data.%0d.bin", INPUT_DATA_FOLDER, nufft_op.name().tolower(), nufft_op.name().tolower(), NUM_INPUT_POINTS_SIM), "rb"); // "rb" means reading binary
            while(!$feof(file_nonuniform_coords)) begin // read line by line until an "end of file" is reached
                // Get the x coordinate
                code = $fscanf(file_nonuniform_coords,"%h", file_tmp_32b[31:16]);
                code = $fscanf(file_nonuniform_coords,"%h", file_tmp_32b[15:0]);
                swap_endianness_32b(file_tmp_32b, nonuniform_coord_x);
                // $display("%h", nonuniform_coord_x);

                // Get the y coordinate
                code = $fscanf(file_nonuniform_coords,"%h", file_tmp_32b[31:16]);
                code = $fscanf(file_nonuniform_coords,"%h", file_tmp_32b[15:0]);
                swap_endianness_32b(file_tmp_32b, nonuniform_coord_y);
                // $display("%h", nonuniform_coord_y);

                // Get the real component
                code = $fscanf(file_nonuniform_data,"%h", file_tmp_32b[31:16]);
                code = $fscanf(file_nonuniform_data,"%h", file_tmp_32b[15:0]);
                swap_endianness_32b(file_tmp_32b, nonuniform_data_real);
                // $display("%h", nonuniform_data_real);

                // Get the real component
                code = $fscanf(file_nonuniform_data,"%h", file_tmp_32b[31:16]);
                code = $fscanf(file_nonuniform_data,"%h", file_tmp_32b[15:0]);
                swap_endianness_32b(file_tmp_32b, nonuniform_data_imag);
                // $display("%h", nonuniform_data_imag);

                // Assign the values to the signals
                sample_in.en      <= `SD 1;
                sample_in.x_coord <= `SD nonuniform_coord_x;
                sample_in.y_coord <= `SD nonuniform_coord_y;
                sample_in.data    <= `SD {nonuniform_data_real, nonuniform_data_imag};

                @(posedge clock);
            end // while(!$feof(file_nonuniform_coords))
            sample_in.en      <= `SD '0;
            sample_in.x_coord <= `SD '0;
            sample_in.y_coord <= `SD '0;
            sample_in.data    <= `SD '0;
            $fclose(file_nonuniform_data); // once reading is finished, close the file
            $fclose(file_nonuniform_coords); // once reading is finished, close the file
            $display("Done reading input data.\n");
            $fdisplay(ppfile, "Done reading input data.");

            for(int i = 0; i <= 500; i++) begin
                @(posedge clock); // extra cycles to account for module propagation delay
            end

            // Finished
            // $toggle_stop;
            // $toggle_report("PIPELINE.saif", 1.0e-9, pipeline_0);

            $display("@@\n@@\nData Stream Complete...Reading Grid Data...... %d Cycles\n@@\n@@\n", clock_count);

            // Read out the data
            nufft_op = READ_MEM;
            rd_en_in <= `SD 1;
            for(int addr = 0; addr < NUM_BUFFER_DATA; addr++) begin
                rd_en_in   <= `SD 1;
                rd_addr_in <= `SD addr;
                @(posedge clock);
                rd_en_in <= `SD 0;

                // Loop until we see the data is ready (all bits will be true)
                while(!rd_en_out)
                    @(posedge clock);

                $fdisplay(output_file, "%b", rd_data_out);
            end
            rd_en_in <= `SD 0;
            @(posedge clock);
        end else if(nufft_op == REGRIDDING) begin
            output_file = $fopen($psprintf("%s/tmp/%s_output_x%0d_y%0d.out", OUTPUT_DATA_FOLDER, nufft_op.name().tolower(), X_ACCEL_IDX_SIM, Y_ACCEL_IDX_SIM), "w");

            // nufft_op <= `SD READ_MEM;
            $display("Reading Grid Data From %s", $psprintf("%s/matlab/input/%s/uniform_data/%s_uniform_data_col_x%0d_y%0d.%0d.bin", INPUT_DATA_FOLDER, nufft_op.name().tolower(), nufft_op.name().tolower(), X_ACCEL_IDX_SIM, Y_ACCEL_IDX_SIM, NUM_BUFFER_DATA));
            $fdisplay(ppfile, "Reading Grid Data From %s", $psprintf("%s/matlab/input/%s/uniform_data/%s_uniform_data_col_x%0d_y%0d.%0d.bin", INPUT_DATA_FOLDER, nufft_op.name().tolower(), nufft_op.name().tolower(), X_ACCEL_IDX_SIM, Y_ACCEL_IDX_SIM, NUM_BUFFER_DATA));
            file_uniform_data = $fopen($psprintf("%s/matlab/input/%s/uniform_data/%s_uniform_data_col_x%0d_y%0d.%0d.bin", INPUT_DATA_FOLDER, nufft_op.name().tolower(), nufft_op.name().tolower(), X_ACCEL_IDX_SIM, Y_ACCEL_IDX_SIM, NUM_BUFFER_DATA), "rb"); // "rb" means reading binary
            iter_count        = 0;
            while(!$feof(file_uniform_data)) begin
                // Get the real component
                code = $fscanf(file_uniform_data,"%h", file_tmp_32b[31:16]);
                code = $fscanf(file_uniform_data,"%h", file_tmp_32b[15:0]);
                swap_endianness_32b(file_tmp_32b, uniform_data_real);
                // $display("%h", nonuniform_data_real);

                // Get the real component
                code = $fscanf(file_uniform_data,"%h", file_tmp_32b[31:16]);
                code = $fscanf(file_uniform_data,"%h", file_tmp_32b[15:0]);
                swap_endianness_32b(file_tmp_32b, uniform_data_imag);
                // $display("%h", nonuniform_data_imag);

                // Assign the values to the signals
                wr_en_in   <= `SD 1;
                wr_addr_in <= `SD iter_count;
                wr_data_in <= `SD {uniform_data_real, uniform_data_imag};

                @(posedge clock);
                iter_count++;
            end // while(!$feof(file_uniform_data))
            wr_en_in   <= `SD '0;
            wr_addr_in <= `SD '0;
            wr_data_in <= `SD '0;
            $fclose(file_uniform_data); // once reading is finished, close the file
            $display("Done Reading Grid Data.");
            $fdisplay(ppfile, "Done Reading Grid Data.");
            @(posedge clock);
            nufft_op <= `SD REGRIDDING;


            $display("Reading input data...");
            $fdisplay(ppfile, "Reading input data...");
            file_nonuniform_coords = $fopen($psprintf("%s/matlab/input/%s/%s_nonuniform_coords.%0d.bin", INPUT_DATA_FOLDER, nufft_op.name().tolower(), nufft_op.name().tolower(), NUM_INPUT_POINTS_SIM), "rb"); // "rb" means reading binary
            file_nonuniform_data   = $fopen($psprintf("%s/matlab/input/%s/nonuniform_data/%s_nonuniform_data_col_x%0d_y%0d.%0d.bin", INPUT_DATA_FOLDER, nufft_op.name().tolower(), nufft_op.name().tolower(), accel_x_idx_in, accel_y_idx_in, NUM_INPUT_POINTS_SIM), "rb"); // "rb" means reading binary
            while(!$feof(file_nonuniform_coords)) begin // read line by line until an "end of file" is reached
                // Get the x coordinate
                code = $fscanf(file_nonuniform_coords,"%h", file_tmp_32b[31:16]);
                code = $fscanf(file_nonuniform_coords,"%h", file_tmp_32b[15:0]);
                swap_endianness_32b(file_tmp_32b, nonuniform_coord_x);
                // $display("%h", nonuniform_coord_x);

                // Get the y coordinate
                code = $fscanf(file_nonuniform_coords,"%h", file_tmp_32b[31:16]);
                code = $fscanf(file_nonuniform_coords,"%h", file_tmp_32b[15:0]);
                swap_endianness_32b(file_tmp_32b, nonuniform_coord_y);
                // $display("%h", nonuniform_coord_y);

                // Get the real component
                code = $fscanf(file_nonuniform_data,"%h", file_tmp_32b[31:16]);
                code = $fscanf(file_nonuniform_data,"%h", file_tmp_32b[15:0]);
                swap_endianness_32b(file_tmp_32b, nonuniform_data_real);
                // $display("%h", nonuniform_data_real);

                // Get the real component
                code = $fscanf(file_nonuniform_data,"%h", file_tmp_32b[31:16]);
                code = $fscanf(file_nonuniform_data,"%h", file_tmp_32b[15:0]);
                swap_endianness_32b(file_tmp_32b, nonuniform_data_imag);
                // $display("%h", nonuniform_data_imag);

                // Assign the values to the signals
                sample_in.en      <= `SD 1;
                sample_in.x_coord <= `SD nonuniform_coord_x;
                sample_in.y_coord <= `SD nonuniform_coord_y;
                sample_in.data    <= `SD {nonuniform_data_real, nonuniform_data_imag};

                @(posedge clock);
            end // while(!$feof(file_nonuniform_coords))
            sample_in.en      <= `SD '0;
            sample_in.x_coord <= `SD '0;
            sample_in.y_coord <= `SD '0;
            sample_in.data    <= `SD '0;
            $fclose(file_nonuniform_data); // once reading is finished, close the file
            $fclose(file_nonuniform_coords); // once reading is finished, close the file
            $display("Done reading input data.\n");
            $fdisplay(ppfile, "Done reading input data.");

            for(int i = 0; i <= 500; i++) begin
                @(posedge clock); // extra cycles to account for module propagation delay
            end
        end

        for(int i = 0; i <= 20; i++) begin
            @(posedge clock); // extra cycles to account for module propagation delay
        end

        $display("\n@@@Output should be invalid from this point\n\n");

        $fclose(fileID12);

        $fclose(ppfile);
        $fclose(testfile);
        $fclose(output_file);

        $display("@@@DONE\n");
        $finish;
    end

endmodule
