/////////////////////////////////////////////////////////////////////////
//                                                                     //
//  Module name :  fp_mem_accum.sv                                     //
//                                                                     //
//  Description :  FP memory array, capable of accumulating FP values. //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

// `timescale 1ns/100ps

module fp_mem_accum #(
    parameter N                  = 32,
    parameter E                  = 8,
    parameter S                  = 1,
    parameter SAMPLE_BIT_WIDTH   = 32,
    parameter NUM_BANKS          = 4,
    parameter NUM_BANK_ENTRIES   = 4096,
    parameter NUM_BUFFER_ENTRIES = NUM_BANKS*NUM_BANK_ENTRIES
    ) (

        // Inputs
        input                                  clock,
        input                                  reset,
        input                                  init_mem,
        input                                  swap_comp_en_in,
        input                                  rd_en_in, // read/write enable
        input [$clog2(NUM_BUFFER_ENTRIES)-1:0] rd_addr_in, // read/write tile address (can be used for reading final values out)
        input                                  wr_en_in, // read/write enable
        input [$clog2(NUM_BUFFER_ENTRIES)-1:0] wr_addr_in, // read/write tile address (can be used for reading final values out)
        input [(SAMPLE_BIT_WIDTH*2)-1:0]       wr_data_in, // write data
        input                                  accum_en_in, // read/write enable
        input [$clog2(NUM_BUFFER_ENTRIES)-1:0] accum_addr_in, // read/write tile address (can be used for reading final values out)
        input [(SAMPLE_BIT_WIDTH*2)-1:0]       accum_data_in, // write data

        // Outputs
        output logic                            rd_en_out,
        output logic [(SAMPLE_BIT_WIDTH*2)-1:0] rd_data_out

    );


    // Local variables
    logic [(SAMPLE_BIT_WIDTH*2)-1:0]       rd_data_next, mem_output_tmp; // read value
    logic [$clog2(NUM_BUFFER_ENTRIES)-1:0] interp_addr, wr_addr, wr_addr_next, wr_addr_saved, wr_addr_saved_next; // read value
    logic                                  interp_en, wr_en, wr_en_next; // saved read/write enable
    logic                                  rd_en_saved, swap_comp_en; // saved read out enable
    logic [(SAMPLE_BIT_WIDTH*2)-1:0]       wr_data, wr_data_next, forward_data, forward_data_next, forward_data_2_next;
    logic [SAMPLE_BIT_WIDTH-1:0]           adder1_op1, adder1_op1_next, adder1_op2, adder1_op2_next, adder2_op1, adder2_op1_next, adder2_op2, adder2_op2_next;

    logic [(SAMPLE_BIT_WIDTH*2)-1:0]       rd_data; // read value
    logic [$clog2(NUM_BUFFER_ENTRIES)-1:0] accum_addr_in_saved;
    logic [(SAMPLE_BIT_WIDTH*2)-1:0]       accum_data_in_saved;
    logic                                  accum_en_in_saved;
    logic [(SAMPLE_BIT_WIDTH*2)-1:0]       interp_data_next;
    logic [$clog2(NUM_BUFFER_ENTRIES)-1:0] interp_addr_next;
    logic                                  interp_en_next;

    logic                            rd_en_out_next;
    logic [(SAMPLE_BIT_WIDTH*2)-1:0] rd_data_out_next;

    fadd_0cycle #(.N(N), .E(E), .S(S)) adder1 (
        .en(interp_en),
        .op1(adder1_op1),
        .op2(adder1_op2),

        .res_val(),
        .res(wr_data_next[(SAMPLE_BIT_WIDTH*2)-1:SAMPLE_BIT_WIDTH])
    );

    fadd_0cycle #(.N(N), .E(E), .S(S)) adder2 (
        .en(interp_en),
        .op1(adder2_op1),
        .op2(adder2_op2),

        .res_val(),
        .res(wr_data_next[SAMPLE_BIT_WIDTH-1:0] )
    );

    // Instantiate the accumulation array for this pipeline
    `ifdef USE_ACCUM_BANK
    logic [$clog2(NUM_BANKS)-1:0]                       bank_rd_idx, bank_rd_idx_saved, bank_wr_idx;
    logic [NUM_BANKS-1:0][(SAMPLE_BIT_WIDTH*2)-1:0]     bank_rd_data_next; // read value
    logic [NUM_BANKS-1:0][$clog2(NUM_BANK_ENTRIES)-1:0] bank_rd_addr, bank_wr_addr; // read value
    logic [NUM_BANKS-1:0]                               bank_wr_en; // saved read/write enable
    logic [NUM_BANKS-1:0]                               bank_rd_en; // saved read out enable
    logic [NUM_BANKS-1:0][(SAMPLE_BIT_WIDTH*2)-1:0]     bank_wr_data;
    

    `ifdef USE_ACCUM_SRAM_16
    // TSMC 16nm
    sram_2p_uhde_64b bank_array [NUM_BANKS-1:0] (

        // Inputs
        .CLK(clock), // clock - ADD "~" FOR SIMULATION; REMOVE FOR SYNTHESIS
        .CENA(~bank_rd_en), // read port chip enable
        .CENB(~bank_wr_en), // write port chip enable
        .AA(bank_rd_addr), // read port address
        .AB(bank_wr_addr), // write port address
        .DB(bank_wr_data), // write port data
        .STOV(1'b0), // When the STOV and STOVAB pins are HIGH, the internal clock pulse for port A is generated directly from the high phase of the external clock input
        .STOVAB(1'b0), // When the STOV pin is HIGH and the STOVAB pin is LOW, the internal clock pulse for port B is generated directly from the high phase of the external clock input.
        .EMA(3'b010), // extra margin adjustment - Austin uses 010; try 010 (default)
        .EMAW(2'b01), // similar to EMA; try 01 (default)
        .EMAS(1'b0), // default is LOW
        .EMAP(1'b0), // default is LOW
        .RET1N(1'b1), // retention mode; try 1?

        // Outputs
        .QA(bank_rd_data_next) // output data for A

    );
    `else
    mem_1r_1w #(
        .ENTRY_BIT_WIDTH((SAMPLE_BIT_WIDTH*2)),
        .NUM_ENTRIES(NUM_BANK_ENTRIES)
        ) bank_array [NUM_BANKS-1:0] (

        // Inputs
        .clock(clock),
        .init(init_mem),
        .rd_idx(bank_rd_addr), // read address
        .wr_idx(bank_wr_addr), // write address
        .wr_data(bank_wr_data), // write data
        .wr_en(bank_wr_en), // write enable

        // Outputs
        .rd_out(bank_rd_data_next) // read data

    );
    `endif
    `else
    mem_1r_1w #(
        .ENTRY_BIT_WIDTH((SAMPLE_BIT_WIDTH*2)),
        .NUM_ENTRIES(NUM_BUFFER_ENTRIES)
        ) data_array (

        // Inputs
        .clock(clock),
        .init(init_mem),
        .rd_idx(rd_en_in ? rd_addr_in : accum_addr_in), // read address
        .wr_idx(wr_addr), // write address
        .wr_data(wr_data), // write data
        .wr_en(wr_en), // write enable

        // Outputs
        .rd_out(mem_output_tmp) // read data

    );
    `endif


    // Accumulate the interpolation result with the current grid value
    always_comb begin
        `ifdef USE_ACCUM_BANK
        bank_wr_idx = wr_addr[$clog2(NUM_BUFFER_ENTRIES)-1:$clog2(NUM_BUFFER_ENTRIES)-$clog2(NUM_BANKS)];
        bank_rd_idx = rd_en_in ? rd_addr_in[$clog2(NUM_BUFFER_ENTRIES)-1:$clog2(NUM_BUFFER_ENTRIES)-$clog2(NUM_BANKS)] : accum_addr_in[$clog2(NUM_BUFFER_ENTRIES)-1:$clog2(NUM_BUFFER_ENTRIES)-$clog2(NUM_BANKS)];

        bank_rd_addr              = 0;
        bank_rd_addr[bank_rd_idx] = rd_en_in ? rd_addr_in[$clog2(NUM_BUFFER_ENTRIES)-$clog2(NUM_BANKS)-1:0] : accum_addr_in[$clog2(NUM_BUFFER_ENTRIES)-$clog2(NUM_BANKS)-1:0];
        bank_rd_en                = 0;
        bank_rd_en[bank_rd_idx]   = accum_en_in | rd_en_in;

        bank_wr_addr              = 0;
        bank_wr_addr[bank_wr_idx] = wr_addr[$clog2(NUM_BUFFER_ENTRIES)-$clog2(NUM_BANKS)-1:0];
        bank_wr_data              = 0;
        bank_wr_data[bank_wr_idx] = wr_data;
        bank_wr_en                = 0;
        bank_wr_en[bank_wr_idx]   = wr_en;

        if(swap_comp_en) begin
            rd_data_next = {bank_rd_data_next[bank_rd_idx_saved][SAMPLE_BIT_WIDTH-1:0], bank_rd_data_next[bank_rd_idx_saved][(SAMPLE_BIT_WIDTH*2)-1:SAMPLE_BIT_WIDTH]};
        end else begin
            rd_data_next = bank_rd_data_next[bank_rd_idx_saved];
        end
        `elsif
        if(swap_comp_en) begin
            rd_data_next = {mem_output_tmp[SAMPLE_BIT_WIDTH-1:0], mem_output_tmp[(SAMPLE_BIT_WIDTH*2)-1:SAMPLE_BIT_WIDTH]};
        end else begin
            rd_data_next = mem_output_tmp;
        end
        `endif

        rd_en_out_next   = rd_en_saved;
        rd_data_out_next = rd_en_saved ? rd_data_next : '0;

        wr_addr_next = interp_addr;
        wr_en_next   = interp_en;

        forward_data_next = wr_data;

        forward_data_2_next = forward_data;
        wr_addr_saved_next  = wr_addr;

        interp_data_next = accum_data_in_saved;
        interp_addr_next = accum_addr_in_saved;
        interp_en_next   = accum_en_in_saved;

        // Cycle 1 - Read value from the data array

        // Cycle 2 - Add input to the read value (check if read address is the same as previous; forward if needed)
        adder1_op2_next = interp_data_next[(SAMPLE_BIT_WIDTH*2)-1:SAMPLE_BIT_WIDTH];
        adder2_op2_next = interp_data_next[SAMPLE_BIT_WIDTH-1:0];
        if(accum_addr_in_saved == interp_addr) begin
            // Current write address matches saved read address; forward data!
            adder1_op1_next = wr_data_next[(SAMPLE_BIT_WIDTH*2)-1:SAMPLE_BIT_WIDTH];
            adder2_op1_next = wr_data_next[SAMPLE_BIT_WIDTH-1:0];
        end else if(wr_en && (accum_addr_in_saved == wr_addr)) begin
            adder1_op1_next = forward_data_next[(SAMPLE_BIT_WIDTH*2)-1:SAMPLE_BIT_WIDTH];
            adder2_op1_next = forward_data_next[SAMPLE_BIT_WIDTH-1:0];
        end else if(accum_en_in_saved && (accum_addr_in_saved == wr_addr_saved)) begin
            adder1_op1_next = forward_data_2_next[(SAMPLE_BIT_WIDTH*2)-1:SAMPLE_BIT_WIDTH];
            adder2_op1_next = forward_data_2_next[SAMPLE_BIT_WIDTH-1:0];
        end else begin
            adder1_op1_next = rd_data_next[(SAMPLE_BIT_WIDTH*2)-1:SAMPLE_BIT_WIDTH];
            adder2_op1_next = rd_data_next[SAMPLE_BIT_WIDTH-1:0];
        end

        // Cycle 3 - Write value to data array
    end // always_comb

    // synopsys sync_set_reset "reset"
    always_ff @(posedge clock) begin
        if(reset) begin
            rd_en_saved  <= `SD '0;
            interp_addr  <= `SD '0;
            interp_en    <= `SD '0;
            wr_data      <= `SD '0;
            wr_addr      <= `SD '0;
            wr_en        <= `SD '0;
            forward_data <= `SD '0;

            wr_addr_saved       <= `SD '0;
            rd_data             <= `SD '0;
            accum_addr_in_saved <= `SD '0;
            accum_data_in_saved <= `SD '0;
            accum_en_in_saved   <= `SD '0;
            swap_comp_en        <= `SD '0;

            adder1_op1 <= `SD '0;
            adder1_op2 <= `SD '0;
            adder2_op1 <= `SD '0;
            adder2_op2 <= `SD '0;

            `ifdef USE_ACCUM_BANK
            bank_rd_idx_saved <= `SD '0;
            `endif

            rd_en_out   <= `SD '0;
            rd_data_out <= `SD '0;
        end else begin
            rd_en_saved  <= `SD rd_en_in;
            interp_addr  <= `SD interp_addr_next;
            interp_en    <= `SD interp_en_next;
            wr_en        <= `SD wr_en_in ? wr_en_in   : wr_en_next;
            wr_addr      <= `SD wr_en_in ? wr_addr_in : wr_addr_next;
            wr_data      <= `SD wr_en_in ? wr_data_in : wr_data_next;
            forward_data <= `SD forward_data_next;

            wr_addr_saved       <= `SD wr_addr_saved_next;
            rd_data             <= `SD rd_data_next;
            accum_addr_in_saved <= `SD accum_addr_in;
            accum_data_in_saved <= `SD accum_data_in;
            accum_en_in_saved   <= `SD accum_en_in;
            swap_comp_en        <= `SD swap_comp_en_in;

            adder1_op1 <= `SD adder1_op1_next;
            adder1_op2 <= `SD adder1_op2_next;
            adder2_op1 <= `SD adder2_op1_next;
            adder2_op2 <= `SD adder2_op2_next;

            `ifdef USE_ACCUM_BANK
            bank_rd_idx_saved <= `SD bank_rd_idx;
            `endif

            rd_en_out   <= `SD rd_en_out_next;
            rd_data_out <= `SD rd_data_out_next;
        end // if(reset) begin
    end // always_ff @(posedge clock)
endmodule // fp_mem_accum
