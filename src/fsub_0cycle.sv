module fsub_0cycle #(parameter N = 32, parameter E = 8, parameter S = 1) (
    // Inputs
    input logic         en,
    input logic [N-1:0] op1,
    input logic [N-1:0] op2,

    // Outputs
    output logic         res_val,
    output logic [N-1:0] res
);

    logic [N-1:0] op2_negated;

    fadd_0cycle #(.N(N), .E(E), .S(S)) add (
        .en(en),
        .op1(op1),
        .op2(op2_negated),

        .res_val(res_val),
        .res(res)
    );

    assign op2_negated = {op2[N-1] ^ 1'b1, op2[N-2:0]};

endmodule : fsub_0cycle
