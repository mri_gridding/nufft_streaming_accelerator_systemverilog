/////////////////////////////////////////////////////////////////////////
//                                                                     //
//   Modulename :  sys_defs.vh                                         //
//                                                                     //
//  Description :  This file has the macro-defines for macros used in  //
//                 the pipeline design.                                //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

`ifndef __SYS_DEFS_VH__
`define __SYS_DEFS_VH__


//////////////////////////////////////////////
//                                          //
//              Pipeline Setup              //
//                                          //
//////////////////////////////////////////////
// Enable 16nm SRAM instances
`define USE_ACCUM_BANK // Use the banked regfile
// `define USE_SRAM_16 // Use the 16nm SRAM versions of the regfiles
// `define USE_ACCUM_SRAM_16 // Use the 16nm SRAM versions of the regfiles


//////////////////////////////////////////////
//                                          //
//           Testbench Attributes           //
//                                          //
//////////////////////////////////////////////
// Clock period for test bench
`define VERILOG_CLOCK_PERIOD 10.0

// Set the signal assignment delay; used for SRAMs that don't have a "fast" mode (IBM 45nm SOI)
`define SD #1

// Set up the SRAM for simulation
`define INITIALIZE_MEMORY 1
`define ARM_UD_MODEL

`endif
