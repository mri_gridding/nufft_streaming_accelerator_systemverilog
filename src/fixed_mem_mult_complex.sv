/////////////////////////////////////////////////////////////////////////
//                                                                     //
//  Module name :  fixed_mem_mult_complex.sv                           //
//                                                                     //
//  Description :  Returns the single or combined weight constant.     //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

// `timescale 1ns/100ps

module fixed_mem_mult_complex #(
    parameter SAMPLE_BIT_WIDTH       = 32,
    parameter WEIGHT_BIT_WIDTH       = 16,
    parameter WEIGHT_FRAC_WIDTH      = 14,
    parameter MAX_INTERP_WINDOW_SIZE = 8,
    parameter WEIGHT_OVERSAMP        = 32,
    parameter NUM_WEIGHTS            = 512, // MAX_INTERP_WINDOW_SIZE*WEIGHT_OVERSAMP,
    parameter NUM_BUFFER_ENTRIES     = 256
    ) (

        // Inputs
        input                            clock,
        input                            reset,
        input                            init_mem,
        input                            rd_only_en_in,
        input                            rd_en_in,
        input [$clog2(NUM_WEIGHTS)-1:0]  rd_addr1_in,
        input [$clog2(NUM_WEIGHTS)-1:0]  rd_addr2_in,
        input                            wr_en_in,
        input [$clog2(NUM_WEIGHTS)-1:0]  wr_addr_in,
        input [(WEIGHT_BIT_WIDTH*2)-1:0] wr_data_in,

        // Outputs
        output logic                            en_out,
        output logic [(WEIGHT_BIT_WIDTH*2)-1:0] prod_out

    );


    // Local variables
    logic en_in_saved;

    logic [(WEIGHT_BIT_WIDTH*2)-1:0] rd_data1, rd_data1_next; // first weight value
    logic [(WEIGHT_BIT_WIDTH*2)-1:0] rd_data2, rd_data2_next; // second weight value

    logic [WEIGHT_BIT_WIDTH-1:0]                   k1, k1_next;
    logic [WEIGHT_BIT_WIDTH-1:0]                   k2, k2_next;
    logic [WEIGHT_BIT_WIDTH-1:0]                   k3, k3_next;
    logic [WEIGHT_BIT_WIDTH-1:0]                   k4, k4_next;
    logic [WEIGHT_BIT_WIDTH-1:0]                   k5, k5_next;
    logic [WEIGHT_BIT_WIDTH-1:0]                   k6, k6_next;
    logic [WEIGHT_BIT_WIDTH-1:0]                   r_next;
    logic [WEIGHT_BIT_WIDTH-1:0]                   i_next;
    logic [WEIGHT_BIT_WIDTH+WEIGHT_FRAC_WIDTH-1:0] k4_temp, k5_temp, k6_temp;

    logic k3_val_en, k6_val_en, k6_val_en_next, i_val_en, i_val_en_next;

    logic [WEIGHT_BIT_WIDTH-1:0] weight_1_real, weight_1_real_next, weight_1_imag, weight_1_imag_next; // 
    logic [WEIGHT_BIT_WIDTH-1:0] weight_2_real, weight_2_real_next, weight_2_imag_next; // 

    logic                            en_out_next;
    logic [(WEIGHT_BIT_WIDTH*2)-1:0] prod_out_next;


    // Instantiate the weight storage array
    `ifdef USE_SRAM_16
    // TSMC 16nm
    sram_dp_uhde_32b interp_array_xy (

        // Inputs
        .CLK(clock), // clock - ADD "~" FOR SIMULATION; REMOVE FOR SYNTHESIS
        .CENA(~(wr_en_in | rd_en_in)), // read port chip enable
        .CENB(~rd_en_in), // write port chip enable
        .GWENA(~wr_en_in), // read port chip enable
        .GWENB(1'b1), // write port chip enable
        .AA(wr_en_in ? wr_addr_in : rd_addr1_in), // read port address
        .AB(rd_addr2_in), // write port address
        .DA(wr_data_in), // write port data
        .DB(32'b0), // write port data
        .STOV(1'b0), // When the STOV and STOVAB pins are HIGH, the internal clock pulse for port A is generated directly from the high phase of the external clock input
        .STOVAB(1'b0), // When the STOV pin is HIGH and the STOVAB pin is LOW, the internal clock pulse for port B is generated directly from the high phase of the external clock input.
        .EMA(3'b010), // extra margin adjustment - Austin uses 010; try 010 (default)
        .EMAP(1'b0), // default is LOW
        .EMAW(2'b01), // similar to EMA; try 01 (default)
        .EMAS(1'b0), // default is LOW
        .RET1N(1'b1), // retention mode; try 1?

        // Outputs
        .QA(rd_data1_next), // output data for A
        .QB(rd_data2_next) // output data for A

    );
    `else
    mem_2rw #(
        .ENTRY_BIT_WIDTH(WEIGHT_BIT_WIDTH*2), // store the entire complex value in the same slot
        .NUM_ENTRIES(NUM_WEIGHTS)
        ) interp_array_xy (

        // Inputs
        .clock(clock),
        .init(init_mem),
        .rwa_en(wr_en_in | rd_en_in), // port A enable
        .rwb_en(rd_en_in), // port B enable
        .wra_en(wr_en_in), // port A write enable
        .wrb_en(1'b0), // port B write enable
        .rwa_idx(wr_en_in ? wr_addr_in : rd_addr1_in), // read A address
        .rwb_idx(rd_addr2_in), // read B address
        .wra_data(wr_data_in), // port A write data
        .wrb_data(32'b0), // port B write data

        // Outputs
        .rda_out(rd_data1_next), // read A data
        .rdb_out(rd_data2_next) // read B data

    );
    `endif


    // Combine the two weight values to create the final weight
    always_comb begin
        k6_val_en_next = k3_val_en;
        i_val_en_next  = k6_val_en;

        weight_1_real_next = rd_data1[(WEIGHT_BIT_WIDTH*2)-1:WEIGHT_BIT_WIDTH];
        weight_1_imag_next = rd_data1[WEIGHT_BIT_WIDTH-1:0];
        weight_2_real_next = rd_data2[(WEIGHT_BIT_WIDTH*2)-1:WEIGHT_BIT_WIDTH];
        weight_2_imag_next = rd_data2[WEIGHT_BIT_WIDTH-1:0];

        // Cycle 0 - read weights from regfile/SRAM array

        // Cycle 1
        k1_next = $signed(weight_1_real_next) + $signed(weight_1_imag_next); // x_real + x_imag
        k2_next = $signed(weight_2_imag_next) - $signed(weight_2_real_next); // y_imag - y_real
        k3_next = $signed(weight_2_real_next) + $signed(weight_2_imag_next); // y_real + y_imag

        // Cycle 2
        k4_temp = $signed(weight_2_real) * $signed(k1); // y_real * k1
        k4_next = k4_temp[WEIGHT_BIT_WIDTH+WEIGHT_FRAC_WIDTH-1:WEIGHT_FRAC_WIDTH]; // y_real * k1
        k5_temp = $signed(weight_1_real) * $signed(k2); // x_real * k2
        k5_next = k5_temp[WEIGHT_BIT_WIDTH+WEIGHT_FRAC_WIDTH-1:WEIGHT_FRAC_WIDTH]; // x_real * k2
        k6_temp = $signed(weight_1_imag) * $signed(k3); // x_imag * k3
        k6_next = k6_temp[WEIGHT_BIT_WIDTH+WEIGHT_FRAC_WIDTH-1:WEIGHT_FRAC_WIDTH]; // x_imag * k3

        // Cycle 3
        r_next = $signed(k4) - $signed(k6); // k4 - k6
        i_next = $signed(k4) + $signed(k5); // k4 + k5

        en_out_next   = rd_only_en_in ? en_in_saved   : i_val_en;
        prod_out_next = rd_only_en_in ? rd_data1_next : {r_next, i_next};
    end // always_comb

    // synopsys sync_set_reset "reset"
    always_ff @(posedge clock) begin
        if(reset) begin
            rd_data1    <= `SD '0;
            rd_data2    <= `SD '0;
            en_in_saved <= `SD '0;

            k3_val_en <= `SD '0;
            k6_val_en <= `SD '0;
            i_val_en  <= `SD '0;

            k1            <= `SD '0;
            k2            <= `SD '0;
            k3            <= `SD '0;
            k4            <= `SD '0;
            k5            <= `SD '0;
            k6            <= `SD '0;
            weight_1_real <= `SD '0;
            weight_1_imag <= `SD '0;
            weight_2_real <= `SD '0;

            en_out   <= `SD '0;
            prod_out <= `SD '0;
        end else begin
            rd_data1    <= `SD rd_data1_next;
            rd_data2    <= `SD rd_data2_next;
            en_in_saved <= `SD rd_en_in;

            k3_val_en <= `SD en_in_saved;
            k6_val_en <= `SD k6_val_en_next;
            i_val_en  <= `SD i_val_en_next;

            k1            <= `SD k1_next;
            k2            <= `SD k2_next;
            k3            <= `SD k3_next;
            k4            <= `SD k4_next;
            k5            <= `SD k5_next;
            k6            <= `SD k6_next;
            weight_1_real <= `SD weight_1_real_next;
            weight_1_imag <= `SD weight_1_imag_next;
            weight_2_real <= `SD weight_2_real_next;

            en_out   <= `SD en_out_next;
            prod_out <= `SD prod_out_next;
        end // if(reset) begin
    end // always_ff @(posedge clock)
endmodule // fixed_mem_mult_complex
