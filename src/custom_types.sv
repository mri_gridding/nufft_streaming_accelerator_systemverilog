`timescale 1ns/100ps

package custom_types;
	localparam SAMPLE_BIT_WIDTH       = 32;
	localparam SAMPLE_COORD_BIT_WIDTH = 32;


    typedef enum {GRIDDING, REGRIDDING, FFT_IFFT, READ_MEM} operation_types;


    typedef struct {
		logic 							   en;
		logic [(SAMPLE_BIT_WIDTH*2)-1:0]   data;
		logic [SAMPLE_COORD_BIT_WIDTH-1:0] x_coord;
		logic [SAMPLE_COORD_BIT_WIDTH-1:0] y_coord;
    } nonuniform_sample;
endpackage
