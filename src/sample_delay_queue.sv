module sample_delay_queue #(
    parameter DELAY_CYCLES = 1
    )(

        // Inputs
        input                   clock,
        input                   reset,
        input nonuniform_sample datum_in,

        // Outputs
        output nonuniform_sample datum_out

);

    delay_queue #(
        .DELAY_CYCLES(DELAY_CYCLES),
        .ENTRY_BIT_WIDTH($bits(datum_in.en))
        ) en_dq (

        // Inputs
        .clock(clock),
        .reset(reset),
        .datum_in(datum_in.en),

        // Outputs
        .datum_out(datum_out.en)

    );

    delay_queue #(
        .DELAY_CYCLES(DELAY_CYCLES),
        .ENTRY_BIT_WIDTH($bits(datum_in.data))
        ) data_dq (

        // Inputs
        .clock(clock),
        .reset(reset),
        .datum_in(datum_in.data),

        // Outputs
        .datum_out(datum_out.data)

    );

    delay_queue #(
        .DELAY_CYCLES(DELAY_CYCLES),
        .ENTRY_BIT_WIDTH($bits(datum_in.x_coord))
        ) x_coord_dq (

        // Inputs
        .clock(clock),
        .reset(reset),
        .datum_in(datum_in.x_coord),

        // Outputs
        .datum_out(datum_out.x_coord)

    );

    delay_queue #(
        .DELAY_CYCLES(DELAY_CYCLES),
        .ENTRY_BIT_WIDTH($bits(datum_in.y_coord))
        ) y_coord_dq (

        // Inputs
        .clock(clock),
        .reset(reset),
        .datum_in(datum_in.y_coord),

        // Outputs
        .datum_out(datum_out.y_coord)

    );

endmodule