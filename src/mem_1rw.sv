/////////////////////////////////////////////////////////////////////////
//                                                                     //
//   Modulename :  mem_1rw.sv                                          //
//                                                                     //
//  Description :  This module creates the regfile for the interp      //
//                 table oversamp values.                              //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

// `timescale 1ns/100ps

module mem_1rw #(
    parameter ENTRY_BIT_WIDTH = 32,
    parameter NUM_ENTRIES     = 256
    ) (

        // Inputs
        input                           clock,
        input                           init,
        input                           rwa_en, // port A enable
        input                           wra_en, // write A enable
        input [$clog2(NUM_ENTRIES)-1:0] rwa_idx, // read A index
        input [ENTRY_BIT_WIDTH-1:0]     wra_data, // write data

        // Outputs
        output logic [ENTRY_BIT_WIDTH-1:0] rda_out // read A data

    );

    logic [NUM_ENTRIES-1:0][ENTRY_BIT_WIDTH-1:0] registers; // registers

    wire [ENTRY_BIT_WIDTH-1:0] rda_reg = registers[rwa_idx];

    //
    // Read/Write port A
    //
    // synopsys sync_set_reset "reset"
    always_ff @(posedge clock) begin
        if(init)
            registers <= `SD '0;

        if(rwa_en) begin
            if(wra_en) begin
                registers[rwa_idx] <= `SD wra_data;
                rda_out            <= `SD '0;
            end else begin
                rda_out <= `SD rda_reg;
            end
        end
    end

endmodule // mem_1rw
