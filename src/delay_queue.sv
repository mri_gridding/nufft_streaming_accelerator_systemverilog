module delay_queue #(
    parameter DELAY_CYCLES = 1,
    parameter ENTRY_BIT_WIDTH = 32
    )(

        // Inputs
        input                       clock,
        input                       reset,
        input [ENTRY_BIT_WIDTH-1:0] datum_in,

        // Outputs
        output logic [ENTRY_BIT_WIDTH-1:0] datum_out

);

    logic [DELAY_CYCLES-1:0][ENTRY_BIT_WIDTH-1:0] datum_buf;

    //synthesis translate_off
    initial begin
        if(DELAY_CYCLES < 1) begin
            $fatal("\n\nError: DELAY_CYCLES must be greater than 0! DELAY_CYCLES is %0d.\n\n", DELAY_CYCLES);
        end 
    end 
    //synthesis translate_on

    assign datum_out = datum_buf[DELAY_CYCLES-1];

    always_ff @(posedge clock) begin
        if(reset) begin
            datum_buf <= `SD '0;
        end else begin
            datum_buf[0] <= `SD datum_in;

            for(int p = 1; p < DELAY_CYCLES; ++p) begin
                datum_buf[p] <= `SD datum_buf[p-1];
            end
        end
    end

endmodule