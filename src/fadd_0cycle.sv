module fadd_0cycle #(parameter N = 32, parameter E = 8, parameter S = 1) (
    // Inputs
    input logic         en,
    input logic [N-1:0] op1,
    input logic [N-1:0] op2,

    // Outputs
    output logic         res_val,
    output logic [N-1:0] res
);

    logic             op1_sign_next, op2_sign_next;
    logic [E-1:0]     op1_exp_next, op2_exp_next;
    logic [N-E-S-1:0] op1_mant_next, op2_mant_next;
    logic [E-1:0]     tmp_exp_next;
    logic [N-E:0]     tmp_mant_next;
    logic             res_val_next;
    logic             res_sign_next;
    logic [E-1:0]     res_exp_next;
    logic [N-E-S-1:0] res_mant_next;

    assign res_val = en;
    assign res     = {res_sign_next, res_exp_next, res_mant_next};

    always_comb begin
        op1_sign_next = op1[N-1];
        op1_exp_next  = op1[N-S-1:N-E-1];
        op1_mant_next = op1[N-E-S-1:0];

        op2_sign_next = op2[N-1];
        op2_exp_next  = op2[N-S-1:N-E-1];
        op2_mant_next = op2[N-E-S-1:0];

        tmp_exp_next  = '0;
        tmp_mant_next = '0;

        res_val_next  = en;
        res_sign_next = '0;
        res_exp_next  = '0;
        res_mant_next = '0;

        // Cycle 0 - sort operands (find larger) and calculate exponent difference
        if(en) begin
            if((op2[N-S-1:N-E-1] > op1[N-S-1:N-E-1]) || ((op2[N-S-1:N-E-1] == op1[N-S-1:N-E-1]) && (op2[N-E-S-1:0] > op1[N-E-S-1:0]))) begin
                op1_sign_next = op2[N-1];
                op1_exp_next  = op2[N-S-1:N-E-1];
                op1_mant_next = op2[N-E-S-1:0];

                op2_sign_next = op1[N-1];
                op2_exp_next  = op1[N-S-1:N-E-1];
                op2_mant_next = op1[N-E-S-1:0];
            end

            tmp_exp_next = op1_exp_next;

            // Check if either operand is zero
            if((op1[N-S-1:N-E-1] == 0 && op1[N-E-S-1:0] == 0) || (op2[N-S-1:N-E-1] == 0 && op2[N-E-S-1:0] == 0)) begin
                tmp_mant_next = {1'b1, op1_mant_next};
            end else if(op1_sign_next == op2_sign_next) begin
                tmp_mant_next = {1'b1, op1_mant_next} + ({1'b1, op2_mant_next} >> (op1_exp_next - op2_exp_next));
            end else begin
                tmp_mant_next = {1'b1, op1_mant_next} - ({1'b1, op2_mant_next} >> (op1_exp_next - op2_exp_next));
            end

            if(tmp_mant_next[N-E]) begin
                res_exp_next  = tmp_exp_next + 1;
                res_mant_next = tmp_mant_next[N-E:1];
            end else begin
                for(int i = 0; i < N-E-1; ++i) begin
                    if(tmp_mant_next[N-E-1-i]) begin
                        // Check for underflow
                        if(i > tmp_exp_next) begin
                            res_exp_next = 1;
                        end else begin
                            res_exp_next  = tmp_exp_next - i;
                            res_mant_next = tmp_mant_next[N-E-S-1:0] << i;
                        end

                        break;
                    end
                end
            end

            if((res_exp_next == 0) && (res_mant_next == 0)) begin
                res_sign_next = 0;
            end else begin
                res_sign_next = op1_sign_next;
            end
        end
    end

endmodule : fadd_0cycle
