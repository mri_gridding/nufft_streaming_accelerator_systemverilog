/////////////////////////////////////////////////////////////////////////
//                                                                     //
//  Module name :  fft2_controller.sv                                  //
//                                                                     //
//  Description :  Calculates addresses for weights and both values    //
//                 in the IFFT2/FFT2 operations and notifies pipelines //
//                 of data movement requirements.                      //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

// `timescale 1ns/100ps

module fft2_controller #(
    parameter MAX_GRID_DIM       = 1024,
    parameter ACCEL_DIM          = 8,
    parameter MEM_TILE_DIM       = (MAX_GRID_DIM/ACCEL_DIM),
    parameter NUM_WEIGHTS        = 512,
    parameter NUM_BUFFER_ENTRIES = 16384,
    parameter MAX_TRANSMIT_HOPS  = ACCEL_DIM
    ) (
        // Inputs
        input logic                                  clock,
        input logic                                  reset,
        input logic                                  start_in, // initiate the FFT2/IFFT2 operation
        input logic [$clog2(MAX_GRID_DIM):0]         fft_len_in,
        input logic [$clog2($clog2(MAX_GRID_DIM)):0] fft_num_stages_in,
        input logic [$clog2(ACCEL_DIM)-1:0]          x_controller_idx_in,
        input logic [$clog2(ACCEL_DIM)-1:0]          y_controller_idx_in,

        // Outputs
        output logic                                  w_en_out, // indicates if the weight address is valid
        output logic [$clog2(NUM_WEIGHTS)-1:0]        w_addr_out, // address of the weight
        output logic                                  val_en_out, // indicates if the val address is valid
        output logic [$clog2(NUM_BUFFER_ENTRIES)-1:0] val_addr_out, // address of the butterfly value
        output logic [$clog2(MAX_TRANSMIT_HOPS)-1:0]  col_transmit_dist_out, // distance to transmit values in the x dimension
        output logic                                  fft_second_dim_en_out, // indicates if the first or second dimension is being processed
        output logic                                  done_out // FFT complete
    );


    // Local variables
    enum {IDLE, INIT_COL, COL_EASY, COL_HARD, INIT_ROW, ROW_EASY, ROW_HARD} state, state_next; // COL is x-dim
    enum {SET_IDLE, SET_ACTIVE}                                             set_state, set_state_next;
    enum {ITER_IDLE, ITER_ACTIVE}                                           iter_state, iter_state_next;

    logic [$clog2(MAX_GRID_DIM):0]             fft_len, fft_len_next;
    logic [$clog2(MEM_TILE_DIM):0]             step_val, step_val_next;
    logic [$clog2(MAX_GRID_DIM*2*ACCEL_DIM):0] end_val, end_val_next;
    logic [$clog2(MAX_TRANSMIT_HOPS)-1:0]      col_transmit_dist, col_transmit_dist_next; // distance to transmit values in the x dimension

    logic [$clog2((MAX_GRID_DIM/ACCEL_DIM)*MEM_TILE_DIM):0] set_idx, set_idx_next, set_idx_max, set_idx_max_next, set_idx_inc, set_idx_inc_next;
    logic [$clog2(NUM_BUFFER_ENTRIES):0]                    iter1, iter1_next, iter2, iter2_next, saved_val, saved_val_next;

    logic [$clog2(NUM_BUFFER_ENTRIES)-1:0] tile_idx_next;
    logic                                  tile_idx_2_en, tile_idx_2_en_next;

    logic [$clog2(MAX_GRID_DIM):0] stage_idx, stage_idx_next, num_easy_stages, num_easy_stages_next;

    logic [$clog2(MAX_GRID_DIM*ACCEL_DIM):0] offset, offset_next;

    logic [$clog2(NUM_WEIGHTS)-1:0] w_addr_next, w_addr_tmp, w_addr_tmp2;

    logic set_start, set_start_next, iter_start, iter_start_next;
    logic sets_processed, sets_processed_next, iters_processed, iters_processed_next;

    logic                          iter_en, w_en_next, tile_idx_en_next, fft_second_dim_en, fft_second_dim_en_next, done_next;
    logic [$clog2(MAX_GRID_DIM):0] fft_num_stages, fft_num_stages_next;
    logic [$clog2(ACCEL_DIM)-1:0]  x_controller_idx, y_controller_idx;


    always_comb begin
        fft_len_next         = fft_len;
        fft_num_stages_next  = fft_num_stages;
        num_easy_stages_next = num_easy_stages;

        end_val_next           = end_val;
        col_transmit_dist_next = col_transmit_dist;
        fft_second_dim_en_next = fft_second_dim_en;

        // Set loop variables
        state_next = state;

        // Stage loop variables
        stage_idx_next = stage_idx;

        // Set loop variables
        set_idx_next     = set_idx;
        set_idx_max_next = set_idx_max;
        set_idx_inc_next = set_idx_inc;

        // Iter loop variables
        iter1_next     = iter1;
        iter2_next     = iter2;
        saved_val_next = saved_val;
        step_val_next  = step_val;
        offset_next    = offset;

        set_state_next      = set_state;
        set_start_next      = '0;
        sets_processed_next = '0;

        iter_state_next      = iter_state;
        iters_processed_next = '0;
        iter_start_next      = '0;

        iter_en          = '0;
        w_en_next        = '0;
        tile_idx_en_next = '0;
        w_addr_tmp       = '0;
        w_addr_tmp2      = '0;
        w_addr_next      = '0;
        tile_idx_next    = '0;
        done_next        = '0;

        tile_idx_2_en_next = tile_idx_2_en;

        case(state)
            IDLE: begin
                fft_second_dim_en_next = '0;
                step_val_next          = '0;

                col_transmit_dist_next = '0;
                end_val_next           = '0; // fft_len/ACCEL_DIM
                offset_next            = '0; // fft_len/2/ACCEL_DIM

                set_idx_max_next = '0;
                set_idx_inc_next = '0;
                set_start_next   = '0;

                stage_idx_next = '0;

                if(start_in) begin
                    fft_len_next         = fft_len_in;
                    fft_num_stages_next  = fft_num_stages_in;
                    num_easy_stages_next = fft_num_stages_in - $clog2(ACCEL_DIM);

                    done_next = '0;

                    state_next = INIT_COL;
                end
            end
            INIT_COL: begin
                // $display("INIT_COL");
                step_val_next          = 1;
                fft_second_dim_en_next = '0;

                assert (set_state == SET_IDLE) else $error("set_state should be IDLE, but is %s", set_state.name());
                assert (iter_state == ITER_IDLE) else $error("iter_state should be IDLE, but is %s", iter_state.name());

                state_next = COL_EASY;

                col_transmit_dist_next = '0;
                end_val_next           = fft_len >> $clog2(ACCEL_DIM); // fft_len/ACCEL_DIM
                offset_next            = fft_len >> ($clog2(ACCEL_DIM) + 1); // fft_len/2/ACCEL_DIM

                set_idx_max_next = {fft_len, {$clog2(MEM_TILE_DIM/ACCEL_DIM){1'b0}}};
                set_idx_inc_next = MEM_TILE_DIM;
                set_start_next   = 1'b1;
            end
            COL_EASY: begin
                if(sets_processed) begin
                    // $display("COL_EASY");
                    assert (set_state == SET_IDLE) else $error("set_state should be IDLE, but is %s", set_state.name());
                    assert (iter_state == ITER_IDLE) else $error("iter_state should be IDLE, but is %s", iter_state.name());

                    offset_next    = offset >> 1;
                    stage_idx_next = stage_idx+1;
                    set_start_next = 1'b1;
                    
                    if(stage_idx == num_easy_stages-1) begin
                        col_transmit_dist_next = 4;
                        offset_next            = '0;

                        state_next = COL_HARD;

                        // DEBUG
                        // stage_idx_next = '0;
                        // state_next     = IDLE;

                        // set_start_next = 1'b0;
                        // done_next      = 1'b1;
                        // DEBUG
                    end
                end
            end
            COL_HARD: begin
                if(sets_processed) begin
                    // $display("COL_HARD");
                    assert (set_state == SET_IDLE) else $error("set_state should be IDLE, but is %s", set_state.name());
                    assert (iter_state == ITER_IDLE) else $error("iter_state should be IDLE, but is %s", iter_state.name());
                    
                    stage_idx_next         = stage_idx+1;
                    set_start_next         = 1'b1;
                    col_transmit_dist_next = col_transmit_dist >> 1;

                    if(stage_idx == fft_num_stages-1) begin
                        stage_idx_next = '0;
                        // state_next     = IDLE;
                        state_next     = INIT_ROW;

                        set_start_next = 1'b0;
                        // done_next      = 1'b1;
                    end
                end
            end
            INIT_ROW: begin
                // $display("INIT_ROW");
                step_val_next          = MEM_TILE_DIM;
                fft_second_dim_en_next = 1;

                assert (set_state == SET_IDLE) else $error("set_state should be IDLE, but is %s", set_state.name());
                assert (iter_state == ITER_IDLE) else $error("iter_state should be IDLE, but is %s", iter_state.name());

                state_next = ROW_EASY;

                col_transmit_dist_next = '0;
                end_val_next           = fft_len << ($clog2(ACCEL_DIM) + 1); // fft_len*2*ACCEL_DIM
                offset_next            = fft_len << $clog2(ACCEL_DIM); // fft_len*ACCEL_DIM

                set_idx_max_next = fft_len >> $clog2(ACCEL_DIM);
                set_idx_inc_next = 1;
                set_start_next   = 1'b1;
            end
            ROW_EASY: begin
                if(sets_processed) begin
                    // $display("ROW_EASY");
                    assert (set_state == SET_IDLE) else $error("set_state should be IDLE, but is %s", set_state.name());
                    assert (iter_state == ITER_IDLE) else $error("iter_state should be IDLE, but is %s", iter_state.name());

                    offset_next    = offset >> 1;
                    stage_idx_next = stage_idx+1;
                    set_start_next = 1'b1;
                    
                    if(stage_idx == num_easy_stages-1) begin
                        col_transmit_dist_next = 4;
                        offset_next            = '0;

                        // state_next = IDLE;
                        state_next = ROW_HARD;
                    end
                end
            end
            ROW_HARD: begin
                if(sets_processed) begin
                    // $display("ROW_HARD");
                    assert (set_state == SET_IDLE) else $error("set_state should be IDLE, but is %s", set_state.name());
                    assert (iter_state == ITER_IDLE) else $error("iter_state should be IDLE, but is %s", iter_state.name());

                    stage_idx_next         = stage_idx+1;
                    set_start_next         = 1'b1;
                    col_transmit_dist_next = col_transmit_dist >> 1;

                    if(stage_idx == fft_num_stages-1) begin
                        stage_idx_next = '0;
                        state_next     = IDLE;

                        set_start_next = 1'b0;
                        done_next      = 1'b1;
                    end
                end
            end
        endcase // state



        case(set_state)
            SET_IDLE: begin
                if(set_start) begin
                    iter1_next     = offset;
                    iter2_next     = '0;
                    saved_val_next = offset;

                    iter_start_next = 1'b1;
                    set_idx_next    = '0;

                    set_state_next = SET_ACTIVE;
                end
            end
            SET_ACTIVE: begin
                sets_processed_next = 1'b0;

                if(iters_processed) begin
                    if((set_idx + set_idx_inc) < set_idx_max) begin
                        iter1_next     = offset;
                        iter2_next     = '0;
                        saved_val_next = offset;

                        iter_start_next = 1'b1;
                        set_idx_next    = set_idx + set_idx_inc;
                    end else begin
                        sets_processed_next = 1'b1;

                        assert (set_start == 0) else $error("set_start should be 0, but is %0d", set_start);
                        assert (set_start_next == 0) else $error("set_start_next should be 0, but is %0d", set_start_next);

                        set_state_next = SET_IDLE;
                    end
                end
            end
        endcase // set_state



        case(iter_state)
            ITER_IDLE: begin
                if(iter_start) begin
                    iter_state_next = ITER_ACTIVE;
                    assert (iter1 < end_val) else $error("iter1 (%0d) should be less than end_val (%0d)", iter1, end_val);
                end
            end
            ITER_ACTIVE: begin
                iters_processed_next = 1'b0;

                if(iter1 < end_val) begin
                    // Toggle the enable bit so we know which idx to output next cycle
                    tile_idx_2_en_next = tile_idx_2_en + 1;

                    // Mark the tile idx output as enabled
                    tile_idx_en_next = 1'b1;

                    if(state == COL_EASY || state == ROW_EASY) begin
                        // Output either the first val addr along with the weight addr or just the second val addr
                        if(tile_idx_2_en) begin
                            tile_idx_next = iter2+set_idx;

                            if(iter2+step_val == saved_val) begin
                                iter1_next     = iter1+step_val+offset;
                                iter2_next     = iter2+step_val+offset;
                                saved_val_next = iter1+step_val+offset;
                            end else begin
                                iter1_next = iter1+step_val;
                                iter2_next = iter2+step_val;
                            end
                        end else begin
                            tile_idx_next = iter1+set_idx;

                            w_en_next = 1'b1;

                            // tile_idx_2[6:(7-stage)] for stage > 0; else 0 (6:7 is zero bits)
                            if(stage_idx > 0) begin
                                if(~fft_second_dim_en) begin
                                    w_addr_next = tile_idx_next[6:0] >> (7-(stage_idx+1));
                                end else begin
                                    w_addr_next = tile_idx_next[13:7] >> (7-(stage_idx+1));
                                end
                            end else begin
                                w_addr_next = '0;
                            end
                        end
                    end else begin
                        if(iter2+step_val == saved_val) begin
                            iter1_next     = iter1+step_val+offset;
                            iter2_next     = iter2+step_val+offset;
                            saved_val_next = iter1+step_val+offset;
                        end else begin
                            iter1_next = iter1+step_val;
                            iter2_next = iter2+step_val;
                        end

                        if(~fft_second_dim_en) begin
                            if(x_controller_idx[(fft_num_stages-1)-stage_idx] == 1'b1) begin
                                tile_idx_next = iter2+set_idx;

                                w_en_next = 1'b1;
                                
                                // 7: {tile_idx_2[6:0]}. 8: {tile_idx_2[6:0], x_accel_idx[2]}. 9: {tile_idx_2[6:0], x_accel_idx[2:1]}
                                w_addr_tmp[6:0]  = tile_idx_next[6:0];
                                w_addr_tmp2[2:0] = x_controller_idx[2:0];
                                w_addr_next      = (w_addr_tmp << ((stage_idx+1)-7)) | (w_addr_tmp2 >> ((fft_num_stages) - stage_idx));
                            end else begin
                                tile_idx_next = iter1+set_idx;
                            end
                        end else begin // if(fft_second_dim_en)
                            if(y_controller_idx[(fft_num_stages-1)-stage_idx] == 1'b1) begin
                                tile_idx_next = iter2+set_idx;

                                w_en_next = 1'b1;
                                
                                // 7: {tile_idx_2[13:7]}. 8: {tile_idx_2[13:7], y_accel_idx[2]}. 9: {tile_idx_2[13:7], y_accel_idx[2:1]}
                                w_addr_tmp[6:0]  = tile_idx_next[13:7];
                                w_addr_tmp2[2:0] = y_controller_idx[2:0];
                                w_addr_next      = (w_addr_tmp << ((stage_idx+1)-7)) | (w_addr_tmp2 >> ((fft_num_stages) - stage_idx));
                            end else begin
                                tile_idx_next = iter1+set_idx;
                            end
                        end
                    end

                    iter_en = 1;
                end else begin
                    iters_processed_next = 1'b1;

                    assert (iter_start == 0) else $error("iter_start should be 0, but is %0d", iter_start);
                    assert (iter_start_next == 0) else $error("iter_start_next should be 0, but is %0d", iter_start_next);

                    iter_state_next = ITER_IDLE;
                end
            end
        endcase // iter_state
    end // always_comb

    // synopsys sync_set_reset "reset"
    always_ff @(posedge clock) begin
        if(reset) begin
            fft_len          <= `SD '0;
            fft_num_stages   <= `SD '0;
            num_easy_stages  <= `SD '0;
            x_controller_idx <= `SD x_controller_idx_in;
            y_controller_idx <= `SD y_controller_idx_in;

            step_val <= `SD '0;
            end_val  <= `SD '0;

            state     <= `SD IDLE;
            stage_idx <= `SD '0;
            iter1     <= `SD '0;
            iter2     <= `SD '0;
            saved_val <= `SD '0;

            set_state       <= `SD SET_IDLE;
            set_idx_max     <= `SD '0;
            set_idx_inc     <= `SD '0;
            set_start       <= `SD '0;
            set_idx         <= `SD '0;
            sets_processed  <= `SD '0;
            iter_state      <= `SD ITER_IDLE;
            iter_start      <= `SD '0;
            iters_processed <= `SD '0;
            offset          <= `SD '0;

            tile_idx_2_en <= `SD '0;

            col_transmit_dist <= `SD '0;
            fft_second_dim_en <= `SD '0;

            w_en_out              <= `SD '0;
            w_addr_out            <= `SD '0;
            val_en_out            <= `SD '0;
            val_addr_out          <= `SD '0;
            col_transmit_dist_out <= `SD '0;
            fft_second_dim_en_out <= `SD '0;
            done_out              <= `SD '0;
        end else begin
            fft_len         <= `SD fft_len_next;
            fft_num_stages  <= `SD fft_num_stages_next;
            num_easy_stages <= `SD num_easy_stages_next;

            step_val  <= `SD step_val_next;
            end_val   <= `SD end_val_next;
            iter1     <= `SD iter1_next;
            iter2     <= `SD iter2_next;
            saved_val <= `SD saved_val_next;

            state     <= `SD state_next;
            stage_idx <= `SD stage_idx_next;

            set_state       <= `SD set_state_next;
            set_idx_max     <= `SD set_idx_max_next;
            set_idx_inc     <= `SD set_idx_inc_next;
            set_start       <= `SD set_start_next;
            set_idx         <= `SD set_idx_next;
            sets_processed  <= `SD sets_processed_next;
            iter_state      <= `SD iter_state_next;
            iter_start      <= `SD iter_start_next;
            iters_processed <= `SD iters_processed_next;
            offset          <= `SD offset_next;

            tile_idx_2_en <= `SD tile_idx_2_en_next;

            col_transmit_dist <= `SD col_transmit_dist_next;
            fft_second_dim_en  <= `SD fft_second_dim_en_next;

            w_en_out              <= `SD w_en_next;
            w_addr_out            <= `SD w_addr_next;
            val_en_out            <= `SD tile_idx_en_next;
            val_addr_out          <= `SD tile_idx_next;
            col_transmit_dist_out <= `SD col_transmit_dist;
            fft_second_dim_en_out <= `SD fft_second_dim_en;
            done_out              <= `SD done_next;
        end // if(reset) begin
    end // always_ff @(posedge clock)
endmodule // fft2_controller
