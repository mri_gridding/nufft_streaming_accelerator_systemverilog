module complex_fixed_to_float_0cycle #(
    parameter FIXED_BIT_WIDTH = 16,
    parameter N = 32,
    parameter E = 8,
    parameter S = 1
    ) (

    // Inputs
    input logic                           en,
    input logic [(FIXED_BIT_WIDTH*2)-1:0] fixed_in,

    // Outputs
    output logic             float_en_out,
    output logic [(N*2)-1:0] float_out

);

    fixed_to_float_0cycle #(.FIXED_BIT_WIDTH(FIXED_BIT_WIDTH), .N(N), .E(E), .S(S)) converter1 (
        .en(en),
        .fixed_in(fixed_in[(FIXED_BIT_WIDTH*2)-1:FIXED_BIT_WIDTH]),
        .float_val_out(float_en_out),
        .float_out(float_out[(N*2)-1:N])
    );
    fixed_to_float_0cycle #(.FIXED_BIT_WIDTH(FIXED_BIT_WIDTH), .N(N), .E(E), .S(S)) converter2 (
        .en(en),
        .fixed_in(fixed_in[FIXED_BIT_WIDTH-1:0]),
        .float_val_out(),
        .float_out(float_out[N-1:0])
    );

endmodule : complex_fixed_to_float_0cycle
