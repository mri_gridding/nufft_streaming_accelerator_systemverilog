module fmul_0cycle #(parameter N = 32, parameter E = 8, parameter S = 1) (
    // Inputs
    input logic         en,
    input logic [N-1:0] op1,
    input logic [N-1:0] op2,

    // Outputs
    output logic         res_val,
    output logic [N-1:0] res
);

    logic [E-1:0]         tmp_exp_next;
    logic [((N-E)*2)-1:0] tmp_mant_next;
    logic                 res_sign_next;
    logic [E-1:0]         res_exp_next;
    logic [N-E-S-1:0]     res_mant_next;

    assign res_val = en;
    assign res     = {res_sign_next, res_exp_next, res_mant_next};

    always_comb begin
        tmp_exp_next  = '0;
        tmp_mant_next = '0;

        res_sign_next = '0;
        res_exp_next  = '0;
        res_mant_next = '0;

        // Cycle 0 - perform the multiplication
        if(en) begin
            // Check if either operand is zero
            if(~((op1[N-S-1:N-S-E] == 0 && op1[N-E-S-1:0] == 0) || (op2[N-S-1:N-S-E] == 0 && op2[N-E-S-1:0] == 0))) begin
                res_sign_next = op1[N-1] ^ op2[N-1];
                tmp_exp_next = op1[N-S-1:N-S-E] + op2[N-S-1:N-S-E] - 127;
                tmp_mant_next = {1'b1, op1[N-E-S-1:0]} * {1'b1, op2[N-E-S-1:0]};
            end

            res_exp_next  = tmp_exp_next;
            res_mant_next = tmp_mant_next;

            if(tmp_mant_next[((N-E)*2)-1]) begin
                res_exp_next  = tmp_exp_next + 1;
                res_mant_next = tmp_mant_next[((N-E)*2)-2:N-E];
            end else begin
                res_mant_next = tmp_mant_next[((N-E)*2)-3:N-E-1];
            end
        end
    end

endmodule : fmul_0cycle
