module fixed_to_float_0cycle #(parameter FIXED_BIT_WIDTH = 16, parameter N = 32, parameter E = 8, parameter S = 1) (
    // Inputs
    input logic                       en,
    input logic [FIXED_BIT_WIDTH-1:0] fixed_in,

    // Outputs
    output logic         float_val_out,
    output logic [N-1:0] float_out
);

    logic                       float_sign_next;
    logic [E-1:0]               float_exp_next;
    logic [FIXED_BIT_WIDTH-2:0] tmp_mant_next;
    logic [N-E-S-1:0]           float_mant_next;

    assign float_val_out = en;
    assign float_out     = {float_sign_next, float_exp_next, float_mant_next};

    always_comb begin
        tmp_mant_next = fixed_in[FIXED_BIT_WIDTH-2:0];

        float_sign_next = '0;
        float_exp_next  = '0;
        float_mant_next = '0;

        // Cycle 0 - get absolute value of input
        if(en) begin
            if(fixed_in[FIXED_BIT_WIDTH-1]) begin
                float_sign_next = 1;
                tmp_mant_next   = (~fixed_in[FIXED_BIT_WIDTH-2:0])+1;
            end

            if(fixed_in[FIXED_BIT_WIDTH-2:0] != 0) begin
                for(int i = 0; i < FIXED_BIT_WIDTH-1; ++i) begin
                    if(tmp_mant_next[FIXED_BIT_WIDTH-2-i]) begin
                        float_exp_next             = 127 - i;
                        float_mant_next[N-E-S-1:9] = tmp_mant_next[FIXED_BIT_WIDTH-3:0] << i;

                        break;
                    end
                end
            end
        end
    end

endmodule : fixed_to_float_0cycle
