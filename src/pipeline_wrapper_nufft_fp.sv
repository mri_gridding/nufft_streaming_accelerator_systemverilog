/////////////////////////////////////////////////////////////////////////
//                                                                     //
//  Module name :  pipeline_nufft_wrapper_fp.sv                        //
//                                                                     //
//  Description :  This instantiates and connects the individual       //
//                 nufft pipelines to form the 2D array.               //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

// `timescale 1ns/100ps
import custom_types::*;

module pipeline_wrapper_nufft_fp #(
    parameter ACCEL_DIM              = 8,
    parameter MAX_GRID_DIM           = 1024,
    parameter MEM_TILE_DIM           = (MAX_GRID_DIM/ACCEL_DIM),
    parameter MAX_INTERP_WINDOW_SIZE = ACCEL_DIM,
    parameter SAMPLE_BIT_WIDTH       = 32,
    parameter SAMPLE_COORD_BIT_WIDTH = 32,
    parameter WEIGHT_BIT_WIDTH       = 16,
    parameter WEIGHT_FRAC_WIDTH      = 14,
    parameter WEIGHT_OVERSAMP        = 32,
    parameter NUM_WEIGHTS            = 512, // MAX_INTERP_WINDOW_SIZE*WEIGHT_OVERSAMP,
    parameter NUM_BANKS              = 4,
    parameter NUM_BANK_ENTRIES       = 4096,
    parameter NUM_BUFFER_ENTRIES     = NUM_BANKS*NUM_BANK_ENTRIES,
    parameter MAX_TRANSMIT_HOPS      = ACCEL_DIM,
    parameter N                      = 32,
    parameter E                      = 8,
    parameter S                      = 1
    ) (

        // Inputs
        input                                                                clock,
        input                                                                reset,
        input operation_types                                                nufft_op,
        input                                                                init_mem,
        input                                                                fft_start_en_in,
        input                                                                ifft_en_in,
        input [$clog2(MAX_GRID_DIM):0]                                       grid_dim_in,
        input [$clog2($clog2(MAX_GRID_DIM)):0]                               fft_num_stages_in,
        input [$clog2(MEM_TILE_DIM)-1:0]                                     tile_dim_in,
        input [$clog2(MAX_INTERP_WINDOW_SIZE)-1:0]                           window_dim_in,
        input                                                                weight_const_wr_en_in,
        input [$clog2(NUM_WEIGHTS)-1:0]                                      weight_const_wr_addr_in,
        input [(WEIGHT_BIT_WIDTH*2)-1:0]                                     weight_const_wr_data_in,
        input [ACCEL_DIM-1:0][ACCEL_DIM-1:0]                                 wr_en_in, // write a value (for loading uniform grid data)
        input [ACCEL_DIM-1:0][ACCEL_DIM-1:0][$clog2(NUM_BUFFER_ENTRIES)-1:0] wr_addr_in,
        input [ACCEL_DIM-1:0][ACCEL_DIM-1:0][(SAMPLE_BIT_WIDTH*2)-1:0]       wr_data_in,
        input                                                                rd_en_in, // output a value (for reading final values out)
        input [$clog2(NUM_BUFFER_ENTRIES)-1:0]                               rd_addr_in, // read/write tile address (can be used for reading final values out)
        input nonuniform_sample                                              sample_in,

        // Outputs
        output nonuniform_sample                                              sample_out,
        output logic [ACCEL_DIM-1:0][ACCEL_DIM-1:0]                           rd_en_out,
        output logic [ACCEL_DIM-1:0][ACCEL_DIM-1:0][(SAMPLE_BIT_WIDTH*2)-1:0] rd_data_out,
        output logic [ACCEL_DIM-1:0][ACCEL_DIM-1:0]                           fft_done_out

    );


    // Generate the pipelines (make one slice)
    nonuniform_sample                                            sample_arr [ACCEL_DIM-1:0][ACCEL_DIM:0];
    logic [ACCEL_DIM-1:0][ACCEL_DIM-1:0][(SAMPLE_BIT_WIDTH*2):0] extern_data;
    logic [ACCEL_DIM-1:0][ACCEL_DIM-1:0]                         fft_done;

    always_ff @(posedge clock) begin
        if(reset) begin
            fft_done_out <= `SD '0;

            sample_arr[0][0].en      <= `SD '0;
            sample_arr[0][0].x_coord <= `SD '0;
            sample_arr[0][0].y_coord <= `SD '0;
            sample_arr[0][0].data    <= `SD '0;

            sample_arr[1][0].en      <= `SD '0;
            sample_arr[1][0].x_coord <= `SD '0;
            sample_arr[1][0].y_coord <= `SD '0;
            sample_arr[1][0].data    <= `SD '0;

            sample_arr[2][0].en      <= `SD '0;
            sample_arr[2][0].x_coord <= `SD '0;
            sample_arr[2][0].y_coord <= `SD '0;
            sample_arr[2][0].data    <= `SD '0;

            sample_arr[3][0].en      <= `SD '0;
            sample_arr[3][0].x_coord <= `SD '0;
            sample_arr[3][0].y_coord <= `SD '0;
            sample_arr[3][0].data    <= `SD '0;

            sample_arr[4][0].en      <= `SD '0;
            sample_arr[4][0].x_coord <= `SD '0;
            sample_arr[4][0].y_coord <= `SD '0;
            sample_arr[4][0].data    <= `SD '0;

            sample_arr[5][0].en      <= `SD '0;
            sample_arr[5][0].x_coord <= `SD '0;
            sample_arr[5][0].y_coord <= `SD '0;
            sample_arr[5][0].data    <= `SD '0;

            sample_arr[6][0].en      <= `SD '0;
            sample_arr[6][0].x_coord <= `SD '0;
            sample_arr[6][0].y_coord <= `SD '0;
            sample_arr[6][0].data    <= `SD '0;

            sample_arr[7][0].en      <= `SD '0;
            sample_arr[7][0].x_coord <= `SD '0;
            sample_arr[7][0].y_coord <= `SD '0;
            sample_arr[7][0].data    <= `SD '0;

            sample_out.en      <= `SD '0;
            sample_out.x_coord <= `SD '0;
            sample_out.y_coord <= `SD '0;
            sample_out.data    <= `SD '0;
        end else begin
            fft_done_out <= `SD |fft_done;

            sample_arr[0][0] <= `SD sample_in;
            sample_arr[1][0] <= `SD sample_arr[0][8];
            sample_arr[2][0] <= `SD sample_arr[1][8];
            sample_arr[3][0] <= `SD sample_arr[2][8];
            sample_arr[4][0] <= `SD sample_arr[3][8];
            sample_arr[5][0] <= `SD sample_arr[4][8];
            sample_arr[6][0] <= `SD sample_arr[5][8];
            sample_arr[7][0] <= `SD sample_arr[6][8];

            sample_out       <= `SD sample_arr[7][8];
        end
    end

    // Generate the pipelines (make one slice)
    genvar gen_x_idx, gen_y_idx;
    generate
        for(gen_x_idx = 0; gen_x_idx < ACCEL_DIM; gen_x_idx++) begin : gen_pipelines_x
            for(gen_y_idx = 0; gen_y_idx < ACCEL_DIM; gen_y_idx++) begin : gen_pipelines_y
                // Instantiate the pipelines
                pipeline_nufft_fp #(
                    .ACCEL_DIM(ACCEL_DIM),
                    .MAX_GRID_DIM(MAX_GRID_DIM),
                    .MEM_TILE_DIM(MEM_TILE_DIM),
                    .MAX_INTERP_WINDOW_SIZE(MAX_INTERP_WINDOW_SIZE),
                    .SAMPLE_BIT_WIDTH(SAMPLE_BIT_WIDTH),
                    .SAMPLE_COORD_BIT_WIDTH(SAMPLE_COORD_BIT_WIDTH),
                    .WEIGHT_BIT_WIDTH(WEIGHT_BIT_WIDTH),
                    .WEIGHT_FRAC_WIDTH(WEIGHT_FRAC_WIDTH),
                    .WEIGHT_OVERSAMP(WEIGHT_OVERSAMP),
                    .NUM_WEIGHTS(NUM_WEIGHTS),
                    .NUM_BANKS(NUM_BANKS),
                    .NUM_BANK_ENTRIES(NUM_BANK_ENTRIES),
                    .NUM_BUFFER_ENTRIES(NUM_BUFFER_ENTRIES),
                    .MAX_TRANSMIT_HOPS(MAX_TRANSMIT_HOPS),
                    .N(N),
                    .E(E),
                    .S(S)
                    ) pipeline_0 (

                    // Inputs
                    .clock(clock),
                    .reset(reset),
                    .nufft_op(nufft_op),
                    .init_mem(init_mem),
                    .fft_start_en_in(fft_start_en_in),
                    .ifft_en_in(ifft_en_in),
                    .accel_x_idx_in(gen_x_idx[$clog2(ACCEL_DIM)-1:0]),
                    .accel_y_idx_in(gen_y_idx[$clog2(ACCEL_DIM)-1:0]),
                    .grid_dim_in(grid_dim_in),
                    .fft_num_stages_in(fft_num_stages_in),
                    .tile_dim_in(tile_dim_in),
                    .window_dim_in(window_dim_in),
                    .weight_const_wr_en_in(weight_const_wr_en_in),
                    .weight_const_wr_addr_in(weight_const_wr_addr_in),
                    .weight_const_wr_data_in(weight_const_wr_data_in),
                    .rd_en_in(rd_en_in),
                    .rd_addr_in(rd_addr_in),
                    .wr_en_in(wr_en_in[gen_x_idx][gen_y_idx]),
                    .wr_addr_in(wr_addr_in[gen_x_idx][gen_y_idx]),
                    .wr_data_in(wr_data_in[gen_x_idx][gen_y_idx]),
                    .sample_in(sample_arr[gen_x_idx][gen_y_idx]),
                    .x_extern_data_in({extern_data[7][gen_y_idx], extern_data[6][gen_y_idx], extern_data[5][gen_y_idx], extern_data[4][gen_y_idx], extern_data[3][gen_y_idx], extern_data[2][gen_y_idx], extern_data[1][gen_y_idx], extern_data[0][gen_y_idx]}),
                    .y_extern_data_in({extern_data[gen_x_idx][7], extern_data[gen_x_idx][6], extern_data[gen_x_idx][5], extern_data[gen_x_idx][4], extern_data[gen_x_idx][3], extern_data[gen_x_idx][2], extern_data[gen_x_idx][1], extern_data[gen_x_idx][0]}),

                    // Outputs
                    .sample_out(sample_arr[gen_x_idx][gen_y_idx+1]),
                    .rd_en_out(rd_en_out[gen_x_idx][gen_y_idx]),
                    .rd_data_out(rd_data_out[gen_x_idx][gen_y_idx]),
                    .extern_data_out(extern_data[gen_x_idx][gen_y_idx]),
                    .fft_done_out(fft_done[gen_x_idx][gen_y_idx])

                );
            end
        end
    endgenerate

endmodule // pipeline_wrapper_nufft_fp
