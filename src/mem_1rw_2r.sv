/////////////////////////////////////////////////////////////////////////
//                                                                     //
//   Modulename :  mem_1rw_2r.sv                                       //
//                                                                     //
//  Description :  This module creates the regfile for the interp      //
//                 table oversamp values.                              //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

// `timescale 1ns/100ps

module mem_1rw_2r #(
    parameter ENTRY_BIT_WIDTH = 32,
    parameter NUM_ENTRIES     = 256
    ) (

        // Inputs
        input                           clock,
        input                           init,
        input                           wra_en, // write A enable
        input [$clog2(NUM_ENTRIES)-1:0] rwa_idx, // read/write A index
        input [$clog2(NUM_ENTRIES)-1:0] rdb_idx, // read B index
        input [$clog2(NUM_ENTRIES)-1:0] rdc_idx, // read C index
        input [ENTRY_BIT_WIDTH-1:0]     wra_data, // write A data

        // Outputs
        output logic [ENTRY_BIT_WIDTH-1:0] rda_out, // read A data
        output logic [ENTRY_BIT_WIDTH-1:0] rdb_out, // read B data
        output logic [ENTRY_BIT_WIDTH-1:0] rdc_out  // read C data

    );

    logic [NUM_ENTRIES-1:0][ENTRY_BIT_WIDTH-1:0] registers; // registers

    wire [ENTRY_BIT_WIDTH-1:0] rda_reg = registers[rwa_idx];
    wire [ENTRY_BIT_WIDTH-1:0] rdb_reg = registers[rdb_idx];
    wire [ENTRY_BIT_WIDTH-1:0] rdc_reg = registers[rdc_idx];

    //
    // Read/Write port A
    //
    // synopsys sync_set_reset "reset"
    always_ff @(posedge clock) begin
        if(init)
            registers <= `SD '0;

        if(wra_en) begin
            registers[rwa_idx] <= `SD wra_data;
            rda_out            <= `SD '0;
        end else begin
            rda_out <= `SD rda_reg;
        end
    end

    //
    // Read port B
    //
    // synopsys sync_set_reset "reset"
    always_ff @(posedge clock)
        rdb_out <= `SD rdb_reg;

    //
    // Read port C
    //
    // synopsys sync_set_reset "reset"
    always_ff @(posedge clock)
        rdc_out <= `SD rdc_reg;

endmodule // mem_1rw_2r
