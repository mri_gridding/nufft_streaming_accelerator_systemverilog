/////////////////////////////////////////////////////////////////////////
//                                                                     //
//  Module name :  pipeline_nufft_fp.sv                                //
//                                                                     //
//  Description :  Top-level module of a single nufft pipeline; this   //
//                 instantiates and connects the components of the     //
//                 nufft pipeline together.                            //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

// `timescale 1ns/100ps
import custom_types::*;

module pipeline_nufft_fp #(
    parameter ACCEL_DIM              = 8,
    parameter MAX_GRID_DIM           = 1024,
    parameter MEM_TILE_DIM           = (MAX_GRID_DIM/ACCEL_DIM),
    parameter MAX_INTERP_WINDOW_SIZE = ACCEL_DIM,
    parameter SAMPLE_BIT_WIDTH       = 32,
    parameter SAMPLE_COORD_BIT_WIDTH = 32,
    parameter WEIGHT_BIT_WIDTH       = 16,
    parameter WEIGHT_FRAC_WIDTH      = 14,
    parameter WEIGHT_OVERSAMP        = 32,
    parameter NUM_WEIGHTS            = 512, // MAX_INTERP_WINDOW_SIZE*WEIGHT_OVERSAMP,
    parameter NUM_BANKS              = 4,
    parameter NUM_BANK_ENTRIES       = 4096,
    parameter NUM_BUFFER_ENTRIES     = NUM_BANKS*NUM_BANK_ENTRIES,
    parameter MAX_TRANSMIT_HOPS      = ACCEL_DIM,
    parameter N                      = 32,
    parameter E                      = 8,
    parameter S                      = 1
    ) (

        // Inputs
        input                                         clock,
        input                                         reset,
        input operation_types                         nufft_op,
        input                                         init_mem,
        input                                         fft_start_en_in,
        input                                         ifft_en_in,
        input [$clog2(ACCEL_DIM)-1:0]                 accel_x_idx_in,
        input [$clog2(ACCEL_DIM)-1:0]                 accel_y_idx_in,
        input [$clog2(MAX_GRID_DIM):0]                grid_dim_in,
        input [$clog2($clog2(MAX_GRID_DIM)):0]        fft_num_stages_in,
        input [$clog2(MEM_TILE_DIM)-1:0]              tile_dim_in,
        input [$clog2(MAX_INTERP_WINDOW_SIZE)-1:0]    window_dim_in,
        input                                         weight_const_wr_en_in,
        input [$clog2(NUM_WEIGHTS)-1:0]               weight_const_wr_addr_in,
        input [(WEIGHT_BIT_WIDTH*2)-1:0]              weight_const_wr_data_in,
        input                                         rd_en_in,
        input [$clog2(NUM_BUFFER_ENTRIES)-1:0]        rd_addr_in, // read address (used for reading uniform grid values)
        input                                         wr_en_in,
        input [$clog2(NUM_BUFFER_ENTRIES)-1:0]        wr_addr_in, // write address (used for loading uniform grid values)
        input [(SAMPLE_BIT_WIDTH*2)-1:0]              wr_data_in,
        input nonuniform_sample                       sample_in,
        input [ACCEL_DIM-1:0][(SAMPLE_BIT_WIDTH*2):0] x_extern_data_in,
        input [ACCEL_DIM-1:0][(SAMPLE_BIT_WIDTH*2):0] y_extern_data_in,

        // Outputs
        output nonuniform_sample                sample_out,
        output logic                            rd_en_out,
        output logic [(SAMPLE_BIT_WIDTH*2)-1:0] rd_data_out,
        output logic [(SAMPLE_BIT_WIDTH*2):0]   extern_data_out,
        output logic                            fft_done_out

    );


    ////////////////////////////////////////////////////////////////
    //                      Internal Signals                      //
    ////////////////////////////////////////////////////////////////

    // Select Signals
    // Output
    logic                                  sel_en;
    logic [$clog2(NUM_WEIGHTS)-1:0]        sel_weight_addr1;
    logic [$clog2(NUM_WEIGHTS)-1:0]        sel_weight_addr2;
    logic [$clog2(NUM_BUFFER_ENTRIES)-1:0] sel_fp_addr;

    // FP Memory/Accumulation Signals
    // Input
    logic                                  fp_swap_comp;
    logic                                  fp_rd_en;
    logic [$clog2(NUM_BUFFER_ENTRIES)-1:0] fp_rd_addr;
    logic                                  fp_wr_en;
    logic [$clog2(NUM_BUFFER_ENTRIES)-1:0] fp_wr_addr;
    logic [(SAMPLE_BIT_WIDTH*2)-1:0]       fp_wr_data;
    logic                                  fp_accum_en;
    logic [$clog2(NUM_BUFFER_ENTRIES)-1:0] fp_accum_addr;
    logic [(SAMPLE_BIT_WIDTH*2)-1:0]       fp_accum_data;
    // Output
    logic                                  fp_rd_en_out;
    logic [(SAMPLE_BIT_WIDTH*2)-1:0]       fp_rd_data_out;

    // Fixed-Point Memory/Complex Multiplication Signals
    // Input
    logic                            weight_const_rd_en;
    logic [$clog2(NUM_WEIGHTS)-1:0]  weight_const_rd_addr1;
    logic [$clog2(NUM_WEIGHTS)-1:0]  weight_const_rd_addr2;
    logic                            fixed_rd_only;
    // Output
    logic                            fixed_prod_en;
    logic [(WEIGHT_BIT_WIDTH*2)-1:0] fixed_prod;

    // Complex Fixed to Complex FP Conversion Signals
    // Output
    logic                            fp_weight_en;
    logic [(SAMPLE_BIT_WIDTH*2)-1:0] fp_weight;

    // FP Memory/Accumulation Signals
    // Input
    logic                            fp_mult_en;
    logic [(SAMPLE_BIT_WIDTH*2)-1:0] weight_data;
    logic [(SAMPLE_BIT_WIDTH*2)-1:0] sample_data;
    // Output
    logic                            fp_prod_en;
    logic [(SAMPLE_BIT_WIDTH*2)-1:0] fp_prod;

    // FFT_Ctrl Signals
    // Output
    logic                                  fft_w_en;
    logic [$clog2(NUM_WEIGHTS)-1:0]        fft_w_addr;
    logic                                  fft_val_en;
    logic [$clog2(NUM_BUFFER_ENTRIES)-1:0] fft_val_addr;
    logic [$clog2(MAX_TRANSMIT_HOPS)-1:0]  fft_transmit_dist;
    logic                                  fft_second_dim_en;

    // FP Complex Addition Signals
    // Input
    logic                            fp_add_en;
    logic [(SAMPLE_BIT_WIDTH*2)-1:0] fp_add_val1;
    logic [(SAMPLE_BIT_WIDTH*2)-1:0] fp_add_val2;
    // Output
    logic                            fp_sum_en;
    logic [(SAMPLE_BIT_WIDTH*2)-1:0] fp_sum;

    // FP Complex Subtraction Signals
    // Input
    logic                            fp_sub_en;
    logic [(SAMPLE_BIT_WIDTH*2)-1:0] fp_sub_val1;
    logic [(SAMPLE_BIT_WIDTH*2)-1:0] fp_sub_val2;
    // Output
    logic                            fp_diff_en;
    logic [(SAMPLE_BIT_WIDTH*2)-1:0] fp_diff;

    // Sample Delay Data
    nonuniform_sample                      delayed_sample_1, delayed_sample_2, delayed_sample_3;
    logic [$clog2(NUM_BUFFER_ENTRIES)-1:0] delayed_sel_fp_addr1;
    logic [(SAMPLE_BIT_WIDTH*2):0]         delayed_fp_rd_data_1, delayed_fp_rd_data_2, delayed_fp_rd_data_3;
    logic [$clog2(NUM_BUFFER_ENTRIES)-1:0] delayed_fft_val_addr;
    logic [$clog2(MAX_TRANSMIT_HOPS)-1:0]  delayed_fft_transmit_dist1, delayed_fft_transmit_dist2;
    logic                                  delayed_fft_second_dim_en1, delayed_fft_second_dim_en2;
    logic [(SAMPLE_BIT_WIDTH*2):0]         delayed_fp_prod;

    // External Transmission Signals
    nonuniform_sample                sample_out_next;
    logic                            rd_en_out_next;
    logic [(SAMPLE_BIT_WIDTH*2)-1:0] rd_data_out_next;
    logic [(SAMPLE_BIT_WIDTH*2):0]   extern_data_out_next;
    logic                            fft_done_out_next;


    // Select Module
    select #(
        .SAMPLE_BIT_WIDTH(SAMPLE_BIT_WIDTH),
        .SAMPLE_COORD_BIT_WIDTH(SAMPLE_COORD_BIT_WIDTH),
        .WEIGHT_OVERSAMP(WEIGHT_OVERSAMP),
        .ACCEL_DIM(ACCEL_DIM),
        .MAX_GRID_DIM(MAX_GRID_DIM),
        .MEM_TILE_DIM(MEM_TILE_DIM),
        .MAX_INTERP_WINDOW_SIZE(MAX_INTERP_WINDOW_SIZE),
        .NUM_BUFFER_ENTRIES(NUM_BUFFER_ENTRIES)
        ) slct (

        // Inputs
        .clock(clock),
        .reset(reset),
        .sample_en_in(sample_in.en),
        .sample_x_coord_in(sample_in.x_coord),
        .sample_y_coord_in(sample_in.y_coord),
        .accel_x_idx_in(accel_x_idx_in),
        .accel_y_idx_in(accel_y_idx_in),
        .tile_dim_in(tile_dim_in),
        .window_dim_in(window_dim_in),

        // Outputs
        .select_en_out(sel_en),
        .select_x_weight_idx_out(sel_weight_addr1),
        .select_y_weight_idx_out(sel_weight_addr2),
        .select_tile_idx_out(sel_fp_addr)

    );


    // FFT_Ctrl Module
    fft2_controller #(
        .MAX_GRID_DIM(MAX_GRID_DIM),
        .ACCEL_DIM(ACCEL_DIM),
        .MEM_TILE_DIM(MEM_TILE_DIM),
        .NUM_WEIGHTS(NUM_WEIGHTS),
        .NUM_BUFFER_ENTRIES(NUM_BUFFER_ENTRIES),
        .MAX_TRANSMIT_HOPS(MAX_TRANSMIT_HOPS)
        ) fft2_ctrl (

        // Inputs
        .clock(clock),
        .reset(reset),
        .start_in(fft_start_en_in),
        .fft_len_in(grid_dim_in),
        .fft_num_stages_in(fft_num_stages_in),
        .x_controller_idx_in(accel_x_idx_in),
        .y_controller_idx_in(accel_y_idx_in),

        // Outputs
        .w_en_out(fft_w_en),
        .w_addr_out(fft_w_addr),
        .val_en_out(fft_val_en),
        .val_addr_out(fft_val_addr),
        .col_transmit_dist_out(fft_transmit_dist),
        .fft_second_dim_en_out(fft_second_dim_en),
        .done_out(fft_done_out_next)

    );


    // Select Address Delay Module (output of slct to output of fp_mult_complex_unit)
    delay_queue #(
        .DELAY_CYCLES(10),
        .ENTRY_BIT_WIDTH($clog2(NUM_BUFFER_ENTRIES))
        ) sel_fp_addr_dq1 (

        // Inputs
        .clock(clock),
        .reset(reset),
        .datum_in(sel_fp_addr),

        // Outputs
        .datum_out(delayed_sel_fp_addr1)

    );

    // FFT Read/Write Address Delay Module (output of fft2_col_ctrl to output of fp_sub_complex_unit/fp_add_complex_unit)
    delay_queue #(
        .DELAY_CYCLES(10),
        .ENTRY_BIT_WIDTH($clog2(NUM_BUFFER_ENTRIES))
        ) fft_val_addr_dq (

        // Inputs
        .clock(clock),
        .reset(reset),
        .datum_in(fft_val_addr),

        // Outputs
        .datum_out(delayed_fft_val_addr)

    );

    // FP Memory/Accumulation Module (can accumulate)
    always_ff @(posedge clock) begin
        if(reset) begin
            fp_rd_en   <= `SD '0;
            fp_rd_addr <= `SD '0;

            fp_swap_comp <= `SD '0;

            fp_wr_en   <= `SD '0;
            fp_wr_addr <= `SD '0;
            fp_wr_data <= `SD '0;

            fp_accum_en   <= `SD '0;
            fp_accum_addr <= `SD '0;
            fp_accum_data <= `SD '0;
        end else begin
            if(nufft_op == GRIDDING) begin
                fp_rd_en   <= `SD '0;
                fp_rd_addr <= `SD '0;

                fp_swap_comp <= `SD '0;

                fp_wr_en   <= `SD '0;
                fp_wr_addr <= `SD '0;
                fp_wr_data <= `SD '0;

                fp_accum_en   <= `SD fp_prod_en;
                fp_accum_addr <= `SD delayed_sel_fp_addr1;
                fp_accum_data <= `SD fp_prod;
            end else if(nufft_op == REGRIDDING) begin
                fp_rd_en   <= `SD sel_en;
                fp_rd_addr <= `SD sel_fp_addr;

                fp_swap_comp <= `SD '0;

                fp_wr_en   <= `SD wr_en_in;
                fp_wr_addr <= `SD wr_addr_in; // TODO: figure out why this can't be zeroed out without breaking regridding
                fp_wr_data <= `SD wr_data_in;

                fp_accum_addr <= `SD '0;
                fp_accum_data <= `SD '0;
                fp_accum_en   <= `SD '0;
            end else if(nufft_op == FFT_IFFT) begin
                fp_rd_en   <= `SD fft_val_en;
                fp_rd_addr <= `SD fft_val_addr;

                fp_swap_comp <= `SD ifft_en_in;

                fp_wr_en   <= `SD fp_diff_en | fp_sum_en;
                fp_wr_addr <= `SD delayed_fft_val_addr;
                fp_wr_data <= `SD fp_diff_en ? fp_diff : fp_sum;

                fp_accum_addr <= `SD '0;
                fp_accum_data <= `SD '0;
                fp_accum_en   <= `SD '0;
            end else begin // if(nufft_op == READ_MEM)
                fp_rd_en   <= `SD rd_en_in;
                fp_rd_addr <= `SD rd_addr_in;

                fp_swap_comp <= `SD '0;

                fp_wr_en   <= `SD wr_en_in;
                fp_wr_addr <= `SD wr_addr_in;
                fp_wr_data <= `SD wr_data_in;

                fp_accum_en   <= `SD fp_prod_en;
                fp_accum_addr <= `SD delayed_sel_fp_addr1;
                fp_accum_data <= `SD fp_prod;
            end
        end
    end
    fp_mem_accum #(
        .SAMPLE_BIT_WIDTH(SAMPLE_BIT_WIDTH),
        .NUM_BANKS(NUM_BANKS),
        .NUM_BANK_ENTRIES(NUM_BANK_ENTRIES),
        .NUM_BUFFER_ENTRIES(NUM_BUFFER_ENTRIES)
        ) fp_mem_accum_unit (

        // Inputs
        .clock(clock),
        .reset(reset),
        .init_mem(init_mem),
        .swap_comp_en_in(fp_swap_comp),
        .rd_en_in(fp_rd_en),
        .rd_addr_in(fp_rd_addr),
        .wr_en_in(fp_wr_en),
        .wr_addr_in(fp_wr_addr),
        .wr_data_in(fp_wr_data),
        .accum_en_in(fp_accum_en),
        .accum_addr_in(fp_accum_addr),
        .accum_data_in(fp_accum_data),

        // Outputs
        .rd_en_out(fp_rd_en_out),
        .rd_data_out(fp_rd_data_out)

    );


    // Fixed-Point Memory/Complex Multiplication Module
    always_ff @(posedge clock) begin
        if(reset) begin
            weight_const_rd_en    <= `SD '0;
            weight_const_rd_addr1 <= `SD '0;
            weight_const_rd_addr2 <= `SD '0;

            fixed_rd_only <= `SD '0;
        end else begin
            if(nufft_op == FFT_IFFT) begin
                weight_const_rd_en    <= `SD fft_w_en;
                weight_const_rd_addr1 <= `SD fft_w_addr;
                weight_const_rd_addr2 <= `SD '0;

                fixed_rd_only <= `SD 1'b1;
            end else begin
                weight_const_rd_en    <= `SD sel_en;
                weight_const_rd_addr1 <= `SD sel_weight_addr1;
                weight_const_rd_addr2 <= `SD sel_weight_addr2;

                fixed_rd_only <= `SD '0;
            end
        end
    end
    fixed_mem_mult_complex #(
        .SAMPLE_BIT_WIDTH(SAMPLE_BIT_WIDTH),
        .WEIGHT_BIT_WIDTH(WEIGHT_BIT_WIDTH),
        .MAX_INTERP_WINDOW_SIZE(MAX_INTERP_WINDOW_SIZE),
        .WEIGHT_OVERSAMP(WEIGHT_OVERSAMP),
        .NUM_WEIGHTS(NUM_WEIGHTS),
        .NUM_BUFFER_ENTRIES(NUM_BUFFER_ENTRIES)
        ) fixed_mem_mult_complex_unit (

        // Inputs
        .clock(clock),
        .reset(reset),
        .init_mem(init_mem),
        .rd_only_en_in(fixed_rd_only),
        .rd_en_in(weight_const_rd_en),
        .rd_addr1_in(weight_const_rd_addr1),
        .rd_addr2_in(weight_const_rd_addr2),
        .wr_en_in(weight_const_wr_en_in),
        .wr_addr_in(weight_const_wr_addr_in),
        .wr_data_in(weight_const_wr_data_in),

        // Outputs
        .prod_out(fixed_prod),
        .en_out(fixed_prod_en)

    );


    // Complex Fixed to Complex FP Conversion Module
    complex_fixed_to_float_0cycle #(
        .FIXED_BIT_WIDTH(WEIGHT_BIT_WIDTH),
        .N(N),
        .E(E),
        .S(S)
        ) complex_fixed_to_float_0cycle_unit (

        // Inputs
        .en(fixed_prod_en),
        .fixed_in(fixed_prod),

        // Outputs
        .float_en_out(fp_weight_en),
        .float_out(fp_weight)
    );

    // Sample Delay Module (input of pipeline to output of fixed_mem_mult_complex_unit)
    sample_delay_queue #(
        .DELAY_CYCLES(9)
        ) sample_dq1 (

        // Inputs
        .clock(clock),
        .reset(reset),
        .datum_in(sample_in),

        // Outputs
        .datum_out(delayed_sample_1)

    );

    // FP Data Delay Module (output of fp_mem_accum_unit to [REGRIDDING] fixed_mem_mult_complex_unit / [FFT_IFFT] fp_mult_complex_unit)
    delay_queue #(
        .DELAY_CYCLES(3),
        .ENTRY_BIT_WIDTH(SAMPLE_BIT_WIDTH*2+1)
        ) fp_rd_data_dq1 (

        // Inputs
        .clock(clock),
        .reset(reset),
        .datum_in({fp_rd_en_out, fp_rd_data_out}),

        // Outputs
        .datum_out(delayed_fp_rd_data_1)

    );

    // FP Complex Multiplication Module
    always_ff @(posedge clock) begin
        if(reset) begin
            fp_mult_en  <= `SD '0;
            weight_data <= `SD '0;
            sample_data <= `SD '0;
        end else begin
            if(nufft_op == GRIDDING) begin
                fp_mult_en  <= `SD fp_weight_en;
                weight_data <= `SD {fp_weight[(SAMPLE_BIT_WIDTH*2)-1:SAMPLE_BIT_WIDTH], (fp_weight[SAMPLE_BIT_WIDTH-1] ^ 1'b1), fp_weight[SAMPLE_BIT_WIDTH-2:0]}; // flips the sign bit of the imaginary component
                sample_data <= `SD delayed_sample_1.data;
            end else if(nufft_op == REGRIDDING) begin
                fp_mult_en  <= `SD fp_weight_en;
                weight_data <= `SD fp_weight;
                sample_data <= `SD delayed_fp_rd_data_1[SAMPLE_BIT_WIDTH*2-1:0];
            end else if(nufft_op == FFT_IFFT) begin
                fp_mult_en  <= `SD fp_weight_en;
                weight_data <= `SD fp_weight;
                sample_data <= `SD fp_rd_data_out;
            end
        end
    end
    fp_mult_complex #(
        .SAMPLE_BIT_WIDTH(SAMPLE_BIT_WIDTH),
        .WEIGHT_BIT_WIDTH(WEIGHT_BIT_WIDTH),
        .NUM_BUFFER_ENTRIES(NUM_BUFFER_ENTRIES)
        ) fp_mult_complex_unit (

        // Inputs
        .clock(clock),
        .reset(reset),
        .mult_en_in(fp_mult_en),
        .val1_in(weight_data),
        .val2_in(sample_data),

        // Outputs
        .prod_out(fp_prod),
        .en_out(fp_prod_en)

    );


    // Sample Delay Module (output of fixed_mem_mult_complex_unit to output of fp_mult_complex_unit)
    sample_delay_queue #(
        .DELAY_CYCLES(4)
        ) sample_dq2 (

        // Inputs
        .clock(clock),
        .reset(reset),
        .datum_in(delayed_sample_1),

        // Outputs
        .datum_out(delayed_sample_2)

    );

    // Data Delay Module (output of fp_mem_accum_unit to output of fp_mult_complex_unit + 1 cycle for transmission)
    delay_queue #(
        .DELAY_CYCLES(1),
        .ENTRY_BIT_WIDTH((SAMPLE_BIT_WIDTH*2)+1)
        ) fp_rd_data_dq2 (

        // Inputs
        .clock(clock),
        .reset(reset),
        .datum_in(delayed_fp_rd_data_1),

        // Outputs
        .datum_out(delayed_fp_rd_data_2)

    );

    // Data Delay Module (output of fp_mem_accum_unit to output of fp_mult_complex_unit + 1 cycle for transmission)
    delay_queue #(
        .DELAY_CYCLES(1),
        .ENTRY_BIT_WIDTH((SAMPLE_BIT_WIDTH*2)+1)
        ) fp_rd_data_dq3 (

        // Inputs
        .clock(clock),
        .reset(reset),
        .datum_in(delayed_fp_rd_data_2),

        // Outputs
        .datum_out(delayed_fp_rd_data_3)

    );

    // Transmit Distance Delay Module (output of fft2_col_ctrl to output of fp_mult_complex_unit + 1 cycle for transmission)
    delay_queue #(
        .DELAY_CYCLES(7),
        .ENTRY_BIT_WIDTH($clog2(MAX_TRANSMIT_HOPS))
        ) fft_transmit_dist_dq1 (

        // Inputs
        .clock(clock),
        .reset(reset),
        .datum_in(fft_transmit_dist),

        // Outputs
        .datum_out(delayed_fft_transmit_dist1)

    );

    // Transmit Distance Delay Module (output of fft2_col_ctrl to output of fp_mult_complex_unit + 1 cycle for transmission)
    delay_queue #(
        .DELAY_CYCLES(1),
        .ENTRY_BIT_WIDTH($clog2(MAX_TRANSMIT_HOPS))
        ) fft_transmit_dist_dq2 (

        // Inputs
        .clock(clock),
        .reset(reset),
        .datum_in(delayed_fft_transmit_dist1),

        // Outputs
        .datum_out(delayed_fft_transmit_dist2)

    );

    // FFT Second Dimension Enable Delay Module (output of fft2_col_ctrl to output of fp_mult_complex_unit + 1 cycle for transmission)
    delay_queue #(
        .DELAY_CYCLES(7),
        .ENTRY_BIT_WIDTH(1)
        ) fft_second_dim_en_dq1 (

        // Inputs
        .clock(clock),
        .reset(reset),
        .datum_in(fft_second_dim_en),

        // Outputs
        .datum_out(delayed_fft_second_dim_en1)

    );

    // FFT Second Dimension Enable Delay Module (output of fft2_col_ctrl to output of fp_mult_complex_unit + 1 cycle for transmission)
    delay_queue #(
        .DELAY_CYCLES(1),
        .ENTRY_BIT_WIDTH(1)
        ) fft_second_dim_en_dq2 (

        // Inputs
        .clock(clock),
        .reset(reset),
        .datum_in(delayed_fft_second_dim_en1),

        // Outputs
        .datum_out(delayed_fft_second_dim_en2)

    );

    // FP Multiplication Result Delay Module (output of fp_mult_complex_unit to output of fp_mult_complex_unit + 1 cycle for transmission)
    delay_queue #(
        .DELAY_CYCLES(1),
        .ENTRY_BIT_WIDTH((SAMPLE_BIT_WIDTH*2)+1)
        ) fp_prod_dq (

        // Inputs
        .clock(clock),
        .reset(reset),
        .datum_in({fp_prod_en, fp_prod}),

        // Outputs
        .datum_out(delayed_fp_prod)

    );

    // FP Complex Subtraction Module
    always_ff @(posedge clock) begin
        if(reset) begin
            fp_sub_en   <= `SD '0;
            fp_sub_val1 <= `SD '0;
            fp_sub_val2 <= `SD '0;
        end else begin
            if(nufft_op == FFT_IFFT) begin
                // If delayed_fft_transmit_dist2 == 0, don't send anything anywhere
                if(delayed_fft_transmit_dist2 == 0) begin
                    fp_sub_en   <= `SD delayed_fp_prod[SAMPLE_BIT_WIDTH*2];
                    fp_sub_val1 <= `SD delayed_fp_rd_data_2[SAMPLE_BIT_WIDTH*2-1:0];
                    fp_sub_val2 <= `SD delayed_fp_prod[(SAMPLE_BIT_WIDTH*2)-1:0];
                end else if(~delayed_fft_second_dim_en2) begin
                    // If delayed_fft_transmit_dist2 > 0, val2 is sent forward (higher pipe idx), while val1 is sent backward (lower pipe idx)
                    if(delayed_fft_transmit_dist2[2] && accel_x_idx_in[2]) begin
                        fp_sub_en   <= `SD delayed_fp_prod[SAMPLE_BIT_WIDTH*2];
                        fp_sub_val1 <= `SD x_extern_data_in[accel_x_idx_in-4][SAMPLE_BIT_WIDTH*2-1:0];
                        fp_sub_val2 <= `SD delayed_fp_prod[(SAMPLE_BIT_WIDTH*2)-1:0];
                    end else if(delayed_fft_transmit_dist2[1] && accel_x_idx_in[1]) begin
                        fp_sub_en   <= `SD delayed_fp_prod[SAMPLE_BIT_WIDTH*2];
                        fp_sub_val1 <= `SD x_extern_data_in[accel_x_idx_in-2][SAMPLE_BIT_WIDTH*2-1:0];
                        fp_sub_val2 <= `SD delayed_fp_prod[(SAMPLE_BIT_WIDTH*2)-1:0];
                    end else if(delayed_fft_transmit_dist2[0] && accel_x_idx_in[0]) begin
                        fp_sub_en   <= `SD delayed_fp_prod[SAMPLE_BIT_WIDTH*2];
                        fp_sub_val1 <= `SD x_extern_data_in[accel_x_idx_in-1][SAMPLE_BIT_WIDTH*2-1:0];
                        fp_sub_val2 <= `SD delayed_fp_prod[(SAMPLE_BIT_WIDTH*2)-1:0];
                    end else begin
                        fp_sub_en   <= `SD '0;
                        fp_sub_val1 <= `SD '0;
                        fp_sub_val2 <= `SD '0;
                    end
                end else begin // if(delayed_fft_second_dim_en2)
                    // If delayed_fft_transmit_dist2 > 0, val2 is sent forward (higher pipe idx), while val1 is sent backward (lower pipe idx)
                    if(delayed_fft_transmit_dist2[2] && accel_y_idx_in[2]) begin
                        fp_sub_en   <= `SD delayed_fp_prod[SAMPLE_BIT_WIDTH*2];
                        fp_sub_val1 <= `SD y_extern_data_in[accel_y_idx_in-4][SAMPLE_BIT_WIDTH*2-1:0];
                        fp_sub_val2 <= `SD delayed_fp_prod[(SAMPLE_BIT_WIDTH*2)-1:0];
                    end else if(delayed_fft_transmit_dist2[1] && accel_y_idx_in[1]) begin
                        fp_sub_en   <= `SD delayed_fp_prod[SAMPLE_BIT_WIDTH*2];
                        fp_sub_val1 <= `SD y_extern_data_in[accel_y_idx_in-2][SAMPLE_BIT_WIDTH*2-1:0];
                        fp_sub_val2 <= `SD delayed_fp_prod[(SAMPLE_BIT_WIDTH*2)-1:0];
                    end else if(delayed_fft_transmit_dist2[0] && accel_y_idx_in[0]) begin
                        fp_sub_en   <= `SD delayed_fp_prod[SAMPLE_BIT_WIDTH*2];
                        fp_sub_val1 <= `SD y_extern_data_in[accel_y_idx_in-1][SAMPLE_BIT_WIDTH*2-1:0];
                        fp_sub_val2 <= `SD delayed_fp_prod[(SAMPLE_BIT_WIDTH*2)-1:0];
                    end else begin
                        fp_sub_en   <= `SD '0;
                        fp_sub_val1 <= `SD '0;
                        fp_sub_val2 <= `SD '0;
                    end
                end
            end else begin
                fp_sub_en   <= `SD '0;
                fp_sub_val1 <= `SD '0;
                fp_sub_val2 <= `SD '0;
            end
        end
    end
    fp_sub_complex #(
        .N(N),
        .E(E),
        .S(S)
        ) fp_sub_complex_unit (

        // Inputs
        .clock(clock),
        .reset(reset),
        .en_in(fp_sub_en),
        .val1_in(fp_sub_val1),
        .val2_in(fp_sub_val2),

        // Outputs
        .en_out(fp_diff_en),
        .diff_out(fp_diff)

    );


    // FP Complex Addition Module
    always_ff @(posedge clock) begin
        if(reset) begin
            fp_add_en   <= `SD '0;
            fp_add_val1 <= `SD '0;
            fp_add_val2 <= `SD '0;
        end else begin
            if(nufft_op == REGRIDDING) begin
                fp_add_en   <= `SD fp_prod_en;
                fp_add_val1 <= `SD delayed_sample_2.data;
                fp_add_val2 <= `SD fp_prod;
            end else if(nufft_op == FFT_IFFT) begin
                // If delayed_fft_transmit_dist2 == 0, don't send anything anywhere
                if(delayed_fft_transmit_dist2 == 0) begin
                    fp_add_en   <= `SD fp_sub_en;
                    fp_add_val1 <= `SD fp_sub_val1;
                    fp_add_val2 <= `SD fp_sub_val2;
                end else if(~delayed_fft_second_dim_en2) begin
                    // If delayed_fft_transmit_dist2 > 0, val2 is sent forward (higher pipe idx), while val1 is sent backward (lower pipe idx)
                    if(delayed_fft_transmit_dist2[2] && ~accel_x_idx_in[2]) begin
                        fp_add_en   <= `SD x_extern_data_in[accel_x_idx_in+4][SAMPLE_BIT_WIDTH*2];
                        fp_add_val1 <= `SD delayed_fp_rd_data_3[SAMPLE_BIT_WIDTH*2-1:0];
                        fp_add_val2 <= `SD x_extern_data_in[accel_x_idx_in+4][SAMPLE_BIT_WIDTH*2-1:0];
                    end else if(delayed_fft_transmit_dist2[1] && ~accel_x_idx_in[1]) begin
                        fp_add_en   <= `SD x_extern_data_in[accel_x_idx_in+2][SAMPLE_BIT_WIDTH*2];
                        fp_add_val1 <= `SD delayed_fp_rd_data_3[SAMPLE_BIT_WIDTH*2-1:0];
                        fp_add_val2 <= `SD x_extern_data_in[accel_x_idx_in+2][SAMPLE_BIT_WIDTH*2-1:0];
                    end else if(delayed_fft_transmit_dist2[0] && ~accel_x_idx_in[0]) begin
                        fp_add_en   <= `SD x_extern_data_in[accel_x_idx_in+1][SAMPLE_BIT_WIDTH*2];
                        fp_add_val1 <= `SD delayed_fp_rd_data_3[SAMPLE_BIT_WIDTH*2-1:0];
                        fp_add_val2 <= `SD x_extern_data_in[accel_x_idx_in+1][SAMPLE_BIT_WIDTH*2-1:0];
                    end else begin
                        fp_add_en   <= `SD '0;
                        fp_add_val1 <= `SD '0;
                        fp_add_val2 <= `SD '0;
                    end
                end else begin // if(delayed_fft_second_dim_en2)
                    // If delayed_fft_transmit_dist2 > 0, val2 is sent forward (higher pipe idx), while val1 is sent backward (lower pipe idx)
                    if(delayed_fft_transmit_dist2[2] && ~accel_y_idx_in[2]) begin
                        fp_add_en   <= `SD y_extern_data_in[accel_y_idx_in+4][SAMPLE_BIT_WIDTH*2];
                        fp_add_val1 <= `SD delayed_fp_rd_data_3[SAMPLE_BIT_WIDTH*2-1:0];
                        fp_add_val2 <= `SD y_extern_data_in[accel_y_idx_in+4][SAMPLE_BIT_WIDTH*2-1:0];
                    end else if(delayed_fft_transmit_dist2[1] && ~accel_y_idx_in[1]) begin
                        fp_add_en   <= `SD y_extern_data_in[accel_y_idx_in+2][SAMPLE_BIT_WIDTH*2];
                        fp_add_val1 <= `SD delayed_fp_rd_data_3[SAMPLE_BIT_WIDTH*2-1:0];
                        fp_add_val2 <= `SD y_extern_data_in[accel_y_idx_in+2][SAMPLE_BIT_WIDTH*2-1:0];
                    end else if(delayed_fft_transmit_dist2[0] && ~accel_y_idx_in[0]) begin
                        fp_add_en   <= `SD y_extern_data_in[accel_y_idx_in+1][SAMPLE_BIT_WIDTH*2];
                        fp_add_val1 <= `SD delayed_fp_rd_data_3[SAMPLE_BIT_WIDTH*2-1:0];
                        fp_add_val2 <= `SD y_extern_data_in[accel_y_idx_in+1][SAMPLE_BIT_WIDTH*2-1:0];
                    end else begin
                        fp_add_en   <= `SD '0;
                        fp_add_val1 <= `SD '0;
                        fp_add_val2 <= `SD '0;
                    end
                end
            end else begin
                fp_add_en   <= `SD '0;
                fp_add_val1 <= `SD '0;
                fp_add_val2 <= `SD '0;
            end
        end
    end
    fp_add_complex #(
        .N(N),
        .E(E),
        .S(S)
        ) fp_add_complex_unit (

        // Inputs
        .clock(clock),
        .reset(reset),
        .en_in(fp_add_en),
        .val1_in(fp_add_val1),
        .val2_in(fp_add_val2),

        // Outputs
        .en_out(fp_sum_en),
        .sum_out(fp_sum)

    );


    // Sample Delay Module (output of fp_mult_complex_unit to output of fp_add_complex_unit)
    sample_delay_queue #(
        .DELAY_CYCLES(2)
        ) sample_dq3 (

        // Inputs
        .clock(clock),
        .reset(reset),
        .datum_in(delayed_sample_2),

        // Outputs
        .datum_out(delayed_sample_3)

    );

    // Output Mux
    always_comb begin
        sample_out_next.en      = '0;
        sample_out_next.x_coord = '0;
        sample_out_next.y_coord = '0;
        sample_out_next.data    = '0;

        rd_en_out_next   = '0;
        rd_data_out_next = '0;

        extern_data_out_next = '0;

        if(nufft_op == GRIDDING) begin
            sample_out_next = sample_in;
        end else if(nufft_op == REGRIDDING) begin
            sample_out_next.en      = delayed_sample_3.en;
            sample_out_next.x_coord = delayed_sample_3.x_coord;
            sample_out_next.y_coord = delayed_sample_3.y_coord;
            sample_out_next.data    = fp_sum_en ? fp_sum : delayed_sample_3.data;
        end else if(nufft_op == FFT_IFFT) begin
            extern_data_out_next = delayed_fp_rd_data_2;

            if(~delayed_fft_second_dim_en1) begin
                // If transmit_dist == 0, don't send anything anywhere
                if(delayed_fft_transmit_dist1[2] && accel_x_idx_in[2]) begin
                    extern_data_out_next = {fp_prod_en, fp_prod};
                end else if(delayed_fft_transmit_dist1[1] && accel_x_idx_in[1]) begin
                    extern_data_out_next = {fp_prod_en, fp_prod};
                end else if(delayed_fft_transmit_dist1[0] && accel_x_idx_in[0]) begin
                    extern_data_out_next = {fp_prod_en, fp_prod};
                end
            end else begin // if(delayed_fft_second_dim_en1)
                // If transmit_dist == 0, don't send anything anywhere
                if(delayed_fft_transmit_dist1[2] && accel_y_idx_in[2]) begin
                    extern_data_out_next = {fp_prod_en, fp_prod};
                end else if(delayed_fft_transmit_dist1[1] && accel_y_idx_in[1]) begin
                    extern_data_out_next = {fp_prod_en, fp_prod};
                end else if(delayed_fft_transmit_dist1[0] && accel_y_idx_in[0]) begin
                    extern_data_out_next = {fp_prod_en, fp_prod};
                end
            end
        end else if(nufft_op == READ_MEM) begin
            rd_en_out_next   = fp_rd_en_out;
            rd_data_out_next = fp_rd_data_out;
        end
    end

    always_ff @(posedge clock) begin
        if(reset) begin
            sample_out.en      <= `SD '0;
            sample_out.data    <= `SD '0;
            sample_out.x_coord <= `SD '0;
            sample_out.y_coord <= `SD '0;

            rd_en_out          <= `SD '0;
            rd_data_out        <= `SD '0;

            extern_data_out    <= `SD '0;

            fft_done_out       <= `SD '0;
        end else begin
            sample_out.en      <= `SD sample_out_next.en;
            sample_out.data    <= `SD sample_out_next.data;
            sample_out.x_coord <= `SD sample_out_next.x_coord;
            sample_out.y_coord <= `SD sample_out_next.y_coord;

            rd_en_out          <= `SD rd_en_out_next;
            rd_data_out        <= `SD rd_data_out_next;

            extern_data_out    <= `SD extern_data_out_next;

            fft_done_out       <= `SD fft_done_out_next;
        end
    end

endmodule // pipeline_fp
