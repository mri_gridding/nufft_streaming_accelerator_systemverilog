/////////////////////////////////////////////////////////////////////////
//                                                                     //
//  Module name :  select.sv                                           //
//                                                                     //
//  Description :  Calculates distance from pipeline to input, interp  //
//                 table indices, and tile index.                      //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

// `timescale 1ns/100ps

module select #(
    parameter SAMPLE_BIT_WIDTH        = 32,
    parameter SAMPLE_COORD_BIT_WIDTH  = 32,
    parameter SAMPLE_COORD_FRAC_WIDTH = 22,
    parameter MAX_INTERP_WINDOW_SIZE  = 8,
    parameter WEIGHT_OVERSAMP         = 32,
    parameter ACCEL_DIM               = 8,
    parameter MAX_GRID_DIM            = 1024,
    parameter MEM_TILE_DIM            = (MAX_GRID_DIM /ACCEL_DIM ),
    parameter NUM_WEIGHTS             = 512, // (MAX_INTERP_WINDOW_SIZE*WEIGHT_OVERSAMP),
    parameter NUM_BUFFER_ENTRIES      = 16384
    ) (

        // Inputs
        input                                      clock,
        input                                      reset,
        input                                      sample_en_in,
        input [SAMPLE_COORD_BIT_WIDTH-1:0]         sample_x_coord_in,
        input [SAMPLE_COORD_BIT_WIDTH-1:0]         sample_y_coord_in,
        input [$clog2(ACCEL_DIM)-1:0]              accel_x_idx_in,
        input [$clog2(ACCEL_DIM)-1:0]              accel_y_idx_in,
        input [$clog2(MEM_TILE_DIM)-1:0]           tile_dim_in,
        input [$clog2(MAX_INTERP_WINDOW_SIZE)-1:0] window_dim_in,

        // Outputs
        output logic                                  select_en_out,
        output logic [$clog2(NUM_WEIGHTS)-1:0]        select_x_weight_idx_out,
        output logic [$clog2(NUM_WEIGHTS)-1:0]        select_y_weight_idx_out,
        output logic [$clog2(NUM_BUFFER_ENTRIES)-1:0] select_tile_idx_out

    );


    // Local variables
    logic [$clog2(ACCEL_DIM)-1:0]              accel_x_idx; // holds the x index of this pipeline in the accelerator grid
    logic [$clog2(ACCEL_DIM)-1:0]              accel_y_idx; // holds the y index of this pipeline in the accelerator grid
    logic [$clog2(MEM_TILE_DIM)-1:0]           tile_dim; // holds the y index of this pipeline in the accelerator grid
    logic [$clog2(MAX_INTERP_WINDOW_SIZE)-1:0] interp_window_dim;

    logic [SAMPLE_COORD_BIT_WIDTH-SAMPLE_COORD_FRAC_WIDTH-$clog2(ACCEL_DIM)-1:0] x_base_tile_idx, x_base_tile_idx_next;
    logic [SAMPLE_COORD_BIT_WIDTH-SAMPLE_COORD_FRAC_WIDTH-$clog2(ACCEL_DIM)-1:0] y_base_tile_idx, y_base_tile_idx_next;
    logic [$clog2(ACCEL_DIM)+SAMPLE_COORD_FRAC_WIDTH-1:0]                        x_coord, x_coord_next;
    logic [$clog2(ACCEL_DIM)+SAMPLE_COORD_FRAC_WIDTH-1:0]                        y_coord, y_coord_next;
    logic [$clog2(ACCEL_DIM)+SAMPLE_COORD_FRAC_WIDTH-1:0]                        x_distance, x_distance_next, x_distance_weight_round;
    logic [$clog2(ACCEL_DIM)+SAMPLE_COORD_FRAC_WIDTH-1:0]                        y_distance, y_distance_next, y_distance_weight_round;

    logic [$clog2(MEM_TILE_DIM)-1:0] x_tile_idx, x_tile_idx_next;
    logic [$clog2(MEM_TILE_DIM)-1:0] y_tile_idx, y_tile_idx_next;

    logic sample_en_saved, sample_en_saved_2, sample_en_saved_2_next;

    logic                                  select_en_out_next;
    logic [$clog2(NUM_WEIGHTS)-1:0]        select_x_weight_idx_out_next;
    logic [$clog2(NUM_WEIGHTS)-1:0]        select_y_weight_idx_out_next;
    logic [$clog2(NUM_BUFFER_ENTRIES)-1:0] select_tile_idx_out_next;


    // Combine the x and y table values to create the final interpolation constant
    always_comb begin
        select_en_out_next           = '0;
        select_x_weight_idx_out_next = '0;
        select_y_weight_idx_out_next = '0;
        select_tile_idx_out_next     = '0;

        x_base_tile_idx_next = '0;
        y_base_tile_idx_next = '0;
        x_coord_next         = '0;
        y_coord_next         = '0;

        x_distance_next = '0;
        y_distance_next = '0;
        x_tile_idx_next = '0;
        y_tile_idx_next = '0;

        sample_en_saved_2_next = sample_en_saved;

        x_distance_weight_round = '0;
        y_distance_weight_round = '0;

        // Cycle 1
        if(sample_en_in) begin
            // Get the quotient and remainder to determine grid and tile location
            x_base_tile_idx_next = sample_x_coord_in[SAMPLE_COORD_BIT_WIDTH-1:$clog2(ACCEL_DIM)+SAMPLE_COORD_FRAC_WIDTH]; // x virtual tile index
            y_base_tile_idx_next = sample_y_coord_in[SAMPLE_COORD_BIT_WIDTH-1:$clog2(ACCEL_DIM)+SAMPLE_COORD_FRAC_WIDTH]; // y virtual tile index
            
            // Extract the "relative" coordinates (within a tile)
            x_coord_next = sample_x_coord_in[$clog2(ACCEL_DIM)+SAMPLE_COORD_FRAC_WIDTH-1:0]; // "relative" x coordinate (within a tile)
            y_coord_next = sample_y_coord_in[$clog2(ACCEL_DIM)+SAMPLE_COORD_FRAC_WIDTH-1:0]; // "relative" y coordinate (within a tile)
        end

        // Cycle 2
        if(sample_en_saved) begin
            // Calculate the x and y distances (from the pipeline to the relative coordinates)
            x_distance_next = ({ACCEL_DIM, {SAMPLE_COORD_FRAC_WIDTH{1'b0}}} + x_coord) - {accel_x_idx, {SAMPLE_COORD_FRAC_WIDTH{1'b0}}}; // shortest distance around the torus in the x dimension
            y_distance_next = ({ACCEL_DIM, {SAMPLE_COORD_FRAC_WIDTH{1'b0}}} + y_coord) - {accel_y_idx, {SAMPLE_COORD_FRAC_WIDTH{1'b0}}}; // shortest distance around the torus in the y dimension
            
            // Check if a wrap occured in the x dimension (virtual tile)
            if(x_coord < {accel_x_idx, {SAMPLE_COORD_FRAC_WIDTH{1'b0}}}) begin
                if(x_base_tile_idx == 0) begin
                    x_tile_idx_next = tile_dim - 1;
                end else begin
                    x_tile_idx_next = x_base_tile_idx - 1;
                end
            end else begin
                x_tile_idx_next = x_base_tile_idx;
            end
            
            // Check if a wrap occured in the y dimension (virtual tile)
            if(y_coord < {accel_y_idx, {SAMPLE_COORD_FRAC_WIDTH{1'b0}}}) begin
                if(y_base_tile_idx == 0) begin
                    y_tile_idx_next = tile_dim - 1;
                end else begin
                    y_tile_idx_next = y_base_tile_idx - 1;
                end
            end else begin
                y_tile_idx_next = y_base_tile_idx;
            end
        end

        // Cycle 3
        if(sample_en_saved_2) begin
            // Check if the pipeline is affected
            if((x_distance < {interp_window_dim, {SAMPLE_COORD_FRAC_WIDTH{1'b0}}}) && (y_distance < {interp_window_dim, {SAMPLE_COORD_FRAC_WIDTH{1'b0}}})) begin
                select_en_out_next = 1'b1;
            
                // Calculate the table index (based on distance)
                x_distance_weight_round = x_distance + {1'b1, {(SAMPLE_COORD_FRAC_WIDTH-$clog2(WEIGHT_OVERSAMP)-1){1'b0}}};
                y_distance_weight_round = y_distance + {1'b1, {(SAMPLE_COORD_FRAC_WIDTH-$clog2(WEIGHT_OVERSAMP)-1){1'b0}}};

                select_x_weight_idx_out_next = x_distance_weight_round[$clog2(ACCEL_DIM)+SAMPLE_COORD_FRAC_WIDTH-1:SAMPLE_COORD_FRAC_WIDTH-$clog2(WEIGHT_OVERSAMP)]; // *L (a power of 2) is implemented as a left shift;
                select_y_weight_idx_out_next = y_distance_weight_round[$clog2(ACCEL_DIM)+SAMPLE_COORD_FRAC_WIDTH-1:SAMPLE_COORD_FRAC_WIDTH-$clog2(WEIGHT_OVERSAMP)]; // *L (a power of 2) is implemented as a left shift;

                // Calculate the combined tile index ("depth" in the pipeline's data array)
                select_tile_idx_out_next = (y_tile_idx * MEM_TILE_DIM) + x_tile_idx; // 1D global virtual tile index;
            end
        end
    end // always_comb

    // synopsys sync_set_reset "reset"
    always_ff @(posedge clock) begin
        if(reset) begin
            accel_x_idx       <= `SD accel_x_idx_in;
            accel_y_idx       <= `SD accel_y_idx_in;
            tile_dim          <= `SD tile_dim_in;
            interp_window_dim <= `SD window_dim_in;

            x_base_tile_idx <= `SD '0;
            y_base_tile_idx <= `SD '0;
            x_coord         <= `SD '0;
            y_coord         <= `SD '0;

            x_distance <= `SD '0;
            y_distance <= `SD '0;
            x_tile_idx <= `SD '0;
            y_tile_idx <= `SD '0;

            sample_en_saved   <= `SD '0;
            sample_en_saved_2 <= `SD '0;

            select_en_out           <= `SD '0;
            select_x_weight_idx_out <= `SD '0;
            select_y_weight_idx_out <= `SD '0;
            select_tile_idx_out     <= `SD '0;
        end else begin
            x_base_tile_idx <= `SD x_base_tile_idx_next;
            y_base_tile_idx <= `SD y_base_tile_idx_next;
            x_coord         <= `SD x_coord_next;
            y_coord         <= `SD y_coord_next;

            x_distance <= `SD x_distance_next;
            y_distance <= `SD y_distance_next;
            x_tile_idx <= `SD x_tile_idx_next;
            y_tile_idx <= `SD y_tile_idx_next;

            sample_en_saved   <= `SD sample_en_in;
            sample_en_saved_2 <= `SD sample_en_saved_2_next;

            select_en_out           <= `SD select_en_out_next;
            select_x_weight_idx_out <= `SD select_x_weight_idx_out_next;
            select_y_weight_idx_out <= `SD select_y_weight_idx_out_next;
            select_tile_idx_out     <= `SD select_tile_idx_out_next;
        end // if(reset) begin
    end // always_ff @(posedge clock)
endmodule // select
