module fp_add_complex #(parameter N = 32, parameter E = 8, parameter S = 1) (
    // Inputs
    input logic             clock,
    input logic             reset,
    input logic             en_in,
    input logic [(N*2)-1:0] val1_in,
    input logic [(N*2)-1:0] val2_in,

    // Outputs
    output logic             en_out,
    output logic [(N*2)-1:0] sum_out
);

    logic             en_out_next;
    logic [(N*2)-1:0] sum_out_next;

    fadd_0cycle #(.N(N), .E(E), .S(S)) add_real (
        .en(en_in),
        .op1(val1_in[(N*2)-1:N]),
        .op2(val2_in[(N*2)-1:N]),

        .res_val(en_out_next),
        .res(sum_out_next[(N*2)-1:N])
    );

    fadd_0cycle #(.N(N), .E(E), .S(S)) add_imag (
        .en(en_in),
        .op1(val1_in[N-1:0]),
        .op2(val2_in[N-1:0]),

        .res_val(),
        .res(sum_out_next[N-1:0])
    );

    always_ff @(posedge clock) begin
        if(reset) begin
            en_out  <= '0;
            sum_out <= '0;
        end else begin
            en_out  <= en_out_next;
            sum_out <= sum_out_next;
        end
    end

endmodule : fp_add_complex
