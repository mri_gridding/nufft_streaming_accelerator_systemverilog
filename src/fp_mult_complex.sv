/////////////////////////////////////////////////////////////////////////
//                                                                     //
//  Module name :  interp.sv                                           //
//                                                                     //
//  Description :  Apodizes selected samples.                          //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

// `timescale 1ns/100ps

module fp_mult_complex #(
    parameter N                  = 32,
    parameter E                  = 8,
    parameter S                  = 1,
    parameter SAMPLE_BIT_WIDTH   = 32,
    parameter WEIGHT_BIT_WIDTH   = 16,
    parameter NUM_BUFFER_ENTRIES = 256
    ) (

        // Inputs
        input                            clock,
        input                            reset,
        input                            mult_en_in,
        input [(SAMPLE_BIT_WIDTH*2)-1:0] val1_in,
        input [(SAMPLE_BIT_WIDTH*2)-1:0] val2_in,

        // Outputs
        output logic                            en_out,
        output logic [(SAMPLE_BIT_WIDTH*2)-1:0] prod_out

    );

    // Local variables
    logic [SAMPLE_BIT_WIDTH-1:0] k1, k1_next;
    logic [SAMPLE_BIT_WIDTH-1:0] k2, k2_next;
    logic [SAMPLE_BIT_WIDTH-1:0] k3, k3_next;
    logic [SAMPLE_BIT_WIDTH-1:0] k4, k4_next;
    logic [SAMPLE_BIT_WIDTH-1:0] k5, k5_next;
    logic [SAMPLE_BIT_WIDTH-1:0] k6, k6_next;
    logic [SAMPLE_BIT_WIDTH-1:0] r_next;
    logic [SAMPLE_BIT_WIDTH-1:0] i_next;

    logic k3_val_en, k6_val_en, k6_val_en_next, i_val_en_next;


    logic [SAMPLE_BIT_WIDTH-1:0] val2_real;
    logic [SAMPLE_BIT_WIDTH-1:0] val1_real;
    logic [SAMPLE_BIT_WIDTH-1:0] val1_imag;


    fadd_0cycle #(.N(N), .E(E), .S(S)) a1 (
        .en(mult_en_in),
        .op1(val1_in[(SAMPLE_BIT_WIDTH*2)-1:SAMPLE_BIT_WIDTH]),
        .op2(val1_in[SAMPLE_BIT_WIDTH-1:0]),
        .res_val(),
        .res(k1_next)
    );
    fsub_0cycle #(.N(N), .E(E), .S(S)) s1 (
        .en(mult_en_in),
        .op1(val2_in[SAMPLE_BIT_WIDTH-1:0]),
        .op2(val2_in[(SAMPLE_BIT_WIDTH*2)-1:SAMPLE_BIT_WIDTH]),
        .res_val(),
        .res(k2_next)
    );
    fadd_0cycle #(.N(N), .E(E), .S(S)) a2 (
        .en(mult_en_in),
        .op1(val2_in[(SAMPLE_BIT_WIDTH*2)-1:SAMPLE_BIT_WIDTH]),
        .op2(val2_in[SAMPLE_BIT_WIDTH-1:0]),
        .res_val(),
        .res(k3_next)
    );
    fmul_0cycle #(.N(N), .E(E), .S(S)) m1 (
        .en(k3_val_en),
        .op1(val2_real),
        .op2(k1),
        .res_val(),
        .res(k4_next)
     );
    fmul_0cycle #(.N(N), .E(E), .S(S)) m2 (
        .en(k3_val_en),
        .op1(val1_real),
        .op2(k2),
        .res_val(),
        .res(k5_next)
     );
    fmul_0cycle #(.N(N), .E(E), .S(S)) m3 (
        .en(k3_val_en),
        .op1(val1_imag),
        .op2(k3),
        .res_val(),
        .res(k6_next)
     );
    fsub_0cycle #(.N(N), .E(E), .S(S)) s2 (
        .en(k6_val_en),
        .op1(k4),
        .op2(k6),
        .res_val(),
        .res(r_next)
    );
    fadd_0cycle #(.N(N), .E(E), .S(S)) a3 (
        .en(k6_val_en),
        .op1(k4),
        .op2(k5),
        .res_val(),
        .res(i_next)
    );

    // Combine the x and y table values to create the final interpolation constant
    always_comb begin
        k6_val_en_next = k3_val_en;
        i_val_en_next  = k6_val_en;
    end // always_comb

    // synopsys sync_set_reset "reset"
    always_ff @(posedge clock) begin
        if(reset) begin
            val2_real <= `SD '0;
            val1_real <= `SD '0;
            val1_imag <= `SD '0;

            k3_val_en <= `SD '0;
            k6_val_en <= `SD '0;

            k1 <= `SD '0;
            k2 <= `SD '0;
            k3 <= `SD '0;
            k4 <= `SD '0;
            k5 <= `SD '0;
            k6 <= `SD '0;

            en_out                                            <= `SD '0;
            prod_out[(SAMPLE_BIT_WIDTH*2)-1:SAMPLE_BIT_WIDTH] <= `SD '0;
            prod_out[SAMPLE_BIT_WIDTH-1:0]                    <= `SD '0;
        end else begin
            val2_real <= `SD val2_in[(SAMPLE_BIT_WIDTH*2)-1:SAMPLE_BIT_WIDTH];
            val1_real <= `SD val1_in[(SAMPLE_BIT_WIDTH*2)-1:SAMPLE_BIT_WIDTH];
            val1_imag <= `SD val1_in[SAMPLE_BIT_WIDTH-1:0];

            k3_val_en <= `SD mult_en_in;
            k6_val_en <= `SD k6_val_en_next;

            k1 <= `SD k1_next;
            k2 <= `SD k2_next;
            k3 <= `SD k3_next;
            k4 <= `SD k4_next;
            k5 <= `SD k5_next;
            k6 <= `SD k6_next;

            en_out   <= `SD i_val_en_next;
            prod_out <= `SD {r_next, i_next};
        end // if(reset) begin
    end // always_ff @(posedge clock)

endmodule // fp_mult_complex
